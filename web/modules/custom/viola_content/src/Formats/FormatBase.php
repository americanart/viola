<?php

namespace Drupal\viola_content\Formats;

use Laminas\Hydrator\ObjectPropertyHydrator;

class FormatBase implements \JsonSerializable {

  /**
   * Hydrator Implementation
   *
   * @var \Laminas\Hydrator\ObjectPropertyHydrator
   */
  protected $hydrator;

  public $type = 'node';
  public $bundle;

  /**
   * FormatBase constructor.
   */
  public function __construct() {
    $this->hydrator = new ObjectPropertyHydrator();
  }

  /**
   * The default data format extracts object properties to an array.
   *
   * @return array;
   */
  public function transform() {
    return $this->hydrator->extract($this);
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize() {
    return $this->transform();
  }

}
