<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Media;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
* The repository for remote video.
*
* @TypedRepository(
*    entity_type_id = "media",
*    bundle = "remote_video",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Media\RemoteVideo",
*    ),
*   description = @Translation("Repository that holds logic for remote video")
* )
*/
final class RemoteVideoRepository extends TypedRepositoryBase {}
