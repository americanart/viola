<?php

namespace Drupal\viola_content\WrappedEntities\Node;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\viola_content\WrappedEntities\Traits\HasDateRangeTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\viola_core\Utils\DateTimes;

/**
 * The wrapped entity for the Creator content type.
 */
class Creator extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;
  use HasDateRangeTrait;
  use HasCommonEntityFieldsTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Web Typography utilities service.
   *
   * @var \Drupal\viola_core\Typography\TypographyService
   */
  protected $typography;

  /**
   * JSON decoded value of the JSON data field.
   * @var array
   */
  public $data;

  /**
   * WrappedEntityBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   */
  public function __construct(EntityInterface $entity, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity);
    $this->entityTypeManager = $entityTypeManager;
    $this->typography = \Drupal::service('viola.typography');
    $this->data = JSON::decode($this->getEntity()->get('field_data')->value);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityInterface $entity): self {
    return new static(
      $entity,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @return string
   */
  public function getAlphabeticalName() {
    return $this->getEntity()->get('field_alphabetical_name')->value;
  }

  /**
   * @return array
   */
  public function getAssociatedPlaces() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_associated_places'
    );
  }

  /**
   * @return string
   */
  public function getBirthPlace() {
    return $this->getReferencedTaxonomyTermLabel(
      $this->getEntity(),
      'field_birth_place'
    );
  }

  /**
   * @return string
   */
  public function getCreatorType() {
    return $this->getReferencedTaxonomyTermLabel(
      $this->getEntity(),
      'field_creator_type'
    );
  }

  /**
   * @return string
   */
  public function getConstituentId() {
    return $this->getEntity()->get('field_constituent_id')->value;
  }

  /**
   * @return string
   */
  public function getCollectionDescription() {
    return $this->getEntity()->get('field_collection_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDateDescription() {
    return $this->getEntity()->get('field_date_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDateOfBirth() {
    return DateTimes::getFormattedDateTime(
      $this->getEntity()->get('field_date_of_birth')->value,
      'UTC'
    );
  }

  /**
   * @return string
   */
  public function getDateOfDeath() {
    return DateTimes::getFormattedDateTime(
      $this->getEntity()->get('field_date_of_death')->value,
      'UTC'
    );
  }

  /**
   * @return array
   */
  public function getDateRange() {
    return $this->getDateRangeFromStrings(
      $this->getEntity()->get('field_date_range')->value,
      $this->getEntity()->get('field_date_range')->end_value
    );
  }

  /**
   * @return string
   */
  public function getDefaultDescription() {
    if ($this->getDescription()) {
      return $this->getDescription();
    }
    if ($this->getCollectionDescription()) {
      return $this->getCollectionDescription();
    }
    if (!empty($this->data['wikipedia']['extract'])) {
      return $this->data['wikipedia']['extract'];
    }
    return '';
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->getEntity()->get('field_description')->value ?? '';
  }

  /**
   * @return array
   */
  public function getEthnicities() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_ethnicities'
    );
  }

  /**
   * @return string
   */
  public function getGender() {
    return $this->getReferencedTaxonomyTermLabel(
      $this->getEntity(),
      'field_gender'
    );
  }

  /**
   * @return array
   */
  public function getGroups() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_groups'
    );
  }

  /**
   * @return array
   */
  public function getInfluencedBy() {
    if (isset($this->data['wikidata']) && !empty($this->data['wikidata']['influencedBy'])) {
      return $this->data['wikidata']['influencedBy'];
    }
    return [];
  }

  /**
   * @return bool
   */
  public function isPermanentCollection() {
    return (bool) $this->getEntity()->get('field_is_permanent_collection')->value;
  }

  /**
   * @return string
   */
  public function getLibraryOfCongressId() {
    return $this->getEntity()->get('field_loc_id')->value ?? '';
  }

  /**
   * @return array
   */
  public function getMovements() {
    if (isset($this->data['wikidata']) && !empty($this->data['wikidata']['movement'])) {
      return $this->data['wikidata']['movement'];
    }
    return [];
  }

  /**
   * @return array
   */
  public function getNationalities() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_nationalities'
    );
  }

  /**
   * @return array
   */
  public function getOccupations() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_occupations'
    );
  }

  /**
   * @return array
   */
  public function getPlaces() {
    return array_filter(
      array_unique(
        array_merge(
          $this->getAssociatedPlaces(),
          [$this->getBirthPlace()],
          [$this->getPlaceOfDeath()])
      )
    );
  }

  /**
   * @return string
   */
  public function getPlaceOfDeath() {
    return $this->getReferencedTaxonomyTermLabel(
      $this->getEntity(),
      'field_place_of_death'
    );
  }

  /**
   * @return string
   */
  public function getSummary() {
    return $this->getEntity()->get('field_summary')->value;
  }

  /**
   * @return string
   */
  public function getUlanId() {
    return $this->getEntity()->get('field_ulan_id')->value ?? '';
  }

  /**
   * @return string
   */
  public function getViafId() {
    return $this->getEntity()->get('field_viaf_id')->value ?? '';
  }

  /**
   * @return string
   */
  public function getWikidataId() {
    return $this->getEntity()->get('field_wikidata_id')->value ?? '';
  }

  /**
   * @return string
   */
  public function getWikipediaTitle() {
    return $this->getEntity()->get('field_wikipedia_title')->value ?? '';
  }

}
