<?php

namespace Drupal\viola_linked_data\Commands;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\typed_entity\RepositoryManager;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaLinkedDataExportCommand extends DrushCommands {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * @param \Drupal\Core\Database\Connection $connection
   *   The Drupal default database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The Drupal logger channel factory.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactory $logger_factory
  ) {
    parent::__construct();
    $this->database = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('viola_linked_data');
  }

  /**
   * Export collection data in JSON-LD format.
   *
   * @param array $content_type
   *   The content type (machine name) to export.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option ids
   *   Comma-separated list of specific Node id(s) to import.
   * @option limit
   *   Limit number of items to import.
   * @usage viola-export:edan
   *
   * @command viola-linked-data:export
   * @aliases viola-ld:export
   */
  public function export($content_type, $options = ['ids' => '', 'limit' => 100000]) {
    $start = microtime(true);

    $query = $this->database->select('node_field_data', 'n')
      ->fields('n', ['nid'])
      ->condition('n.type', $content_type, '=')
      ->condition('n.status', 1);
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    if ($ids) {
      // Add condition for supplied Node ID(s).
      $query->condition('n.nid', $ids, 'IN');
    }
    if ($options['limit']) {
      $query->range(0, $options['limit']);
    }
    $nids = $query->execute()->fetchCol();
    $chunks = array_chunk($nids, 100);
    unset($nids);
    foreach ($chunks as $key => $ids) {
      $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple($ids);
      $collection = \Drupal::service(RepositoryManager::class)->wrapMultiple($nodes);
      foreach ($collection as $item) {
        dump($item->transformInto('json_ld'));
      }
    }
    $executionTime = microtime(true) - $start;
    $this->logger()->success(
        dt('Finished export in @time seconds.', ['@time' => $executionTime])
      );
  }

}
