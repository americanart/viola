<?php

namespace Drupal\viola_content\Formats\Paragraph\ImageComponent;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Paragraph\ImageComponent;

final class DefaultFormat extends FormatBase {

  public $aspectRatio;
  public array $image = [];
  public $includeCaption;
  public $text;

  /**
   * Paragraph\ImageComponent\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Paragraph\ImageComponent $component
   *   The wrapped entity.
   */
  public function __construct(ImageComponent $component) {
    parent::__construct();
    $this->bundle = $component->getEntity()->bundle();
    $this->type = $component->getEntity()->getEntityTypeId();
    $this->aspectRatio = $component->getAspectRatio();
    // Get image data.
    if ($wrappedImage = $component->getImage()) {
      $this->image = $wrappedImage->transformInto('default');
    }
    $this->includeCaption = $component->includeCaption();
    $this->text = $component->getText();
  }

}
