<?php

namespace Drupal\viola_content\Formats\Node\Page;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Page;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

final class DefaultFormat extends FormatBase {

  public $blurb;
  public $bundle;
  public array $components = [];
  public $formattedTitle;
  public $path;
  public array $image = [];
  public $summary;
  public $subtitle;
  public $title;

  /**
   * Node\Page\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Page $page
   *   The wrapped entity.
   */
  public function __construct(Page $page) {
    parent::__construct();
    // TODO: Get image data, itself a formatted "wrapped" entity.
    $this->blurb = $page->getBlurb();
    $this->bundle = $page->getBundle();
    $this->formattedTitle = $page->getFormattedTitle();
    // Get image data.
    if ($wrappedImage = $page->getImage()) {
      $this->image = $wrappedImage->transformInto('default');
    }
    $this->path = $page->getPathAlias();
    $this->subtitle = $page->getSubtitle();
    $this->summary = $page->getSummary();
    $this->title = $page->getTitle();
    // Get components data.
    $wrapped = $page->getComponents();
    foreach($wrapped as $component) {
      $this->components[] = $component->transformInto('default');
    }
  }

}
