<?php

namespace Drupal\viola_tms\DataMapper;

use Drupal\Component\Serialization\Json;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\DataMapper\DataMapper;
use Drupal\viola_core\DataMapper\Traits\HasDateRangeTrait;
use Drupal\viola_tms\DataMapper\Traits\HasThesaurusTermsTrait;
use Drupal\viola_core\Utils\Strings;
use Drupal\viola_core\Utils\Markup;
use Drupal\viola_core\Constants;

/**
 * Artwork DataMapper
 *
 * Map data from a TMS Extractor Export to Node fields.
 *
 */
class Artwork extends DataMapper {

  use HasThesaurusTermsTrait;
  use HasDateRangeTrait;

  /**
   * @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository;
   */
  protected $termRepository;

  /**
   * @param array $field_data
   */
  public function __construct($field_data = []) {
    $this->termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
    $this->setData($field_data);
  }

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {
    $this->data = [];
    if (isset($field_data['force'])) {
      $this->data['force'] = TRUE;
    }
    unset($field_data['force']);
    // Checksum.
    $this->data['field_checksum'] = DataMapper::generateChecksum($field_data);
    // Object ID.
    if (self::isValidDataKey('objectId', $field_data)) {
      $this->data['field_object_id'] = $field_data['objectId'];
    }
    // Object Number.
    if (self::isValidDataKey('objectNumber', $field_data)) {
      $this->data['field_accession_number'] = $field_data['objectNumber'];
    }
    // Title.
    if (self::isValidDataKey('title', $field_data)) {
      $title = Strings::decodeSpecialChars($field_data['title']);
      if (strlen($title) > 255) {
        // Ensure the title doesn't exceed field length constraint.
        $field_data['title'] = Strings::truncateText($title, 255);
        // Set the long title.
        $this->data['field_long_title'] = $title;
      }
      $this->data['title'] = $field_data['title'] ?? 'Untitled';
      unset($field_data['title']);
    }
    // Classifications (parent) Term
    if (self::isValidDataKey('classification', $field_data)) {
      $tid = $this->termRepository->getTermIdByName('classifications', $field_data['classification']);
      if ((int) $tid > 0) {
        $this->data['field_classifications'][] = $tid;
        unset($field_data['classification']);
      }
    }
    // Classifications (child) Term.
    if (self::isValidDataKey('subClassification', $field_data)) {
      $tid = $this->termRepository->getTermIdByName('classifications', $field_data['subClassification']);
      if ((int) $tid > 0) {
        $this->data['field_classifications'][] = $tid;
        unset($field_data['subClassification']);
      }
    }

    // Set a "century" Term from the dated field.
    if (self::isValidDataKey('dated', $field_data)) {
      $century = $this->getCenturyString($field_data['dated']);
      if ($century) {
        $tid = $this->termRepository->getTermIdByName('time_periods', $century);
        if ((int) $tid > 0) {
          $this->data['field_time_periods'][] = $tid;
        }
      }
    }
    // Mediums. Terms are populated from TMS Thesaurus topics of type "Medium".
    if (self::isValidDataKey('mediums', $field_data)) {
      $this->data['field_mediums'] = [];
      if (is_array($field_data['mediums'])) {
        foreach($field_data['mediums'] as $thesaurusTerm) {
          if (isset($thesaurusTerm['pathTerm'])) {
            $name = $this->getPathTermWord($thesaurusTerm['pathTerm']);
            $tid = $this->termRepository->getTermIdByName('mediums', $name);
            if ((int) $tid > 0) {
              $this->data['field_mediums'][] = $tid;
            }
          }
        }
      }
    }
    // Collection. Get the "collection" Term from the department category.
    if (self::isValidDataKey('department', $field_data)) {
      if ($field_data['department'] === 'Renwick Gallery') {
        $tid = $this->termRepository->getTermIdByName('venues', 'Renwick Gallery');
      }
      else {
        $tid = $this->termRepository->getTermIdByName('venues', 'Smithsonian American Art Museum');
      }
      if ((int) $tid > 0) {
        $this->data['field_collection'][] = $tid;
        // By default if a collection value is supplied, we set it as permanent.
        $this->data['field_is_permanent_collection'] = TRUE;
      }
    }
    // TODO: get field_is_new_acquisition.
    // On View. Get boolean value based on whether the CurSite field contains "@" sign.
    if (self::isValidDataKey('curSite', $field_data)) {
      $this->data['field_is_on_view'] = (strpos(trim($field_data['curSite']), '@') !== FALSE) ? TRUE : FALSE;
    }
    // Usage Rights.
    if (self::isValidDataKey('objRightsType', $field_data)) {
      $tid = $this->termRepository->getTermIdByName('usage_rights', $field_data['objRightsType']);
      if ((int) $tid > 0) {
        $this->data['field_usage_rights'][] = $tid;
        unset($field_data['objRightsType']);
      }
    }
    // Date Range.
    if (self::isValidDataKey('searchBeginDate', $field_data) && self::isValidDataKey('searchEndDate', $field_data)) {
      if ($range = $this->getDateRangeValuesFromYears($field_data['searchBeginDate'] , $field_data['searchEndDate'] )) {
        $this->data['field_date_range'] = $range;
        unset($field_data['searchBeginDate'], $field_data['searchEndDate']);
      }
    }
    // Unidentified Creator.
    if (isset($field_data['artworkConstituentRelationships'])) {
      // Check for "unidentified" constituent relationship.
      if (is_array($field_data['artworkConstituentRelationships'])) {
        foreach($field_data['artworkConstituentRelationships'] as $relationship) {
          if (isset($relationship['displayName']) && strpos(strtolower($relationship['displayName']), 'unidentified') !== FALSE) {
            $this->data['field_has_unidentified_creator'] = TRUE;
          }
        }
      }
    }
    // Collection Description.
    $labels = ['executionDate', 'exhibitionLabel', 'galleryLabel', 'luceCenterLabel', 'LuceObjectQuote', 'newAcquisitionLabel', 'publicationLabel'];
    foreach ($labels as $label) {
      if (isset($field_data[$label])) {
        $this->data['field_collection_description'] = [
          'value' => trim(Markup::clean($field_data[$label])),
          'format' => 'full_html',
        ];
        unset($field_data[$label]);
        break;
      }
    }
    // Translated Description.
    // if (isset($field_data['spanishLabel'])) {
      // TODO: Decide how to handle this. @see: https://gitlab.com/americanart/viola/-/issues/1
    // }

    // Translated Title.
    if (isset($field_data['translatedTitle'])) {
      $this->data['field_translated_title'] = trim($field_data['translatedTitle']);
    }
    // Dimensions Description.
    if (isset($field_data['displayDimensions'])) {
      $this->data['field_dimensions_description'] = [
        'value' => \Drupal::service('viola.typography')->process(trim($field_data['displayDimensions'])),
        'format' => 'full_html',
      ];
      unset($field_data['displayDimensions']);
    }

    // Data JSON field. (Remaining fields are added as JSON data keyed by source).
    $data = ['tms' => $field_data];
    $this->data['field_data'] = json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
  }

  /**
   * Get the century string from a dated string (i.e. "ca. 1910-1950").
   *
   * @param string $dated
   * @return string
   */
  private function getCenturyString($dated) {
    $centuries = Constants::CENTURIES;
    // If the dated string has a "reproduction" label don't set a century,
    // since it's difficult to parse which year should be used.
    if (empty($dated) || stripos($dated, 'reproduction') !== FALSE) {
      return '';
    }
    // Look for literal century strings written as "15th century".
    foreach($centuries as $key => $value) {
      if (preg_match('/' . $value . '/i', $dated)) {
        return $centuries[$key];
      }
    }
    // Remove common prefixes from the dated string.
    $dated = trim(str_replace(['ca.', 'n.d.', 'published', 'printed'], '', $dated));
    // Get year from dated string.
    preg_match('/[1-2][0-9]\d{2}/', $dated, $year);
    // Return the century based on the first two digits of the year.
    if ($year) {
      $key = substr($year[0], 0, 2);
      if (isset($centuries[$key])) {
        return $centuries[$key];
      }
    }
    return '';
  }

}
