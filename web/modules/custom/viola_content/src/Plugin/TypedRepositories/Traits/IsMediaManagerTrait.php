<?php


namespace Drupal\viola_content\Plugin\TypedRepositories\Traits;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;

/**
 * Trait IsMediaManagerTrait
 *   Common CRUD methods for all Media Repositories.
 *
 * @package Drupal\viola_content\Plugin\TypedRepositories\Traits
 */
trait IsMediaManagerTrait {

  /**
   * Load all entities.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]|bool
   */
  public function all() {
    return $this->findWhere();
  }

  /**
   * Load entities filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]
   */
  public function findWhere($fields = []) {
    try {
      $query = $this->entityTypeManager->getStorage($this->getEntityType()->id());
      $conditions = [
        'status' => TRUE,
        'bundle' => $this->getBundle(),
      ];
      foreach ($fields as $field_name => $field_value) {
        $conditions[$field_name] = $field_value;
      }
      $results = $query->loadByProperties($conditions);
      $entities = [];
      foreach ($results as $item) {
        $entities[] = $item;
      }
      return $this->wrapMultiple($entities);
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return [];
  }

  /**
   * Load the first entity from results filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   */
  public function firstWhere($fields = []) {
    if ($results = $this->findWhere($fields)) {
      return reset($results);
    }
    return NULL;
  }

  /**
   * Prepare supplied fields and field values for entity create/update.
   *
   * @param array $fields
   *   Field data.
   *
   * @return array
   *   Combined array of default and supplied fields.
   */
  protected function prepareFields(array $fields) {
    $commonFields = [
      'langcode' => 'en',
      'changed' => \Drupal::time()->getRequestTime(),
      'bundle' => $this->getBundle(),
      'uid' => 1,
      'status' => 1,
    ];
    // Unset any non-Drupal fields that might have been attached.
    unset($fields['mid']);
    return array_merge($commonFields, $fields);
  }


  /**
   * Create a new Media entity populated by the provided field values.
   *
   * @param array $fields
   *
   * @return int
   */
  public function createMedia(array $fields) {
    try {
      $status = 0;
      $fields = $this->prepareFields($fields);
      if (isset($fields['image_url'])) {
        $file = $this->getRemoteFile($fields['image_url']);
        if ($file instanceof FileInterface) {
          $fields['field_media_image'] = $file->id();
          unset($fields['image_url']);
          $media = $this->entityTypeManager->getStorage($this->getEntityType()->id())->create($fields)->enforceIsNew();
          // Set alt text.
          $media->field_media_image->alt = !empty($fields['alt']) ? $fields['alt'] : $fields['name'];
          $status = $media->save();
        }
      }
      return $status;
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

  /**
   * Update an existing media entity.
   *
   * @param array $fields
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return int
   */
  public function updateMedia(array $fields, EntityInterface $entity) {
    $status = 0;
    try {
      $fields = $this->prepareFields($fields);
      if (isset($fields['image_url'])) {
        $file = $this->getRemoteFile($fields['image_url']);
        if ($file instanceof FileInterface) {
          $fields['field_media_image'] = $file->id();
          unset($fields['image_url']);
        }
      }
      foreach ($fields as $field_name => $field_value) {
        $entity->set($field_name, $field_value);
      }
      $status = $entity->save();
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }

  /**
   * Retrieve a remote file from an absolute url and store as a managed Drupal File.
   *
   * @param $url
   *   The absolute url to the file.
   * @param string $ext
   *   The file extension.
   *
   * @return mixed
   *
   * @throws \Exception
   */
  private function getRemoteFile($url, $ext = 'jpg') {
    // Test valid url.
    if(!UrlHelper::isValid($url, $absolute = TRUE)){
      throw new \Exception('Request for remote file is an invalid url: %s', $url);
    }
    // Test if response returns a bad status code (i.e. 404/403).
    $headers = @get_headers($url) ?: [];
    $responseCode = substr($headers[0], 9, 3);
    if ($responseCode >= 400) {
      throw new \Exception(sprintf('Request for remote file responded with an invalid status code: %s', $responseCode));
    }
    // Get filename from URL.
    $parsed = parse_url($url);
    if (isset($parsed['query']) && substr($parsed['query'], 0, 3) == 'id=') {
      // Set the filename to the "id" parameter.
      $name =  substr_replace($parsed['query'], '', 0, 3);
    }
    else {
      // Generate a random string for filename.
      $name = md5(time());
    }
    $filename = sprintf('%s.%s', $name, $ext);

    // Set the file destination.
    $streamWrapper = 's3://';
    $dir = sprintf('%s/%s/images', $streamWrapper, str_replace('_image', '', $this->getBundle()));
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    // TODO: Use Dependency Injection.
    $file_system = \Drupal::service('file_system');
    $file_system->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS);
    $destination = sprintf('%s/%s', $dir, $filename);
    // Get remote file contents.
    // Note: using standard PHP function instead of Guzzle client or Drupal's system_retrieve_file() because
    // something about those clients are blocked by the SI Image Delivery Service.
    $contents = file_get_contents($url);
    if ($contents) {
      return file_save_data($contents, $destination , FileSystemInterface::EXISTS_REPLACE);
    }
    else {
      return FALSE;
    }
  }


}
