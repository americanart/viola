<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Paragraph;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for `text_component` paragraphs.
 *
 * @TypedRepository(
 *    entity_type_id = "paragraph",
 *    bundle = "text_component",
 *    wrappers = @ClassWithVariants(
 *      fallback = "Drupal\viola_content\WrappedEntities\Paragraph\TextComponent",
 *    ),
 *   description = @Translation("Repository that holds logic for text components")
 * )
 */
final class TextComponentRepository extends TypedRepositoryBase {}
