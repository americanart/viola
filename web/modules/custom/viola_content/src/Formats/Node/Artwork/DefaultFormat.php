<?php

namespace Drupal\viola_content\Formats\Node\Artwork;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Artwork;

final class DefaultFormat extends FormatBase {

  public $accessionNumber;
  public $bundle;
  public $classifications;
  public array $citations = [];
  public $collection;
  public $collectionDescription;
  public $copyright;
  public $credit;
  public $dateDescription;
  public $depictions;
  public $description;
  public $dimensionsDescription;
  public $exhibitionHistory;
  public $keywords;
  public array $genres = [];
  public $guid;
  public $hasUnidentifiedCreator;
  public $isNewAcquisition;
  public $isOnView;
  public $isOpenAccess;
  public $isPermanentCollection;
  public $locationDescription;
  public $longTitle;
  public $markings;
  public $mediums;
  public $mediumsDescription;
  public array $movements = [];
  public $objectId;
  public $ontology;
  public $path;
  public $periods;
  public $title;
  public $translatedDescriptions = [];
  public $translatedTitle;
  public $usageStatement;
  public array $colors = [];
  public array $creators = [];
  // TODO: Handle field_content, field_media.
  public array $dateRange = [];
  public array $images = [];
  public array $media = [];
  public array $usageRights;

  /**
   * Node\Artwork\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Artwork $artwork
   *   The wrapped entity.
   */
  public function __construct(Artwork $artwork) {
    parent::__construct();
    $this->bundle = $artwork->getBundle();
    $this->accessionNumber = $artwork->getAccessionNumber();
    $this->citations = $artwork->getCitations();
    $this->classifications = $artwork->getClassifications();
    $this->collection = $artwork->getCollection();
    $this->creators = $artwork->getCreators();
    $this->copyright = $artwork->getCopyright();
    $this->credit = $artwork->getCredit();
    $this->colors = $artwork->getColors();
    $this->collectionDescription = $artwork->getCollectionDescription();
    $this->dateDescription = $artwork->getDateDescription();
    $this->dateRange = $artwork->getDateRange();
    $this->depictions = $artwork->getDepictions();
    $this->description = $artwork->getDescription();
    $this->dimensionsDescription = $artwork->getDimensionsDescription();
    $this->exhibitionHistory = $artwork->getExhibitionHistory();
    $this->genres = $artwork->getGenres();
    $this->guid = $artwork->getGuid();
    $this->hasUnidentifiedCreator = $artwork->hasUnidentifiedCreator();
    $this->images = $artwork->getImages();
    $this->locationDescription = $artwork->getLocationDescription();
    $this->movements = $artwork->getMovements();
    $this->markings = $artwork->getMarkings();
    $this->mediumsDescription = $artwork->getMediumsDescription();
    $this->ontology = $artwork->getOntology();
    $this->isOpenAccess = $artwork->isOpenAccess();
    $this->title = $artwork->getTitle();
    $this->path = $artwork->getPathAlias();
    $this->periods = $artwork->getPeriods();
    $this->longTitle = $artwork->getLongTitle();
    $this->objectId = $artwork->getObjectId();
    $this->isOnView = $artwork->isOnView();
    $this->isNewAcquisition = $artwork->isNewAcquisition();
    $this->isPermanentCollection = $artwork->isPermanentCollection();
    $this->translatedDescriptions = $artwork->getTranslatedDescriptions();
    $this->translatedTitle = $artwork->getTranslatedTitle();
    $this->usageRights = $artwork->getUsageRights();
    $this->usageStatement = $artwork->getUsageStatement();
    $this->uuid = $artwork->getUuid();
  }

}
