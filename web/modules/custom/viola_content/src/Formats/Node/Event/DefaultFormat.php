<?php

namespace Drupal\viola_content\Formats\Node\Event;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Event;

final class DefaultFormat extends FormatBase {

  public $accessibility;
  public $actionsUrl;
  public $bundle;
  public array $categories = [];
  public $categoriesDescription;
  public $coSponsor;
  public $costDescription;
  public $dateDescription;
  public $dateRange;
  public $description;
  public $dwellTime;
  public array $images = [];
  public $locationDescription;
  public $path;
  public $registrationUrl;
  public $summary;
  public $ticketsUrl;
  public $title;
  public $uuid;
  public $venue;
  public $webcastUrl;

  /**
   * Node\Event\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Event $event
   *   The wrapped entity.
   */
  public function __construct(Event $event) {
    parent::__construct();
    $this->accessibility = $event->getAccessibility();
    $this->actionsUrl = $event->getActionsUrl();
    $this->bundle = $event->getBundle();
    $this->categories = $event->getCategories();
    $this->categoriesDescription = $event->getCategoriesDescription();
    $this->coSponsor = $event->getCoSponsor();
    $this->costDescription = $event->getCostDescription();
    $this->dateRange = $event->getDateRange();
    $this->dateDescription = $event->getDateDescription();
    $this->description = $event->getDescription();
    $this->dwellTime = $event->getDwellTime();
    $this->locationDescription = $event->getLocationDescription();
    $this->path = $event->getPathAlias();
    $this->registrationUrl = $event->getRegistrationUrl();
    $this->summary = $event->getSummary();
    $this->ticketsUrl = $event->getTicketsUrl();
    $this->title = $event->getTitle();
    $this->uuid = $event->getUuid();
    $this->venue = $event->getVenue();
    $this->webcastUrl = $event->getWebcastUrl();
    // Get image data.
    $wrapped = $event->getImages();
    foreach($wrapped as $image) {
      $this->images[] = $image->transformInto('default');
    }
  }

}
