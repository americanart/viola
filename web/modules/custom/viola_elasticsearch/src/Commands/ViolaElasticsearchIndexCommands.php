<?php

namespace Drupal\viola_elasticsearch\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_elasticsearch\Elastic\Index;
use Drupal\viola_elasticsearch\Elastic\Indexer;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaElasticsearchIndexCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Indexer
   */
  protected $indexer;

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Index
   */
  protected $index;

  /**
   * Class constructor.
   *
   * @param \Drupal\viola_elasticsearch\Elastic\Indexer $indexer
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   */
  public function __construct(Indexer $indexer, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->indexer = $indexer;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->index = new Index();
  }

  /**
   * Add or update Drupal entities as documents in the Elasticsearch index.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option index
   *   The index name to perform actions upon.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @option size
   *   The batch size when performing bulk operations.
   * @option ids
   *   A comma-separated list of specific Node ids to import.
   * @option all
   *   Index all content.
   * @usage viola-elasticsearch:index
   *
   * @command viola-elasticsearch:index
   * @aliases viola-es:index
   */
  public function index($type = '', $options = [
    'index' => 'catalog',
    'limit' => 100000,
    'offset' => 0,
    'size' => 100,
    'ids' => '',
    'all' => FALSE,
  ]) {
    $this->logger()->notice(dt("Running viola-elasticsearch:index ..."));
    $indexName = $options['index'] ?? $this->index->getIndexName();
    // Set the bundles if supplied as an option or use all allowed bundles as default.
    $bundles = $this->indexer->nodeBundles;
    $bundle = mb_strtolower($type);
    if ($bundle && in_array($bundle, $this->indexer->nodeBundles)) {
      $bundles = [$bundle];
    }
    // Perform action on specific ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    if ($options['all'] && PHP_SAPI === 'cli') {
      // Execute this command for each content type.
      $command = \Drupal::service('viola_elasticsearch.index.commands');
      $command->index('creator');
      // Import artworks in chunks.
      $command->index('artwork', ['limit' => 10000, 'size' => 100]);
      $command->index('artwork', ['limit' => 10000, 'offset' => 9999, 'size' => 100]);
      $command->index('artwork', ['limit' => 10000, 'offset' => 19999, 'size' => 100]);
      $command->index('artwork', ['limit' => 10000, 'offset' => 29999, 'size' => 100]);
      $command->index('artwork', ['offset' => 39999, 'size' => 100]);
      $command->index('exhibition');
      $command->index('page');
    }
    elseif ($ids) {
      // Index specific ids.
      $this->indexer->bulkOperation($ids, 'index', $indexName);
      $this->logger()->success(
        dt('Queued @count item(s) for indexing', ['@count' => count($ids)])
      );
    }
    else {
      // Index content.
      foreach ($bundles as $bundle) {
        $this->logger()->notice(dt('Indexing @bundle content ...', ['@bundle' => $bundle]));
        $nids = $this->indexer->queryNodes($bundle, $options['offset'], $options['limit']);
        $total = count($nids);
        $chunks = array_chunk($nids, $options['size']);
        unset($nids);
        foreach ($chunks as $key => $nids) {
          $this->indexer->bulkOperation($nids, 'index', $indexName);
          unset($chunks[$key]);
        }
        $this->logger()->success(
          dt('Queued @count item(s) for indexing', ['@count' => $total])
        );
      }
    }
  }

  /**
   * Index custom routes.
   *
   * @option index
   *   The index name to perform actions upon.
   * @usage viola-elasticsearch:index-routes
   *
   * @command viola-elasticsearch:index-routes
   * @aliases viola-es:index-routes
   */
  public function indexRoutes($options = ['index' => 'catalog']) {
    $this->logger()->notice(dt("viola-elasticsearch:index-routes ..."));
    $indexName = $options['index'] ?? $this->index->getIndexName();
    // TODO: Create a Drupal configuration form for the "custom routes" to index? (or a YML file?)
    // Custom routes we want to index.
    $routes = [
      [
        'title' => 'Art + Artists',
        'path' => '/art',
        'uuid' => 'efd285c5-381d-44f2-be3c-66a8c22d5f4b',
        'summary' => 'Browse artworks and artists featured in the collection',
        'contents' => '',
      ],
      [
        'title' => 'Events',
        'path' => '/events',
        'uuid' => 'd19ae1f7-b9a1-420a-af42-b1d2d528f104',
        'summary' => 'Calendar of events',
        'contents' => '',
      ],
      [
        'title' => 'Exhibitions',
        'path' => '/events/exhibitions',
        'uuid' => '812d362d-4d8c-41b5-80cc-1cbb6272045b',
        'summary' => 'Current exhibitions',
        'contents' => '',
      ],
    ];
    $count = 0;
    foreach ($routes as $route) {
      if ($route['uuid']) {
        // The bulk operation index target and settings.
        $params = [
          'index' => $indexName,
          'refresh' => TRUE,
        ];
        // The action for this item.
        $params['body'][] = [
          'index' => [
            '_id' => $route['uuid'],
          ]
        ];
        // Attach metadata for the document.
        $body = [
          '@timestamp' => time(),
          'type' => 'page',
          'uuid' => $route['uuid'],
          'title' => $route['title'],
          'path' => $route['path'],
          'summary' => $route['summary'],
          'contents' => $route['contents'],
        ];
        $params['body'][] = $body;
        $this->indexer->queueBulkJob($params);
        $count++;
      }
    }
    $this->logger()->success(
      dt('Queued @count route(s) for indexing', ['@count' => $count])
    );
  }

}
