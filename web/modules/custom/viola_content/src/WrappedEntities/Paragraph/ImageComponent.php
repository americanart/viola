<?php

namespace Drupal\viola_content\WrappedEntities\Paragraph;

use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the `image_component` paragraph.
 */
class ImageComponent extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;

  /**
   * @return string
   */
  public function getAspectRatio() {
    return $this->getEntity()->get('field_aspect_ratio')->value ?? '';
  }

  /**
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   */
  public function getImage() {
    $images = $this->wrapReferencedEntites($this->getEntity(), 'field_image');
    return reset ($images);
  }

  /**
   * @return boolean
   */
  public function includeCaption() {
    return (bool) $this->getEntity()->get('field_include_caption')->value;
  }

  /**
   * @return string
   */
  public function getText() {
    return $this->getEntity()->get('field_text')->value ?? '';
  }

}
