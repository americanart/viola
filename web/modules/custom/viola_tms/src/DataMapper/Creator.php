<?php

namespace Drupal\viola_tms\DataMapper;

use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\DataMapper\DataMapper;
use Drupal\viola_core\Utils\Markup;
use Drupal\viola_core\DataMapper\Traits\HasDateRangeTrait;
use Drupal\viola_tms\DataMapper\Traits\HasThesaurusTermsTrait;
use Drupal\viola_core\Utils\Strings;

/**
 * Creator DataMapper
 *
 * Map data from a TMS Extractor Export to Node Creator fields.
 *
 */
class Creator extends DataMapper {

  use HasThesaurusTermsTrait;
  use HasDateRangeTrait;

  /**
   * @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository;
   */
  protected $termRepository;

  /**
   * @param array $field_data
   */
  public function __construct($field_data = []) {
    $this->termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
    $this->setData($field_data);
  }

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {
    $this->data = [];
    if (isset($field_data['force'])) {
      $this->data['force'] = TRUE;
    }
    unset($field_data['force']);
    // Checksum
    $this->data['field_checksum'] = DataMapper::generateChecksum($field_data);
    // Constituent ID.
    if (self::isValidDataKey('constituentId', $field_data)) {
      $this->data['field_constituent_id'] = $field_data['constituentId'];
    }
    // Title.
    if (self::isValidDataKey('displayName', $field_data)) {
      $title = trim(Strings::decodeSpecialChars($field_data['displayName']));
      // Remove brackets from Title.
      $title = str_replace(array('[',']'), '', $title);
      $this->data['title'] = $title ?? 'Untitled';
      unset($field_data['displayName']);
    }
    // Alphabetical Name.
    if (self::isValidDataKey('alphaSort', $field_data)) {
      $this->data['field_alphabetical_name'] = $field_data['alphaSort'];
      unset($field_data['alphaSort']);
    }
    // Creator Type.
    if (self::isValidDataKey('constituentType', $field_data)) {
      $type = trim(mb_strtolower($field_data['constituentType']));
      if (in_array($type, ['individual', 'institution'])) {
        $tid = $this->termRepository->getTermIdByName('creator_types', ucfirst($type));
        if ($tid) {
          $this->data['field_creator_type'] = $tid;
        }
        unset($field_data['constituentType']);
      }
    }
    // Date Description.
    if (self::isValidDataKey('displayDate', $field_data)) {
      $this->data['field_date_description'] = $field_data['displayDate'];
      unset($field_data['displayDate']);
    }
    // Date of Birth.
    if (self::isValidDataKey('birthDate', $field_data)) {
      if ($data = \DateTime::createFromFormat('Y-n-j', $field_data['birthDate'])) {
        $this->data['field_date_of_birth']['value'] = $data->format('Y-m-d');
      }
      unset($field_data['birthDate']);
    }
    // Date of Death.
    if (self::isValidDataKey('deathDate', $field_data)) {
      if ($data = \DateTime::createFromFormat('Y-n-j', $field_data['deathDate'])) {
        $this->data['field_date_of_death']['value'] = $data->format('Y-m-d');
      }
      unset($field_data['deathDate']);
    }
    // Date Range.
    if (self::isValidDataKey('searchBeginDate', $field_data) && self::isValidDataKey('searchEndDate', $field_data)) {
      if ($range = $this->getDateRangeValuesFromYears($field_data['searchBeginDate'] , $field_data['searchEndDate'] )) {
        $this->data['field_date_range'] = $range;
        unset($field_data['searchBeginDate'], $field_data['searchEndDate']);
      }
    }
    // Associated States.
    $this->data['field_associated_places'] = [];
    if (isset($field_data['lastKnownPlace']) && is_array($field_data['lastKnownPlace'])) {
      foreach ($field_data['lastKnownPlace'] as $place) {
        $tid = $this->getStateTermIdByPlace($place);
        $this->data['field_associated_places'][] = $tid;
      }
    }
    if (isset($field_data['associatedPlaces']) && is_array($field_data['associatedPlaces'])) {
      foreach ($field_data['associatedPlaces'] as $place) {
        $tid = $this->getStateTermIdByPlace($place);
        $this->data['field_associated_places'][] = $tid;
      }
    }
    // Birth Place.
    if (isset($field_data['birthPlace']) && is_array($field_data['birthPlace'])) {
      if ($birthPlace = trim(reset($field_data['birthPlace']))) {
        $tid = $this->getStateTermIdByPlace($birthPlace);
        if ($tid) {
          $this->data['field_birth_place'] = $tid;
        }
      }
    }
    // Place of Death.
    if (isset($field_data['deathPlace']) && is_array($field_data['deathPlace'])) {
      if ($deathPlace = trim(reset($field_data['deathPlace']))) {
        $tid = $this->getStateTermIdByPlace($deathPlace);
        if ($tid) {
          $this->data['field_place_of_death'] = $tid;
        }
      }
    }
    // Gender. Gender is populated from a TMS Xref topics of type "Gender".
    if (self::isValidDataKey('gender', $field_data)) {
      foreach($field_data['gender'] as $term) {
        if (isset($term['pathTerm'])) {
          $gender = ucfirst($this->getPathTermWord($term['pathTerm']));
          $tid = $this->termRepository->getTermIdByName('genders', $gender);
          if ((int) $tid > 0) {
            $this->data['field_gender'][] = $tid;
            unset($field_data['gender']);
          }
        }
      }
    }
    // Collection Description.
    $biographyFieldNames = ['primaryArtistBiography', 'artistBiography', 'luceArtistBiography'];
    foreach ($biographyFieldNames as $fieldName) {
      if (isset($field_data[$fieldName])) {
        $this->data['field_collection_description'] = [
          'value' => trim(Markup::clean($field_data[$fieldName])),
          'format' => 'full_html',
        ];
        unset($field_data[$fieldName]);
        break;
      }
    }
    // Groups.
    if (self::isValidDataKey('affiliation', $field_data) && !empty($field_data['affiliation'])) {
      $prefix = 'Authorities\\Attributes\\Constituents\\Affiliation\\';
      $tids = $this->getTermIdsForPathTerms($field_data['affiliation'], $prefix, 'groups');
      if ($tids) {
        $this->data['field_groups'] = array_unique($tids);
      }
      unset($field_data['affiliation']);
    }
    // Ethnicities.
    if (self::isValidDataKey('ethnicity', $field_data) && !empty($field_data['ethnicity'])) {
      $prefix = 'Authorities\\Attributes\\Constituents\\Ethnicity\\';
      $tids = $this->getTermIdsForPathTerms($field_data['ethnicity'], $prefix, 'ethnicities');
      if ($tids) {
        $this->data['field_ethnicities'] = array_unique($tids);
      }
      unset($field_data['ethnicity']);
    }
    // Nationalities.
    if (self::isValidDataKey('nationality', $field_data) && !empty($field_data['nationality'])) {
      if ($nationality = trim($field_data['nationality'])) {
        $tid = $this->termRepository->getTermIdByName('nationalities', $field_data['nationality']);
        if ($tid) {
          $this->data['field_nationalities'] = $tid;
        }
      }
      unset($field_data['nationality']);
    }
    // Occupations.
    if (self::isValidDataKey('occupation', $field_data) && !empty($field_data['occupation'])) {
      $prefix = 'Authorities\\Attributes\\Constituents\\Occupation\\';
      $tids = $this->getTermIdsForPathTerms($field_data['occupation'], $prefix, 'occupations');
      if ($tids) {
        $this->data['field_occupations'] = array_unique($tids);
      }
      unset($field_data['occupation']);
    }

    // Data JSON field. (Remaining fields are added as JSON data keyed by source).
    $data = ['tms' => $field_data];
    $this->data['field_data'] = json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
  }

  /**
   * Get the Taxonomy Term ID of string parse from a Thesaurus PathTerm with
   *   a given prefix path.
   *
   * @param array $pathTerms
   * @param string $prefix
   *   The path to a specific Thesaurus term.
   * @param string $vid
   *   The Vocabulary machine name.
   *
   * @return array
   */
  public function getTermIdsForPathTerms($pathTerms, $prefix, $vid) {
    $tids = [];
    foreach($pathTerms as $term) {
      $parts = $this->getPathTermParts($term, $prefix);
      foreach ($parts as $part) {
        $tid = $this->termRepository->getTermIdByName($vid, $part);
        if ((int) $tid > 0) {
          $tids[] = $tid;
        }
      }
    }
    return $tids;
  }

    /**
   * Get the Taxonomy Term ID of a state contained in a place string.
   *   Example place: "Philadelphia,  Pennsylvania, United States".
   *
   * @return int
   *   The U.S. State Taxonomy Term ID.
   */
  public function getStateTermIdByPlace($place) {
    $values = explode(',', $place);
    $country = trim(end($values));
    if (in_array($country, ['United States', 'Puerto Rico'])) {
      // Find the state term id.
      $value = array_slice($values, -2, 1);
      $state = trim($value[0]);
      if ($country == 'Puerto Rico') {
        // Special case for Puerto Rico territory, which is categorized as a U.S. State.
        $state = 'Puerto Rico';
      }
      $tid = $this->termRepository->getTermIdByName('states', $state);
      if ($tid) {
        return $tid;
      }
    }
    return 0;
  }

}
