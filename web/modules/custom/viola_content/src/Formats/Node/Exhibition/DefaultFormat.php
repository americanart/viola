<?php

namespace Drupal\viola_content\Formats\Node\Exhibition;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Exhibition;

final class DefaultFormat extends FormatBase {

  public $blurb;
  public $bundle;
  public $path;
  public $summary;
  public $subtitle;
  public $title;
  public $venues;
  public array $dataRange = [];
  public array $image = [];

  /**
   * Node\Exhibition\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Exhibition $exhibition
   *   The wrapped entity.
   */
  public function __construct(Exhibition $exhibition) {
    parent::__construct();
    $this->blurb = $exhibition->getBlurb();
    $this->bundle = $exhibition->getBundle();
    $this->dataRange = $exhibition->getDateRange();
    $this->path = $exhibition->getPathAlias();
    $this->summary = $exhibition->getSummary();
    $this->title = $exhibition->getTitle();
    $this->venues = $exhibition->getVenues();
  }

}
