<?php

namespace Drupal\viola_elasticsearch\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_elasticsearch\Elastic\EsClient;
use Drupal\viola_elasticsearch\Elastic\Index;
use Drush\Commands\DrushCommands;

class ViolaElasticsearchManageCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Elasticsearch\Client
   */
  protected $elasticClient;
  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Index
   */
  protected $index;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->elasticClient = EsClient::getInstance();
    $this->index = new Index();
  }

  /**
   * Manage an elasticsearch index.
   *
   * @param string $action
   *   The action to take. create, delete, open, or close.
   *
   * @option index
   *   The index name to perform actions upon.
   * @usage viola-elasticsearch:manage create
   *
   * @command viola-elasticsearch:manage
   * @aliases viola-es:manage
   */
  public function manage($action, $options = ['index' => 'catalog']) {
    $this->logger()->notice(dt("Running viola-elasticsearch:manage ..."));

    if (!in_array($action, ['create', 'delete', 'open', 'close'])) {
      $this->logger->error('Invalid action.');
    }
    $indexName = $options['index'] ?? $this->index->getIndexName();
    // If an index name was supplied as an argument, update the index object configuration.
    $this->index->setIndexName($indexName);
    switch (mb_strtolower($action)) {
      case 'create':
        $response = $this->elasticClient->indices()->create($this->index->getConfig());
        break;
      case 'delete':
        $response = $this->elasticClient->indices()->delete(['index' => $this->index->getIndexName()]);
        break;
      case 'open':
        $response = $this->elasticClient->indices()->open(['index' => $this->index->getIndexName()]);
        break;
      case 'close':
        $response = $this->elasticClient->indices()->close(['index' => $this->index->getIndexName()]);
        break;
    }

    if ($response and isset($response['acknowledged']) && $response['acknowledged']) {
      $this->logger()->success(
        dt('@action index: @index (executed)', ['@action' => ucfirst($action), '@index' => $this->index->getIndexName()])
      );
    }
    else {
      $this->logger()->alert(
        dt('@action index: @index (failed)', ['@action' => ucfirst($action), '@index' => $this->index->getIndexName()])
      );
    }
  }

}
