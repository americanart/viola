<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Paragraph;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for `image_component` paragraphs.
 *
 * @TypedRepository(
 *    entity_type_id = "paragraph",
 *    bundle = "image_component",
 *    wrappers = @ClassWithVariants(
 *      fallback = "Drupal\viola_content\WrappedEntities\Paragraph\ImageComponent",
 *    ),
 *   description = @Translation("Repository that holds logic for image components")
 * )
 */
final class ImageComponentRepository extends TypedRepositoryBase {}
