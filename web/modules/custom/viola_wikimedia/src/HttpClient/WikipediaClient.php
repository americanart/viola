<?php

namespace Drupal\viola_wikimedia\HttpClient;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A class to pull information about a topic from Wikipedia.
 */
class WikipediaClient {

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Construct a Wikipedia client object.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle client.
   */
  public function __construct(ClientInterface $httpClient) {
    $this->http_client = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Retrieve and decode the response.
   *
   * @param string $title
   *   A page title to retrieve.
   *
   * @return array
   *   A decoded array of data about the page.
   *
   * @see https://www.mediawiki.org/wiki/API:Query
   * @see http://docs.guzzlephp.org/en/latest/quickstart.html
   */
  public function getResponse($title) {
    $uri = 'https://en.wikipedia.org/w/api.php';
    $props = ['info', 'extracts'];
    $query = [
      'action' => 'query',
      'format' => 'json',
      'prop' => implode('|', $props),
      'inprop' => 'url',
      'exintro' => '',
      'redirects' => 1,
      'titles' => $title,
    ];
    $options = ['query' => $query, 'http_errors' => FALSE];
    try {
      if ($response = $this->http_client->request('GET', $uri, $options)) {
        if ($data = $response->getBody()->getContents()) {
          $data = json_decode($data, TRUE);
          if (array_key_exists('query', $data) && array_key_exists('pages', $data['query'])) {
            return array_shift($data['query']['pages']);
          }
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('wikipedia_client', $e);
    }
    return [];
  }
  /**
   * Helper to get the Wikidata entity data formatted for import.
   *
   * @param string $title
   *   A page title to retrieve.
   *
   * @return array
   *
   * @see https://www.wikidata.org/wiki/Property:P737
   */
  public function getData($title) {
    $result = [];
    $response = $this->getResponse($title);
    if ($response) {
      if ($pageId = $this->getPageId($response)) {
        $result['pageId'] = $pageId;
      }
      if ($url = $this->getCanonicalUrl($response)) {
        $result['url'] = $url;
      }
      if ($extract = $this->getExtract($response)) {
        $result['extract'] = $extract;
      }
    }
    return $result;
  }

  /**
   * Helper to clean up the markup that is returned.
   *
   * @param string $markup
   *   The text to clean.
   *
   * @return string
   *   Cleaned up markup.
   */
  public function clean($markup) {
    $cleaned = strip_tags($markup, ['<p>','<span>','<b>','<strong>',
      '<i>','<em>', '<br>','<cite>','<blockquote>','<cite>','<ul>','<ol>',
      '<li>','<dl>', '<dt>','<dd>']);
    // Remove empty elements.
    $cleaned = preg_replace(
      '/<(\w+)\b(?:\s+[\w\-.:]+(?:\s*=\s*(?:"[^"]*"|[\w\-.:]+))?)*\s*\/?>\s*<\/\1\s*>/',
      '',
      $cleaned
    );
    return trim($cleaned);
  }

  /**
   * Helper to get the extract of the Wikipedia page.
   *
   * @param array $wiki_data
   *   The data returned from Wikipedia.
   *
   * @return string
   *   Formatted and cleaned extract of the data.
   */
  public function getExtract($wiki_data) {
    if (array_key_exists('extract', $wiki_data)) {
      $extract = $wiki_data['extract'];
      $extract = $this->clean($extract);
      return $extract;
    }
    return '';
  }

  /**
   * Helper to get the Wikipedia page ID.
   *
   * @param array $wiki_data
   *   Data from Wikipedia.
   *
   * @return string
   *
   */
  public function getPageId($wiki_data) {
    if (array_key_exists('pageid', $wiki_data)) {
      return $wiki_data['pageid'];
    }
    return '';
  }

  /**
   * Helper to get the Wikipedia page URL.
   *
   * @param array $wiki_data
   *   Data from Wikipedia.
   *
   * @return string
   *
   */
  public function getCanonicalUrl($wiki_data) {
    if (array_key_exists('canonicalurl', $wiki_data)) {
      return $wiki_data['canonicalurl'];
    }
    return '';
  }

}
