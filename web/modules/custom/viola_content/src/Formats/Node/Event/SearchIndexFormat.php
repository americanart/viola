<?php

namespace Drupal\viola_content\Formats\Node\Event;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Event;

final class SearchIndexFormat extends FormatBase {

  /**
   * @var array
   */
  private $document = [];

  /**
   * Node\Event\SearchIndexFormat constructor.
   *   Format a Node for elasticsearch indexing.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Event $event
   *   The wrapped entity.
   */
  public function __construct(Event $event) {
    parent::__construct();
    $this->document['@timestamp'] = time();
    $this->document['contents'] = $event->getDescription();
    $this->document['categories'] = $event->getCategories();
    // Format date range for filtering.
    if ($dateRange = $event->getDateRange()) {
      if (isset($dateRange['start'], $dateRange['end'])) {
        $this->document['dateRange']['lte'] = $dateRange['end'];
        $this->document['dateRange']['gte'] = $dateRange['start'];
      }
    }
    $this->document['path'] = $event->getPathAlias();
    $this->document['summary'] = $event->getSummary();
    $this->document['title'] = $event->getTitle();
    $this->document['type'] = $event->getBundle();
    $this->document['venue'] = $event->getVenue();
  }

  /**
   * @return array;
   */
  public function transform() {
    return $this->document;
  }

}
