<?php

namespace Drupal\viola_content\WrappedEntities\Traits;

use Drupal\viola_core\Utils\DateTimes;

trait HasDateRangeTrait {

  public string $width;
  public string $height;
  public string $orientation;

  /**
   * Returns the formatted range values for two date strings.
   *
   * @param string $start
   * @param string $end
   *
   * @return array
   */
  public static function getDateRangeFromStrings($start, $end, $timezone = 'UTC') {
    $range = [];
    $start = DateTimes::getFormattedDateTime($start, $timezone);
    $end = DateTimes::getFormattedDateTime($end, $timezone);
    if ($start and $end) {
      $range = [
        'start' => $start,
        'end' => $end,
      ];
    }
    return $range;
  }

}
