<?php

namespace Drupal\viola_content\Formats\Media\Image;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Media\Image;

final class DefaultFormat extends FormatBase {

  public $alt;
  public $caption;
  public $height;
  public $id;
  public $orientation;
  public array $sizes;
  public $title;
  public $type;
  public $width;

  /**
   * Media\Image\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Media\Image $image
   *   The wrapped entity.
   */
  public function __construct(Image $image) {
    parent::__construct();
    $this->bundle = $image->getBundle();
    $this->type = $image->getEntity()->getEntityTypeId();
    $this->alt = $image->getAltText();
    $this->caption = $image->getCaption();
    $this->height = $image->getHeight();
    $this->id = $image->getDrupalId();
    $this->orientation = $image->getOrientation();
    $this->sizes = $image->getDerivativeImageUrls();
    $this->title = $image->getTitle();
    $this->width = $image->getWidth();
  }

}
