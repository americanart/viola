<?php

namespace Drupal\viola_content\ParamConverter;

use Drupal\Core\ParamConverter\EntityConverter;

/**
 * Parameter converter for upcasting a UUID to Entity objects.
 *
 * @code
 * example.route:
 *   path: api/node/{uuid}
 * @endcode
 */
class EntityUuidConverter extends EntityConverter {

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults) {
    var_dump($value);
    die();
    $entity_type_id = $this->getEntityTypeFromDefaults($definition, $name, $defaults);
    $uuid_key = $this->entityTypeManager->getDefinition($entity_type_id)
      ->getKey('uuid');
    if ($storage = $this->entityTypeManager->getStorage($entity_type_id)) {
      if (!$entities = $storage->loadByProperties([$uuid_key => $value])) {
        return NULL;
      }
      $entity = reset($entities);
      return $entity;
    }
    return NULL;
  }
}
