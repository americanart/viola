<?php

namespace Drupal\viola_content\Formats\Node\Page;

use Drupal\viola_content\Formats\FormatBase;
use Spatie\SchemaOrg\Schema;
use Drupal\viola_content\WrappedEntities\Node\Page;

final class JsonLdFormat extends FormatBase {

  /**
   * @var object \Spatie\SchemaOrg\Schema
   */
  private $schema;

  /**
   * Node\Page\JsonLdFormat constructor.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Page $page
   *   The wrapped entity.
   */
  public function __construct(Page $page) {
    parent::__construct();

    $this->schema = Schema::webPage()
      ->headline($page->getTitle())
      ->url($page->getPathAlias());
  }

  /**
   * @return string;
   */
  public function transform() {
    return $this->schema;
  }

}
