<?php

namespace Drupal\viola_wikimedia\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaWikimediaIdentifiersCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\viola_wikimedia\HttpClient\WikipediaClient
   */
  public $wikipedia;

  /**
   * @var int
   */
  public $queued;

  /**
   * The allowed import content types.
   *
   * @var array
   */
  protected $types = [
    'artwork',
    'creator',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->wikipedia = \Drupal::service('viola.wikipedia_client');
  }

  /**
   * Generate a file of Wikipedia "title" and "pageid" identifiers from Wikidata IDs.
   *
   * @param string $type
   *   The content type to target.
   *
   * @usage viola-wikimedia:generate-ids artwork
   *
   * @command viola-wikimedia:generate-ids
   * @aliases viola-wiki:gen-ids
   */
  public function generateIds($type) {
    $this->logger()->notice(dt("Running viola-wikimedia:generate-wikipedia-ids ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    if ($bundle == 'creator') {
      $identifier = 'field_constituent_id';
    }
    else {
      $identifier = 'field_object_id';
    }
    $connection = \Drupal::database();
    $query = $connection->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'type']);
    $query->condition('n.type', $bundle, '=');
    $query->condition('n.status', 1);
    // Add the Wikidata ID.
    $query->join('node__field_wikidata_id', 'fw', 'fw.entity_id = n.nid');
    $query->addField('fw', 'field_wikidata_id_value');
    // Add the SAAM identifier.
    $query->join(sprintf('node__%s', $identifier), 'fid', 'fid.entity_id = n.nid');
    $query->addField('fid', sprintf('%s_value', $identifier));
    $results = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
    if ($results) {
      // Retrieve and queue import results for processing.
      $items = [];
      foreach($results as $result) {
        $item = [];
        $item[str_replace('field', 'saam', $identifier)] = $result[sprintf('%s_value', $identifier)];
        $item['wikidata_id'] = $result['field_wikidata_id_value'];
        // Get the "Wikipedia Title" identifier.
        $reader = new JsonDataReader(
          'https://',
          'https://www.wikidata.org/w/api.php?action=wbgetentities&format=json&ids=' . $item['wikidata_id'] . '&sites=enwiki&redirects=yes&props=sitelinks%2Furls&languages=en&normalize=1&sitefilter=enwiki'
        );
        $result = $reader->read();
        if ($result && is_object($result) && property_exists($result, 'entities')) {
          $entity = reset($result->entities);
          if (property_exists($entity, 'type') && $entity->type === 'item') {
            if(isset($entity->sitelinks->enwiki)) {
              $site = (array) $entity->sitelinks->enwiki;
              if (isset($site['url']) && filter_var($site['url'], FILTER_VALIDATE_URL) !== FALSE) {
                // Get the (underscores) Wikipedia title from the English Wikipedia URL.
                $position = strrpos($site['url'], '/');
                $item['wikipedia_title'] = $position === false ? $site['url'] : substr($site['url'], $position + 1);
              }
            }
          }
        }
        // Use the Wikipedia Title to get the Wikipedia "pageid"
        // @see: https://stackoverflow.com/a/43748047/1353811
        $items[] = $item;
      }
      var_dump($items);
    }
    else {
      $this->logger()->warning('Nothing to generate.');
    }
  }

  /**
   * Queue imports of select Wikidata fields to store on content.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option ids
   *   A comma-separated list of specific drupal Node ID's to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @usage viola-wikimedia:wikidata-import
   * @option force
   *   Force import, bypassing the checksum comparison.
   *
   * @command viola-wikimedia:wikidata-import
   * @aliases viola-wiki:wikidata
   */
//  public function wikidataImport($type = '', $options = [
//    'ids' => '',
//    'limit' => 100000,
//    'offset' => 0,
//    'force' => FALSE,
//  ]
//  ) {
//    $this->logger()->notice(dt("Running viola-wikimedia:wikidata-import ..."));
//    $bundle = mb_strtolower($type);
//    if (!in_array($bundle, $this->types)) {
//      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
//      throw new \InvalidArgumentException($message);
//    }
//    if ($bundle == 'creator') {
//      $identifier = 'field_constituent_id';
//    }
//    else {
//      $identifier = 'field_object_id';
//    }
//    $connection = \Drupal::database();
//    $query = $connection->select('node_field_data', 'n');
//    $query->fields('n', ['nid', 'type']);
//    $query->condition('n.type', $bundle, '=');
//    $query->condition('n.status', 1);
//    $query->join(sprintf('node__%s', $identifier), 'fid', 'fid.entity_id = n.nid');
//    $query->addField('fid', sprintf('%s_value', $identifier));
//    $query->join('node__field_wikidata_id', 'fw', 'fw.entity_id = n.nid');
//    $query->addField('fw', 'field_wikidata_id_value');
//    // Filter results by supplied ids.
//    $ids = StringUtils::csvToArray($options['ids']) ?? [];
//    if ($ids) {
//      // Add condition for supplied Node ID(s).
//      $query->condition('n.nid', $ids, 'IN');
//    }
//    if ($options['limit'] || $options['offset']) {
//      $query->range($options['offset'], $options['limit']);
//    }
//    $results = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
//    if ($results) {
//      $batch = [
//        'title' => dt('Import processing ...'),
//        'operations' => [],
//        'progress_message' => dt('Queued @current out of @total.'),
//        'finished' => 'Drupal\viola_wikimedia\BatchService::batchFinished',
//      ];
//      foreach ($results as $result) {
//        if ($options['force']) {
//          $result['force'] = TRUE;
//        }
//        // Prepare the operations, keyed by process method call.
//        $batch['operations'][] = [
//          'Drupal\viola_wikimedia\BatchService::batchProcessWikidata', [
//            $result,
//            dt('Queued import for @bundle with ID: @id and Wikidata ID: @wikidata_id', [
//              '@bundle' => $bundle,
//              '@id' => $result[sprintf('%s_value', $identifier)],
//              '@wikidata_id' => $result['field_wikidata_id_value'],
//            ]),
//          ],
//        ];
//      }
//      // Add batch operations as new batch sets.
//      batch_set($batch);
//      // Process the batch sets.
//      drush_backend_batch_process();
//    }
//    else {
//      $this->logger()->warning('Nothing to import.');
//    }
//  }

}
