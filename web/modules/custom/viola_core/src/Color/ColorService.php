<?php

namespace Drupal\viola_core\Color;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use AmericanArt\Studio\Colors\Palettes\Css4;
use AmericanArt\Studio\Colors\ColorPicker;
use AmericanArt\Studio\Colors\Color;

/**
 * Color Utilities Service class.
 */
class ColorService implements ColorServiceInterface {

  /**
   * LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var array
   */
  public $defaultPalette = [];

  /**
   * @var ColorPicker
   */
  public $colorPicker;

  /**
   * ColorService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The Drupal logger channel factory.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactory $logger_factory) {
    $this->logger = $logger_factory->get('viola_core');
    $this->entityTypeManager = $entity_type_manager;
    $this->defaultPalette = Css4::getColors();
    $this->colorPicker = new ColorPicker($this->defaultPalette);
  }

  /**
   * Get the human name of the closest color to a given Hex color.
   *
   * @param string $hex
   *   The hexadecimal color.
   *
   * @return string
   *   The english color name.
   */
  public function getColorNameFromHex($hex) {
    $this->colorPicker->setPalette($this->defaultPalette);
    $color = new Color($this->colorPicker::fromHexToInt($hex));
    $closest = $this->colorPicker->closestColor($color);
    return $this->colorPicker->getColorName($closest);
  }

  /**
   * Get a taxonomy term id of color name closest to a given hex color.
   *
   * @param string $hex
   *   The color.
   * @param string $vid
   *   The vocabulary ID (machine name).
   *
   * @return int
   *   The numeric Term ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getColorTermIdFromHex($hex, $vid = 'colors') {
    $name = $this->getColorNameFromHex($hex);
    $results = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadByProperties(
        [
          'vid' => $vid,
          'name' => $name,
        ]
      );
    $ids = array_keys($results);
    return reset($ids);
  }

  /**
   * {@inheritdoc}
   */
  public function getColorsFromFile($image_url, $num_colors = 4) {
    $this->colorPicker->setPaletteFromFile($image_url, $num_colors);
    return $this->colorPicker->getPalette();
  }

}
