<?php

namespace Drupal\viola_content\WrappedEntities\Paragraph;

use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the `text_component` paragraph.
 */
class TextComponent extends TransformableWrappedEntity {

  /**
   * @return string
   */
  public function getText() {
    return $this->getEntity()->get('field_text')->value ?? '';
  }

}
