<?php

namespace Drupal\viola_migrate\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drush\Commands\DrushCommands;

class ViolaMigrateMenuCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The path to data directory.
   *
   * @var string
   */
  protected $dataDirectory;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->dataDirectory = dirname(__DIR__, 2) . '/data';
  }

  /**
   * Import menu link content for a given menu..
   *
   * @param string $menu
   *   The menu machine name.
   *
   * @usage viola-migrate:menu main
   *
   * @command viola-migrate:menu-import
   * @aliases viola-migrate:menu
   */
  public function importMenu($menu) {
    $this->logger()->notice(dt("Running viola-migrate:menu-import ..."));

    // Get import file.
    $menu = mb_strtolower($menu);
    $importFile = sprintf('%s/%s/%s.%s', $this->dataDirectory, 'menus', $menu, 'json');
    $reader = new JsonDataReader('file://', $importFile);
    $data = $reader->read()->getResults();
    if (!empty($data)) {
      /** @var \Drupal\viola_content\Plugin\TypedRepositories\MenuLinkContentRepository $repository */
      $repository = \Drupal::service(RepositoryManager::class)->repository('menu_link_content');

      foreach ($data as $item) {
        if ($repository->findByTitle($item['title'], $menu)) {
          $this->logger()->notice(dt('Skipped: @menu menu item `@title` already exists', ['@menu' => $menu, '@title' => $item['title']]));
        }
        else {
          $item['menu_name'] = $menu;
          $status = $repository->createLink($item);
          if ($status === SAVED_NEW) {
            $this->logger()->notice(dt('Created: @menu menu item `@title`', ['@menu' => $menu, '@title' => $item['title']]));
          }
        }
      }
    }
    else {
      $this->logger()->notice('Nothing to import.');
    }
  }

}
