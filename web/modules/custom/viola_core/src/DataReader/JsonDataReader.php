<?php

namespace Drupal\viola_core\DataReader;

use Drupal\viola_core\Utils\Files;

/**
 * Read items from a JSON file.
 */
class JsonDataReader extends DataReader implements DataReaderInterface {

  /**
   * Read data.
   *
   * @return JsonDataReader
   */
  public function read() {
    try {
      $content = '';
      if ($this->getWrapper() == "file://") {
        $content = Files::loadFile($this->getTarget());
      }
      elseif (in_array($this->getWrapper(), ['http://', 'https://'])) {
        $options = [
          'headers' => [
            'Accept' => 'application/json',
          ],
        ];
        $response = Http::getContents($this->getTarget(), $options);
        $content = $response->getBody();
      }
      elseif ($this->getWrapper() == "s3://") {
        $s3reader = new S3();
        $content = $s3reader::getContents($this->getTarget());
      }
      $results = json_decode($content, TRUE);
      if ($results == NULL || json_last_error() !== JSON_ERROR_NONE) {
        throw new \Exception('Empty or Invalid JSON');
      }
      if ($this->getLimit() > 0) {
        $results = array_slice($results, $this->getOffset(), $this->getLimit());
      }
      $this->setResults($results);
    }
    catch (\Exception $e) {
      \Drupal::logger('json_reader')->error(sprintf('Error: %s', $e->getMessage()));
    }
    return $this;
  }

}
