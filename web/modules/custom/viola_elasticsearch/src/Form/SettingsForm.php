<?php

namespace Drupal\viola_elasticsearch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\viola_core\SystemInfo;

/**
 * Provides a form for administering viola settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'viola_elasticsearch_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'viola_elasticsearch.settings'
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $settings = $this->config('viola_elasticsearch.settings');

    $form['content_types_enabled'] = [
      '#type' => 'checkboxes',
      '#multiple' => TRUE,
      '#title' => $this->t('Enabled content types'),
      '#options' => SystemInfo::getAllContentTypes(),
      '#description' => $this->t(
        'The content types that should be indexed by Elasticsearch. When a
        content item from one of these types is created, edited, or deleted a job
        to index that content will be pushed to the <em>viola_elasticsearch</em>
        queue.'
      ),
      '#default_value' => $settings->get('content_types_enabled') ?: [],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('viola_elasticsearch.settings')
      ->set('content_types_enabled', $form_state->getValue('content_types_enabled'))
      ->save();
  }

}
