<?php

namespace Drupal\viola_elasticsearch\Elastic;

use Elasticsearch\ClientBuilder;
use Aws\ElasticsearchService\ElasticsearchPhpHandler;
use Aws\Credentials\CredentialProvider;
use Aws\Credentials\Credentials;
use Exception;

class EsClient {
  /**
   * The singleton Client instance.
   *
   * @var \Elasticsearch\Client
   */
  private static $instance;

  /**
   * Disable instantiation.
   */
  private function __construct() {}

  /**
   * Create or retrieve the instance.
   *
   * @return \Elasticsearch\Client
   *   Return the current instance of EsClient.
   * @throws Exception
   */
  public static function getInstance() {
    if (!static::$instance) {
      if (!$host = $_SERVER['ELASTICSEARCH_HOST']) {
        throw new Exception('Elasticsearch host is not configured');
      }
      if (strpos($host, 'amazonaws.com')) {
        // If an AWS host, add credentials.
        $provider = CredentialProvider::fromCredentials(
          new Credentials($_SERVER['AWS_KEY'], $_SERVER['AWS_SECRET'])
        );
        $handler = new ElasticsearchPhpHandler($_SERVER['AWS_ELASTICSEARCH_REGION'], $provider);
        static::$instance = ClientBuilder::create()
          ->setHandler($handler)
          ->setHosts([$host])
          ->build();
      }
      else {
        static::$instance = ClientBuilder::create()->setHosts([$host])->build();
      }
    }
    return static::$instance;
  }

  /**
   * Disable the cloning of this class.
   *
   * @throws \Exception
   */
  final public function __clone() {
    throw new Exception('Feature disabled.');
  }

  /**
   * Disable the wakeup of this class.
   *
   * @throws \Exception
   */
  final public function __wakeup() {
    throw new Exception('Feature disabled.');
  }

}
