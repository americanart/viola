<?php

namespace Drupal\viola_core\Utils;

class Files
{

    /**
     * @param  $filename
     *   Path to the file.
     * @return mixed
     * @throws \Exception
     */
    public static function loadFile($filename)
    {
        if (!empty($filename) && is_file($filename) && file_exists($filename)) {
            $file = file_get_contents($filename);
        }
        return $file ?? null;
    }
}
