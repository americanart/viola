<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

/**
 * A Query interface.
 */
interface QueryDslInterface {

  /**
   * Get a complete Query DSL object.
   *
   * @return array
   */
  public static function build();

}
