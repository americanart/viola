<?php

namespace Drupal\viola_core\Commands;

use Drupal\advancedqueue\Job;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drush\Commands\DrushCommands;

class ViolaCoreAdvancedqueueCommands extends DrushCommands {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Allowed job states.
   *
   * @var array
   */
  protected $jobStates = [
    Job::STATE_QUEUED,
    Job::STATE_PROCESSING,
    Job::STATE_SUCCESS,
    Job::STATE_FAILURE,
  ];

  /**
   * ViolaCoreAdvancedqueueCommands constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, Connection $database) {
    parent::__construct();
    $this->entityTypeManager = $entity_type_manager;
    $this->database = $database;
  }

  /**
   * Clear a queue.
   *
   * @param string $queue_id
   *   The queue ID (machine name).
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option state
   *   The job state.
   * @usage viola-core:clear-queue
   *   Usage description
   *
   * @command viola-core:clear-queue
   * @aliases vc:clear-queue
   */
  public function clear($queue_id, $options = ['state' => '']) {
    $queue_storage = $this->entityTypeManager->getStorage('advancedqueue_queue');
    /** @var \Drupal\advancedqueue\Entity\QueueInterface $queue */
    $queue = $queue_storage->load($queue_id);
    if (!$queue) {
      throw new \RuntimeException(dt(
        'Could not find queue "@queue".',
          [
            '@queue' => $queue_id
          ]
        )
      );
    }
    $state = mb_strtolower($options['state']);
    if ($state && !in_array($state, $this->jobStates)) {
      throw new \RuntimeException(dt(
          'Unknown job state "@state".',
          [
            '@state' => $options['state']
          ]
        )
      );
    }
    $query = $this->database->delete('advancedqueue');
    $query->condition('queue_id', $queue_id);
    if ($state) {
      $query->condition('state', $state, '=');
    }
    else {
      // All job states.
      $query->condition('state', $this->jobStates, 'IN');
    }
    $num_rows = $query->execute();
    if ($num_rows) {
      $this->logger->success(
        dt(
          'Finished. Removed @count jobs from the @queue queue',
          [
            '@count' => $num_rows,
            '@queue' => $queue->get('label'),
          ]
        )
      );
    }
    else {
      $this->logger->notice(
        dt(
          'Finished. No jobs found in the "@queue" queue',
          [
            '@queue' => $queue->get('label'),
          ]
        )
      );
    }
  }

}
