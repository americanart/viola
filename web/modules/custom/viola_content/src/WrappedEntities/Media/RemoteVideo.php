<?php

namespace Drupal\viola_content\WrappedEntities\Media;

use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the remote video media.
 */
class RemoteVideo extends TransformableWrappedEntity {

  use HasCommonEntityFieldsTrait;
  use HasEntityReferencesTrait;

  const MEDIA_FIELD_NAME = 'field_media_oembed_video';

}
