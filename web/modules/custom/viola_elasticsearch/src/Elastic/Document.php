<?php

namespace Drupal\viola_elasticsearch\Elastic;

use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;

class Document {

  /**
   * Given a Node object return the Elasticsearch document.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *
   * @return array
   * @throws \Exception
   */
  public function __construct($node) {
    try {
      if (!$node instanceof NodeInterface) {
        throw new \Exception('Attempted to index invalid data.');
      }
      $index = new Index();
      $this->indexName = $index->getIndexName();
      $this->id = $node->uuid();
      $this->body = $this->getDocumentBodyFromNode($node);
      $this->document = [
        'id' => $this->id,
        'index' => $this->indexName,
        'body' => $this->body,
      ];
    }
    catch (\Exception $exception) {
      \Drupal::logger('viola_elasticsearch')->error($exception->getMessage());
    }
  }

  /**
   * @param \Drupal\node\NodeInterface $node
   *
   * @returns mixed
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  private function getDocumentBodyFromNode($node) {
    $wrapped = \Drupal::service(RepositoryManager::class)->wrap($node);
    return $wrapped->transformInto('search_index') ?? NULL;
  }

  /**
   * @var string
   */
  public $id;
  /**
   * @var string
   */
  public $indexName;
  /**
   * @var array
   */
  public $body = [];
  /**
   * @var array
   */
  public $document = [];

  /**
   * @return string
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId(string $id): void {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getIndex() {
    return $this->indexName;
  }

  /**
   * @param string $index
   */
  public function setIndex(string $index) {
    $this->indexName = $index;
  }

  /**
   * @return array
   */
  public function getBody() {
    return $this->body;
  }

  /**
   * @param array $body
   */
  public function setBody(array $body) {
    $this->body = $body;
  }

  /**
   * @return array
   */
  public function getDocument() {
    return $this->document;
  }

  /**
   * @param array $document
   */
  public function setDocument(array $document) {
    $this->document = $document;
  }

}
