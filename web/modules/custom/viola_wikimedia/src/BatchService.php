<?php

namespace Drupal\viola_wikimedia;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\viola_core\DataMapper\DataMapper;
use Drupal\viola_core\DataMapper\DataMapperFactory;
use Drupal\viola_core\Queue\QueueManager;

/**
 * Class BatchService.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Batch process callback for Wikidata Import.
   *
   * @param array $data
   *   Parsed Wikidata data for the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessWikidata($data, $operation_details, &$context) {
    try {
      $queued = FALSE;
      if ($data['type'] == 'creator') {
        $identifier = 'field_constituent_id';
      }
      else {
        $identifier = 'field_object_id';
      }
      $wikidata = \Drupal::service('viola.wikidata_client')
        ->getData($data['field_wikidata_id_value']);
      if ($wikidata) {
        $wikidata['checksum'] = DataMapper::generateChecksum($wikidata);
        // Attach the `field_data` keyed by source to payload.
        $payload = [
          'source' => 'wikidata',
          $identifier => $data[sprintf('%s_value', $identifier)],
          'field_data' => json_encode(['wikidata' => $wikidata], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT),
        ];
        if ($data['force']) {
          $payload['force'] = TRUE;
        }
        $queued = QueueManager::queueJob(
          'wikimedia_import_job',
          'update',
          'node',
          $data['type'],
          $payload,
          'viola_wikimedia'
        );
      }
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $data[sprintf('%s_value', $identifier)];
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch process callback for Wikipedia summary import.
   *
   * @param array $data
   *   Parsed Wikidata data for the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessWikipedia($data, $operation_details, &$context) {
    try {
      // Retrieve the page summary from Wikipedia.
      $queued = FALSE;
      if ($data['type'] == 'creator') {
        $identifier = 'field_constituent_id';
      }
      else {
        $identifier = 'field_object_id';
      }
      $wikipedia = \Drupal::service('viola.wikipedia_client')->getData($data['field_wikipedia_title_value']);
      if ($wikipedia) {
        $wikipedia['checksum'] = DataMapper::generateChecksum($wikipedia);
        // Attach the `field_data` keyed by source to payload.
        $payload = [
          'source' => 'wikipedia',
          $identifier => $data[sprintf('%s_value', $identifier)],
          'field_data' => json_encode(['wikipedia' => $wikipedia], JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT),
        ];
        if ($data['force']) {
          $payload['force'] = TRUE;
        }
        $queued = QueueManager::queueJob(
          'wikimedia_import_job',
          'update',
          'node',
          $data['type'],
          $payload,
          'viola_wikimedia'
        );
      }
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $data[sprintf('%s_value', $identifier)];
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(dt('Queued @count import jobs.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(dt('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
