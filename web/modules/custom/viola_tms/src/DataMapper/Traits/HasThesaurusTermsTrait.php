<?php

namespace Drupal\viola_tms\DataMapper\Traits;

/**
 * Trait adding functionality for objects using Thesaurus Xref taxonomies.
 */
trait HasThesaurusTermsTrait {

  /**
   * Parse full Path Term, retrieving the last word in the path (lowercase).
   *
   * @param string $pathTerm
   *   Xref PathTerm. Example: "Authorities\Attributes\Objects\Medium\PAINT\WATERCOLOR"
   * @param string $separator
   *   The term delimiter character.
   * @return string
   */
  private function getPathTermWord($pathTerm, $separator = '\\') {
    $terms = explode($separator, $pathTerm);
    return strtolower(end($terms));
  }

  /**
   * Get an array of separate Taxonomy Term names from a TMS Thesaurus Term.
   *
   * @param array $pathTermObject
   * @param string $prefix
   * @return array
   */
  public function getPathTermParts($pathTermObject, $prefix) {
    if (strpos($pathTermObject['pathTerm'], $prefix) === 0) {
      $path = substr($pathTermObject['pathTerm'], strlen($prefix));
      return explode('\\', $path);
    }
    return [];
  }


}
