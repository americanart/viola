<?php

namespace Drupal\viola_content\WrappedEntities;

use Drupal\Core\Url;
use Drupal\typed_entity\WrappedEntities\WrappedEntityBase;

/**
 * The wrapped entity for the taxonomy terms.
 */
class TaxonomyTerm extends WrappedEntityBase {

  /**
   * @return string
   */
  public function getName() {
    return $this->getEntity()->label();
  }

  /**
   * @return integer
   */
  public function getDrupalId() {
    return $this->getEntity()->id();
  }

  /**
   * @return string
   */
  public function getPathAlias() {
    return Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $this->getDrupalId()])->toString();
  }

}
