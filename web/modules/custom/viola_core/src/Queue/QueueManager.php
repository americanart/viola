<?php

namespace Drupal\viola_core\Queue;

use Drupal\viola_core\Exceptions\CouldNotQueueException;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\Entity\Queue;

/**
 * Queue content import for processing.
 */
class QueueManager {

  /**
   * The allowed import methods.
   *
   * @var array
   */
  const ALLOWED_VERBS = [
    'create',
    'delete',
    'update',
  ];

  /**
   * Create import queue job with with criteria and import content data.
   *
   * @param string $job_type
   *   The JobType machine name.
   * @param string $action
   *   The CRUD method (e.g. "create" or "delete").
   * @param string $entity_type
   *   The Entity type to operate on (e.g. "node").
   * @param string $bundle
   *   The entity bundle (e.g. "artwork").
   * @param array $data
   *   The job payload.
   * @param string $queue
   *   The queue name (e.g. "default").
   *
   * @return bool
   *   If the job was successfully queued.
   */
  public static function queueJob($job_type, $action, $entity_type, $bundle, array $data = [], $queue = 'default') {
    try {
      if (!in_array(strtolower($action), self::ALLOWED_VERBS)) {
        throw CouldNotQueueException::invalidAction($action);
      }
      if (empty($data)) {
        throw CouldNotQueueException::emptyData();
      }
      // Append the import criteria to the Job payload.
      $payload = [
        'action' => strtolower($action),
        'entity_type' => strtolower($entity_type),
        'bundle' => strtolower($bundle),
        'data' => $data,
      ];
      // Create import Job and add to queue.
      $job = Job::create($job_type, $payload);
      if ($job instanceof Job) {
        $q = Queue::load($queue);
        $q->enqueueJob($job, 10);
        return TRUE;
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('queue_manager')->error($e->getMessage());
      return FALSE;
    }
  }

  /**
   * Get a checksum hash for data.
   *
   * @param array $data
   *   The data to use to generate a checksum/hash.
   *
   * @return string
   *   The checksum value.
   */
  public static function checksum(array $data) {
    return hash('sha1', json_encode($data));
  }

}
