<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Node;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\IsNodeManagerTrait;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\OutputsStatusMessageTrait;

/**
* The repository for creators.
*
* @TypedRepository(
*    entity_type_id = "node",
*    bundle = "creator",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Node\Creator",
*    ),
*   description = @Translation("Repository that holds logic for creators")
* )
*/
final class CreatorRepository extends TypedRepositoryBase {

  use IsNodeManagerTrait;
  use OutputsStatusMessageTrait;

  const ALTERNATIVE_IDENTIFIER_FIELD_NAME = 'field_constituent_id';

}
