<?php

namespace Drupal\viola_core\DataReader;

use Exception;
use Aws\S3\S3Client;
use League\Flysystem\AwsS3V3\AwsS3V3Adapter;
use League\Flysystem\Filesystem;

/**
 * Class S3.
 *   Class used to handle objects in an AWS S3 Bucket.
 */
class S3 {

  /**
   * The file system.
   *
   * @var \League\Flysystem\Filesystem
   */
  protected static $filesystem;

  /**
   * The bucket name.
   *
   * @var string
   */
  protected static $bucket;

  /**
   * Constructor.
   *
   * @param string $bucket
   */
  public function __construct($bucket = '') {
    if (!$bucket) {
      $bucket = $_SERVER['AWS_S3_DATA_BUCKET'];
    }
    try {
      self::$bucket = $bucket;
      $client = new S3Client([
        'credentials' => [
          'key'    => $_SERVER['AWS_KEY'],
          'secret' => $_SERVER['AWS_SECRET'],
        ],
        'region' => $_SERVER['AWS_S3_REGION'],
        'version' => 'latest',
      ]);
      $adapter = new AwsS3V3Adapter($client, self::$bucket);
      self::$filesystem = new Filesystem($adapter);
    }
    catch(Exception $e) {
      \Drupal::logger('s3_reader')->error(sprintf('Error: %s', $e->getMessage()));
    }
  }

  /**
   * Get contents of an S3 object.
   *
   * @param string $target
   *   Path to file.
   *
   * @return mixed
   *
   * @throws \Exception
   */
  public static function getContents($target) {
    if (self::$filesystem->fileExists($target)) {
      $file = self::$filesystem->read($target);
      if ($file) {
        \Drupal::logger('s3_reader')->info(sprintf('S3 DataReader: Retrieved S3://%s/%s', self::$bucket, $target));
        return $file;
      }
    }
    else {
      $message = sprintf('S3 DataReader: Failed to retrieve S3://%s/%s', self::$bucket, $target);
      throw new Exception($message);
    }
    return FALSE;
  }

}
