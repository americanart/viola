# Viola Headless

Headless customizations.

Features:

- Removes the "Home" link from the admin toolbar.
- Redirects the front page to the admin menu or login form.
- Adds a local task link to an Entity's JSON:API endpoint.

Note: This module was largely copied from the Acquia [Lightning Headless](https://github.com/acquia/headless_lightning) project.


