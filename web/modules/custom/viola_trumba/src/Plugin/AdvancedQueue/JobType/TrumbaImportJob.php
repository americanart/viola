<?php

namespace Drupal\viola_trumba\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Annotation\AdvancedQueueJobType;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;
use Drupal\viola_core\Plugin\AdvancedQueue\JobType\ImportJobBase;
use Jenssegers\ImageHash\ImageHash;
use Jenssegers\ImageHash\Implementations\DifferenceHash;
use Exception;

/**
 * @AdvancedQueueJobType(
 *   id = "trumba_import_job",
 *   label = @Translation("Trumba Import Job"),
 *   max_retries = 7,
 *   retry_delay = 79200,
 * )
 */
class TrumbaImportJob extends ImportJobBase {

  /**
   * @var array
   *   Allowed Node bundles to act upon in a job.
   */
  protected static $allowedNodeBundles = [
    'event',
  ];

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    try {
      $payload = $job->getPayload();
      $status = 0;

      if ($payload['entity_type'] != 'node') {
        throw new Exception(sprintf('Invalid entity type: %s.', $payload['entity_type']));
      }
      if (!in_array($payload['bundle'], self::$allowedNodeBundles)) {
        throw new Exception(sprintf('Invalid bundle: %s.', $payload['bundle']));
      }

      /** @var \Drupal\viola_content\Plugin\TypedRepositories\ImageRepository $imageRepository */
      $imageRepository = \Drupal::service(RepositoryManager::class)->repository('media', 'image');
      // Get the pHash of provided image.
      if (isset($payload['data']['image_url']) && filter_var($payload['data']['image_url'], FILTER_VALIDATE_URL)) {
        $hasher = new ImageHash(new DifferenceHash());
        $hash = $hasher->hash($payload['data']['image_url']);
        $phash = $hash->toHex();
        // Find an existing image with a matching perceptual hash.
        // Note: Slight changes to an image will cause a different hash, and would require a more complex query
        // to find images with hashes within a "difference" range.
        // @see: https://github.com/jenssegers/imagehash/issues/55#issuecomment-620790751
        $existingImage = $imageRepository->firstWhere(['field_phash' => $phash]);
        /** @var \Drupal\viola_content\WrappedEntities\Image $existingImage */
        if ($existingImage) {
          $mid = $existingImage->getDrupalId();
        }
        else {
          // Create a new image, setting the pHash and usage values.
          $image['name'] = sprintf('Event Image: %s', $phash);
          $image['field_provider'] = 'Trumba';
          $image['field_phash'] = $phash;
          $image['image_url'] = $payload['data']['image_url'];
          /** @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository $termRepository */
          $termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
          $tid = $termRepository->getTermIdByName('media_directories', 'Event');
          if ($tid) {
            $image['field_directory'][] = $tid;
          }
          $tid = $termRepository->getTermIdByName('usage_rights', 'Unrestricted');
          if ($tid) {
            $image['field_usage_rights'][] = $tid;
          }
          $status = $imageRepository->createMedia($image);
          if ($status) {
            $newImage = $imageRepository->firstWhere(['field_phash' => $phash]);
            /** @var \Drupal\viola_content\WrappedEntities\Image $newImage */
            if ($newImage) {
              $mid = $newImage->getDrupalId();
            }
          }
        }
      }
      // Add the media to payload data.
      unset($payload['data']['image_url']);
      if ($mid) {
        $payload['data']['field_images'] = ['target_id' => $mid];
      }

      // Import the Node.
      $node = NULL;
      /** @var \Drupal\viola_content\Plugin\TypedRepositories\EventRepository $eventRepository */
      $eventRepository = \Drupal::service(RepositoryManager::class)->repository('node', $payload['bundle']);
      // Validate the id field.
      if (!isset($payload['data'][$eventRepository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]) || empty($payload['data'][$eventRepository::ALTERNATIVE_IDENTIFIER_FIELD_NAME])) {
        throw new Exception('Missing required fields.');
      }
      else {
        $existingEvent = $eventRepository->firstWhere([
          $eventRepository::ALTERNATIVE_IDENTIFIER_FIELD_NAME => $payload['data'][$eventRepository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]
        ]);
        if ($existingEvent) {
          $node = $existingEvent->getEntity();
        }
      }
      if ($node && $node instanceof NodeInterface) {
        // Skip if checksums match and the "force" flag is not set.
        if (!isset($payload['data']['force'])
          && isset($payload['data']['field_checksum'])
          && self::skipImport($node, $payload['data']['field_checksum'])
        ) {
          return JobResult::success($eventRepository::getStatusMessage(self::SKIPPED));
        }
        unset($payload['data']['force']);
        if ($payload['action'] == 'delete') {
          $status = $eventRepository->deleteNode($node);
        }
        else {
          $status = $eventRepository->updateNode($payload['data'], $node);
        }
      }
      else {
        // Create a new Node.
        unset($payload['data']['nid']);
        if ($payload['action'] == 'create') {
          $status = $eventRepository->createNode($payload['data']);
        }
      }
      if ($status) {
        return JobResult::success($eventRepository::getStatusMessage($status));
      }
      // Default.
      return JobResult::failure($eventRepository::getStatusMessage($status));

    }
    catch (\Exception $e) {
      $message = sprintf('Job ID: %s, Job Type: %s: %s', $job->getId(), $job->getType(), $e->getMessage());
      return JobResult::failure($message);
    }
  }

}
