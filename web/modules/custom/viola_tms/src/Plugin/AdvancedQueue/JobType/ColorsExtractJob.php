<?php

namespace Drupal\viola_tms\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Annotation\AdvancedQueueJobType;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\Color\ColorService;
use Drupal\viola_core\Plugin\AdvancedQueue\JobType\ImportJobBase;

/**
 * @AdvancedQueueJobType(
 *   id = "colors_extract_job",
 *   label = @Translation("Colors Extraction Job"),
 *   max_retries = 3,
 *   retry_delay = 82800,
 * )
 */
class ColorsExtractJob extends ImportJobBase {

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    try {
      $status = 0;
      $payload = $job->getPayload();
      if ($payload['entity_type'] == 'field_storage' && $payload['bundle'] == 'field_colors' && isset($payload['data']['field_object_id'])) {

        /** @var \Drupal\viola_content\Plugin\TypedRepositories\ArtworkRepository $repository */
        $repository = \Drupal::service(RepositoryManager::class)->repository('node', 'artwork');
        $existingArtwork = $repository->firstWhere([$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME => $payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]]);
        if ($existingArtwork) {
          /** @var ColorService $colorPicker */
          $colorPicker = \Drupal::service('viola.color');
          $data = $existingArtwork->transformInto('default');
          $node = $existingArtwork->getEntity();
          $image = reset($data['images']);
          if ($image && isset($image['sizes']) && isset($image['sizes']['default'])) {
            // URL doesn't have a scheme set, so add HTTPS.
            $url = sprintf('https:%s',$image['sizes']['default']);
            // Test if response returns a bad status code (i.e. 404/403).
            $headers = @get_headers($url) ?: [];
            $responseCode = substr($headers[0], 9, 3);
            if ($responseCode >= 400) {
              throw new \Exception(sprintf('Request for remote file responded with an invalid status code: %s', $responseCode));
            }
            // Get an array of most used HEX colors.
            $colors = $colorPicker->getColorsFromFile($url, 5);
            if (!empty($colors)) {
              // Remove any pre-existing values for color terms.
              $node->get($payload['bundle'])->setValue([NULL]);
              if (isset($data['classifications'])) {
                // Remove common background colors from certain types of Artwork.
                $typesWithBackground = [
                  'Assemblage',
                  'Decorative Arts',
                  'Sculpture',
                  'Installation Art',
                  'Media Arts',
                  'Furniture',
                  'Maquette',
                  'Medal',
                  'Medal-Sample',
                  'Relief',
                  'Miniature'
                ];
                $commonClassifications = array_intersect($data['classifications'], $typesWithBackground);
                if (!empty($commonClassifications)) {
                  foreach(['#000000', '#FFFFFF'] as $bgColor) {
                    unset($colors[$bgColor]);
                  }
                }
              }
              // Set the CSS4 Colors Terms that correspond to the hex color.
              foreach ($colors as $color) {
                // Set the taxonomy term.
                $colorTermId = $colorPicker->getColorTermIdFromHex($color['hex']);
                if ($colorTermId) {
                  $node->get($payload['bundle'])->appendItem($colorTermId);
                }
              }
              $status = $node->save();
            }
          }
        }
      }
      if ($status) {
        return JobResult::success($repository::getStatusMessage($status));
      }
      // Default.
      return JobResult::failure($repository::getStatusMessage($status));
    }
    catch (\Exception $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}
