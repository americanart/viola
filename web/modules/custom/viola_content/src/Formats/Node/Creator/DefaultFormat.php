<?php

namespace Drupal\viola_content\Formats\Node\Creator;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Creator;

final class DefaultFormat extends FormatBase {

  public $alphabeticalName;
  public array $associatedPlaces = [];
  public $birthPlace;
  public $bundle;
  public $collectionDescription;
  public $constituentId;
  public $creatorType;
  public $dateDescription;
  public $dateOfBirth;
  public $dateOfDeath;
  public array $dateRange = [];
  public $description;
  public array $ethnicities = [];
  public $gender;
  public array $groups;
  public array $influences = [];
  public $isPermanentCollection;
  public $locId;
  public array $movements = [];
  public $name;
  public array $nationalities = [];
  public array $occupations = [];
  public array $places = [];
  public $placeOfDeath;
  public $ulanId;
  public $uuid;
  public $viafId;
  public $wikidataId;
  public $wikipediaTitle;

  /**
   * Node\Creator\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Creator $creator
   *   The wrapped entity.
   */
  public function __construct(Creator $creator) {
    parent::__construct();
    $this->bundle = $creator->getBundle();
    $this->alphabeticalName = $creator->getAlphabeticalName();
    $this->associatedPlaces = $creator->getAssociatedPlaces();
    $this->birthPlace = $creator->getBirthPlace();
    $this->collectionDescription = $creator->getCollectionDescription();
    $this->constituentId = $creator->getConstituentId();
    $this->creatorType = $creator->getCreatorType();
    $this->dateDescription = $creator->getDateDescription();
    $this->dateOfBirth = $creator->getDateOfBirth();
    $this->dateOfDeath = $creator->getDateOfDeath();
    $this->dateRange = $creator->getDateRange();
    $this->description = $creator->getDescription();
    $this->ethnicities = $creator->getEthnicities();
    $this->gender = $creator->getGender();
    $this->groups = $creator->getGroups();
    $this->isPermanentCollection = $creator->isPermanentCollection();
    if ($influences = $creator->getInfluencedBy()) {
      foreach ($influences as $influence) {
        $this->influences[] = $influence['name'];
      }
    }
    $this->locId = $creator->getLibraryOfCongressId();
    $this->movements = $creator->getMovements();
    // The display name.
    $this->name = $creator->getTitle();
    $this->nationalities = $creator->getNationalities();
    $this->occupations = $creator->getOccupations();
    $this->path = $creator->getPathAlias();
    $this->places = $creator->getPlaces();
    $this->placeOfDeath = $creator->getPlaceOfDeath();
    $this->ulanId = $creator->getUlanId();
    $this->uuid = $creator->getUuid();
    $this->viafId = $creator->getViafId();
    $this->wikidataId = $creator->getWikidataId();
    $this->wikipediaTitle = $creator->getWikipediaTitle();

  }

}
