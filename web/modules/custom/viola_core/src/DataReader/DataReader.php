<?php

namespace Drupal\viola_core\DataReader;

/**
 * Class DataReader.
 *   Base Class DataReader.
 */
class DataReader {

  /**
   * @var array
   *   The read results.
   */
  protected $results = [];
  /**
   * @var string
   *   The file stream wrapper.
   */
  protected $wrapper;
  /**
   * @var string
   *   The file uri or path.
   */
  protected $target;
  /**
   * @var int
   *   Maximum number of data items to return.
   */
  protected $limit;
  /**
   * @var int
   *   Number of data items to skip.
   */
  protected $offset;

  /**
   * Construct a DataReader.
   *
   * @param string $wrapper
   *   File stream wrapper (file://|http://|https://|s3://)
   * @param string $target
   *   File URI or path.
   * @param int $limit
   *   Maximum number of data items to return.
   * @param int $offset
   *   Number of data items to skip.
   *
   * @return mixed
   */
  public function __construct($wrapper = 'file://', $target = '', $limit = -1, $offset = 0) {
    $this->setWrapper($wrapper);
    $this->setTarget($target);
    $this->setLimit($limit);
    $this->setOffset($offset);
  }

  /**
   * Filter a collection of results by a given key/values.
   *
   * @param string $key
   *   The key to test.
   * @param array $values
   *   Values to include in the final results.
   *
   * @return DataReader
   */
  public function filter($key, $values = []) {
    if (!empty($values)) {
      $filtered = array_filter($this->results, function ($result) use ($key, $values) {
        return isset($result[$key]) && in_array(trim($result[$key]), $values);
      }, ARRAY_FILTER_USE_BOTH);
      $this->setResults($filtered);
    }
    return $this;
  }

  /**
   * @return array
   */
  public function getResults(): array {
    return $this->results;
  }

  /**
   * @param array $results
   * @return DataReader
   */
  public function setResults(array $results) {
    $this->results = $results;
    return $this;
  }

  /**
   * @return string
   */
  public function getWrapper(): string {
    return $this->wrapper;
  }

  /**
   * @param string $wrapper
   * @return DataReader
   */
  public function setWrapper(string $wrapper) {
    $this->wrapper = $wrapper;
    return $this;
  }

  /**
   * @return string
   */
  public function getTarget(): string {
    return $this->target;
  }

  /**
   * @param string $target
   * @return DataReader
   */
  public function setTarget(string $target) {
    $this->target = $target;
    return $this;
  }

  /**
   * @return int
   */
  public function getLimit(): int {
    return $this->limit;
  }

  /**
   * @param int $limit
   * @return DataReader
   */
  public function setLimit(int $limit) {
    $this->limit = $limit;
    return $this;
  }

  /**
   * @return int
   */
  public function getOffset(): int {
    return $this->offset;
  }

  /**
   * @param int $offset
   * @return DataReader
   */
  public function setOffset(int $offset) {
    $this->offset = $offset;
    return $this;
  }

}
