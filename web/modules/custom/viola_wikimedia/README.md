# Viola Wikimedia

## Commands

### `drush viola-wiki:wikipedia`
Update the wikipedia data for all creators with a Wikipedia identifier.
```shell script
drush viola-wiki:wikipedia creator
```

### `drush viola-wiki:wikidata`
Update the wikidata data for all artworks with a Wikidata identifier.
```shell script
drush viola-wiki:wikidata artwork
```

Update a specific Creator (Node ID).
```shell script
drush viola-wiki:wikidata creator --id 1
```

