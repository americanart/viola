# Viola Migrate

Viola CMS installation and content migration tools.

A group of [DrupalConsole](https://hechoendrupal.gitbooks.io/drupal-console/content/en/commands/available-commands.html) commands for Viola.

The following commands will create default Menus and Taxonomy Terms for a fresh install
```shell script
lando drush viola-migrate:terms --all
&& lando drush viola-migrate:menu main
```

### `drush viola-migrate:generate-terms
Generate an Taxonomy Terms import file from the full TMS export.

```shell script
// Generates an import file for all "mediums" taxonomy terms.
drush viola-migrate:generate-terms mediums
```

### `drush viola-migrate:import-terms`
Import Taxonomy Terms for a Vocabulary.

```shell script
drush viola-migrate:generate-terms mediums
```


