<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Node;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\IsNodeManagerTrait;

/**
* The repository for exhibitions.
*
* @TypedRepository(
*    entity_type_id = "node",
*    bundle = "exhibition",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Node\Exhibition",
*    ),
*   description = @Translation("Repository that holds logic for exhibitions")
* )
*/
final class ExhibitionRepository extends TypedRepositoryBase {

  use IsNodeManagerTrait;

}
