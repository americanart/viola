# Viola

## Headless Content Management System

Viola is a Headless Content Management System for the [Smithsonian American Art Museum](https://americanart.si.edu/).
The project is built on [Drupal](https://drupal.org) and leverages Drupal's powerful content editing features and
[JSON:API](https://www.drupal.org/docs/core-modules-and-themes/core-modules/jsonapi-module/api-overview).
It also includes some patterns and utilities for ingesting and transforming data, allowing Viola to act as a _hub_ which
can serve data from different sources to multiple frontend consumers, including sites, kiosks, mobile applications, and
digital signage.

### Installation

It is assumed you're using [composer](https://getcomposer.org/) for dependency management, and a technology stack consistent
with Drupal's minimum [requirements](https://www.drupal.org/docs/system-requirements).

The following installation instructions have been written using [Lando](https://lando.dev/) as a local development
environment. [Installing Lando](https://docs.lando.dev/basics/installation.html) is the quickest and easiest way to
evaluate this project, but the project can be installed just as any [Drupal](https://www.drupal.org/docs/installing-drupal)
project.

1. Clone this repository and run `composer install` from the project root.
2. Run `lando start` from the project directory to create a local environment, including services for the database, NodeJS,
Elasticsearch, and a Redis cache.
3. Copy the `.env.example` file to `.env`. The file already contains the Lando default database credentials.
4. Install Viola using [Drush](https://www.drush.org/latest/commands/site_install/):
```shell
lando drush site:install --existing-config --account-mail XXXXX --account-name XXXXX --account-pass XXXXX --yes
```
5. Run the following Drupal Console command to create Viola's default Taxonomy Terms.
```shell script
lando drush viola-migrate:import-terms --all
```

### Updating Drupal

Run the following to update drupal/core and dependencies. Note: some scaffold files in Viola
are excluded from updating, so you may want to reference the Drupal release notes to see if
any of those files have changes.
```shell
lando composer update "drupal/core-*" --with-dependencies
```

### Documentation

Additional documentation regarding
- [Local settings and development](web/sites/DEVELOPMENT.md).
- [Custom module documentation](./web/modules/custom/README.md).
- [User roles](./web/sites/ROLES.md)

### Contributing

When contributing to this repository, please first discuss the change you wish to make via the
[issue queue](https://gitlab.com/americanart/viola/-/issues) before submitting a
[merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

It's important to note that this project is actively being used and developed by the Smithsonian American Art Museum,
which means that not only is it possible (guaranteed) there will be future breaking changes, but that some features are
tailored to our specific requirements. We have made our best effort to make this project configurable and useful for
anyone, with the belief that working in the open is the best way to provide value for the cultural heritage sector as
a whole.

Please note we have a [code of conduct](./CODE_OF_CONDUCT.md), please follow it in all your interactions with the project.

---

<div align="center">
  <img src='/web/sites/default/assets/images/logo.png' width="220" alt="Viola Headless CMS" style="max-width: 100%;" />
</div>

This project is named in honor of the naturalist [Mary Vaux Walcott](https://americanart.si.edu/artist/mary-vaux-walcott-5197).


