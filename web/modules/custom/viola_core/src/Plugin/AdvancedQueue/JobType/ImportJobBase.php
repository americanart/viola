<?php

namespace Drupal\viola_core\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\Plugin\AdvancedQueue\JobType\JobTypeBase;

/**
 * Import Job Base.
 */
class ImportJobBase extends JobTypeBase {

  /**
   * Return status for a skipped operation.
   */
  protected const SKIPPED = -1;

  /**
   * @var array
   *   Allowed Media bundles to act upon in jobs.
   */
  protected static $allowedMediaBundles = [
    'image',
  ];

  /**
   * @var array
   *   Allowed Node bundles to act upon in jobs.
   */
  protected static $allowedNodeBundles = [
    'artwork',
    'creator',
    'event',
  ];

  /**
   * {@inheritdoc}
   */
  public function process(Job $job){}

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   * @param string $checksum
   *
   * @return bool
   */
  public static function skipImport($entity, $checksum) {
    if ($entity->hasField('field_checksum')) {
      return $entity->field_checksum->value == $checksum ? TRUE : FALSE;
    }
    return FALSE;
  }

}
