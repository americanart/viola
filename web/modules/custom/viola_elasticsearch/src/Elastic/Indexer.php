<?php

namespace Drupal\viola_elasticsearch\Elastic;

use Drupal\advancedqueue\Entity\Queue;
use Drupal\advancedqueue\Job;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Class Indexer
 *   Service providing utilities for performing actions on an Elasticsearch index.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class Indexer {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;
  /**
   * @var array
   */
  public $nodeBundles = [
    'artwork',
    'creator',
    'exhibition',
    'event',
    'page',
  ];

  /**
   * Indexer service constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Drupal default database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Drupal entity type manager factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The Drupal logger channel factory.
   */
  public function __construct(
    Connection $connection,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactory $logger_factory
  ) {
    $this->database = $connection;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factory->get('viola_elasticsearch');
  }

  /**
   * Queue a bulk index job.
   *
   * @param array $params
   * @return mixed
   */
  public function queueBulkJob($params) {
    $job = Job::create('bulk_index_job', $params);
    if ($queue = Queue::load('viola_elasticsearch')) {
      return $queue->enqueueJob($job, 5);
    }
    return NULL;
  }

  /**
   * Perform an index action for a single Entity.
   *
   * @param EntityInterface $entity
   * @param string $action
   *   The bulk operation action
   *   @see: https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-bulk.html#bulk-api-request-body
   * @param string $index_name
   *
   * @return mixed
   * @throws \Exception
   *
   */
  public function singleOperation($entity, $action = 'index', $index_name = '') {
    if (!$entity instanceof UserInterface && !$entity instanceof NodeInterface) {
      throw new \Exception('Attempted action on invalid entity.');
    }
    if (!$index_name) {
      $index = new Index();
      $index_name = $index->getIndexName();
    }
    // Queue an index job for the inserted Node
    $doc = new Document($entity);
    $body = $doc->getBody();
    if ($body) {
      // The bulk operation index target and settings.
      $params = [
        'index' => $index_name,
        'refresh' => TRUE,
      ];
      // The action for this item.
      $params['body'][] = [
        $action => [
          '_id' => $entity->uuid(),
        ]
      ];
      // The metadata for this item (required for "create" and "index" actions.
      if (in_array($action, ['create', 'index'])) {
        $params['body'][] = $body;
      }
      return $this->queueBulkJob($params);
    }

    return NULL;
  }

  /**
   * Perform a bulk operation given an array of Entity IDs.
   *
   * @param array $ids
   * @param string $action
   * @param string $index_name
   * @return mixed
   */
  public function bulkOperation($ids, $action = 'index', $index_name = '') {
    try {
      if (!$index_name) {
        $index = new Index();
        $index_name = $index->getIndexName();
      }
      $items = $this->entityTypeManager->getStorage('node')->loadMultiple($ids);
      $params = [
        'index' => $index_name,
        'refresh' => TRUE,
      ];
      foreach ($items as $item) {
        // A bulk operation requires two arrays, an action ("index" in this case) followed by the metadata.
        $doc = new Document($item);
        $body = $doc->getBody();
        if ($body) {
          // The action for this item.
          $params['body'][] = [
            $action => [
              '_id' => $item->uuid(),
            ]
          ];
          // The metadata for this item (required for "create" and "index" actions.
          if (in_array($action, ['create', 'index'])) {
            $params['body'][] = $body;
          }
        }
      }
      $items = NULL;
      unset($items);
      // Queue each bulk index operation.
      return $this->queueBulkJob($params);
    }
    catch (\Exception $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Get Node IDs that can be added to an index.
   *
   * @param int $offset
   * @param int $limit
   * @return mixed
   */
  public function queryNodes($bundle = '', $offset = 0, $limit = 100000) {
    // TODO: Move this to Repository "all()" method?
    $query = $this->database->select('node_field_data', 'n')
      ->fields('n', ['nid'])
      ->condition('n.status', 1);
    if ($bundle) {
      $query->condition('n.type', $bundle, '=');
    }
    $query->range($offset, $limit);
    return $query->execute()->fetchCol();
  }

}
