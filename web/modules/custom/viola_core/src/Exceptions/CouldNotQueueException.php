<?php

namespace Drupal\viola_core\Exceptions;

use Exception;

/**
 * Exceptions specific to queuing jobs.
 */
final class CouldNotQueueException extends Exception {

  /**
   * Exception thrown when attempting to queue a non-supported import action.
   *
   * @param string $action
   *   The action attempted.
   *
   * @return CouldNotQueueException
   */
  public static function invalidAction($action) {
    return new static('Invalid action: `{$action}, unable to queue job with this action.');
  }

  /**
   * Exception thrown when attempting to import an empty payload.
   *
   * @return CouldNotQueueException
   */
  public static function emptyData() {
    return new static('Empty data: unable to queue a job without data.');
  }

}
