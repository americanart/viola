<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class FilterArtistsQuery
 *   Build an Elasticsearch DSL Query to filter Artists content.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class FilterArtistsQuery extends QueryDslBase {

  public function __construct($params) {
    parent::__construct();
    // The query with default filters.
    $query = [
      'bool' => [
        'must_not' => [],
        'filter' => [
          [
            // Limit results to artists.
            'term' => [
              'type' => 'creators'
            ]
          ]
        ]
      ],
    ];
    // Filter by Place(s).
    if (array_key_exists('places', $params)) {
      if(is_array($params['places'])) {
        foreach ($params['places'] as $state) {
          $size = sizeof($query['bool']['filter']);
          $query['bool']['filter'][$size] =
            ['term' => [
              'places.keyword' => $state
            ]];
        }
      } else {
        $query['bool']['filter'][] = [
          'term' => [
            'places.keyword' => $params['places']
          ]
        ];
      }
    }

    $this->setQuery($query);
  }
}
