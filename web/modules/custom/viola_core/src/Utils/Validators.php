<?php

namespace Drupal\viola_core\Utils;

class Validators {

    /**
     * Compare string to null designation
     *
     * @param  string $str
     * @return bool
     */
    public static function isNull(string $str): bool {
        if (!isset($str) || trim($str) === '' || ($str === '<null>') || $str === 'null') {
            return true;
        }
        return  false;
    }

}
