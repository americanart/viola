<?php

namespace Drupal\viola_headless\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Defines a controller for the front page of the site.
 */
class FrontController extends ControllerBase {

  /**
   * Redirect anonymous users to the login form and authenticated users to the admin "content" table.
   */
  public function frontpage() {
    $build = [];
    if ($this->currentUser()->isAnonymous()) {
      // Redirect to the login screen.
      return $this->redirect('user.login');
    }
    else {
      if ($this->currentUser()->hasPermission('access content overview')) {
        // Permitted users are directed to their user profile.
        return $this->redirect('entity.user.canonical', ['user' => $this->currentUser()->id()]);
      }
      $build['heading'] = [
        '#type' => 'markup',
        '#markup' => $this->t('This site has no homepage content.'),
      ];
    }
    return $build;
  }

}
