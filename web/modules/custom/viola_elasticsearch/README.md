# Viola Elasticsearch

Provides integration with Elasticsearch service.

## Commands

Create a new index
```shell script
drush viola-es:manage create
```
Delete the index
```shell script
drush viola-es:manage delete
```

Index a specific Node content type:
```shell script
drush viola-es:index page
```
Remove a specific Node from the index (by Node ID)
```shell script
drush viola-es:remove --ids 50761
```
Index _all_ site content:
```shell script
drush viola-es:index --all
```
Process the reindex queue
```shell script
drush advancedqueue:queue:process viola_elasticsearch
```

## Local development

Elasticsearch can be queried local using cURL or a REST API client like [Postman](https://www.postman.com/downloads/)

### Example Elasticsearch Commands

#### List all indexes
```shell script
curl -X GET '127.0.0.1:9200/_cat/indices?v'
```

### Show index mappings
```shell script
curl -X GET '127.0.0.1:9200/catalog/_mapping?pretty=true'

```
### List all docs in an index
Example using listing the index "autocomplete".
```shell script
curl -X GET '127.0.0.1:9200/catalog/_search?pretty=true'
```
