<?php

namespace Drupal\viola_content\Formats\Node\Creator;

use Drupal\viola_content\Formats\FormatBase;
use Spatie\SchemaOrg\Schema;
use Drupal\viola_content\WrappedEntities\Node\Creator;

final class JsonLdFormat extends FormatBase {

  /**
   * @var object \Spatie\SchemaOrg\Schema
   */
  private $schema;

  /**
   * Node\Creator\JsonLdFormat constructor.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Creator $creator
   *   The wrapped entity.
   */
  public function __construct(Creator $creator) {
    parent::__construct();
    $this->schema = Schema::person()
      ->name($creator->getTitle())
      ->description($creator->getDefaultDescription())
      ->url($creator->getPathAlias());
    // Add the LOD sameAs element(s)
    $links = [];
    $links[] = sprintf('https://edan.si.edu/saam/id/person-institution/%s', $creator->getConstituentId());
    if($ulanId = $creator->getUlanId()) {
      $links[] = sprintf('https://www.getty.edu/vow/ULANFullDisplay?find=&role=&nation=&subjectid=%s', $ulanId);
    }
    if($viafId = $creator->getViafId()) {
      $links[] = sprintf('https://viaf.org/viaf/%s', $viafId);
    }
    if($locId = $creator->getLibraryOfCongressId()) {
      $links[] = sprintf('https://id.loc.gov/authorities/names/%s.html', $locId);
    }
    if($wikidataId = $creator->getWikidataId()) {
      $links[] = sprintf('https://www.wikidata.org/wiki/%s', $wikidataId);
    }
    $this->schema->sameAs($links);

  }

  /**
   * @return string;
   */
  public function transform() {
    return $this->schema;
  }

}
