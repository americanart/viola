<?php

namespace Drupal\viola_content\Formats\Node\Event;

use Drupal\viola_content\Formats\FormatBase;
use Spatie\SchemaOrg\Schema;
use Drupal\viola_content\WrappedEntities\Node\Event;

final class JsonLdFormat extends FormatBase {

  /**
   * @var object \Spatie\SchemaOrg\Schema
   */
  private $schema;

  /**
   * Node\Event\JsonLdFormat constructor.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Event $event
   *   The wrapped entity.
   */
  public function __construct(Event $event) {
    parent::__construct();

    $this->schema = Schema::event()
      ->name(strip_tags($event->getTitle()))
      ->description($event->getDescription())
      ->url($event->getPathAlias());
    if ($dateRange = $event->getDateRange()) {
      if (isset($dateRange['start'], $dateRange['end'])) {
        $this->schema->startDate($dateRange['start'])->endDate($dateRange['end']);
      }
    }
  }

  /**
   * @return string;
   */
  public function transform() {
    return $this->schema;
  }

}
