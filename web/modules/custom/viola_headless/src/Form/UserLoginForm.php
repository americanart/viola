<?php

namespace Drupal\viola_headless\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserLoginForm as BaseUserLoginForm;

/**
 * Defines a user login form with specialized redirect handling.
 */
class UserLoginForm extends BaseUserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_state->setRedirect('<front>');
  }

}
