<?php

namespace Drupal\viola_linked_data\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaLinkedDataCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var int
   */
  public $queued;

  /**
   * The allowed import content types.
   *
   * @var array
   */
  protected $types = [
    'artwork',
    'creator',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Update Creators with import list artist Link Data identifiers.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option ids
   *   A comma-separated list of specific TMS Ids (object or constituent) to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @usage viola-linked-data:artist-identifiers
   *   Usage description
   *
   * @command viola-linked-data:import-identifiers
   * @aliases viola-ld:ids
   */
  public function importIdentifiers($type = '', $options = [
    'ids' => '',
    'limit' => 100000,
    'offset' => 0]
  ) {
    $this->logger()->notice(dt("Running viola-linked-data:import-identifiers ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    if ($type == 'creator') {
      $filepath = 'identifiers/artist_linked_data.json';
      $identifier = 'saam_constituent_id';
    }
    else {
      $filepath = 'identifiers/artwork_linked_data.json';
      $identifier = 'saam_object_id';
    }
    // Retrieve and queue import results for processing.
    $reader = new JsonDataReader(
      's3://',
      $filepath,
      $options['limit'],
      $options['offset']
    );
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    $results = $reader->read()->filter($identifier, $ids)->getResults();
    if ($results) {
      $batch = [
        'title' => dt('@type linked data identifiers processing ...', ['@type' => ucfirst($type)]),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_linked_data\BatchService::batchFinished',
      ];

      foreach ($results as $result) {
        // Prepare the operations.
        $batch['operations'][] = [
          'Drupal\viola_linked_data\BatchService::batchProcessIdentifiers', [
            $result,
            dt('Queue import for @type: @id', ['@type' => ucfirst($type), '@id' => $result[$identifier],]),
          ],
        ];
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

}
