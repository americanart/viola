<?php

namespace Drupal\viola_content\WrappedEntities\Paragraph;

use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the `heading_component` paragraph.
 */
class HeadingComponent extends TransformableWrappedEntity {

  /**
   * @return string
   */
  public function getSize() {
    return $this->getEntity()->get('field_size')->value ?? '';
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->getEntity()->get('field_title')->value ?? '';
  }

}
