<?php

namespace Drupal\viola_linked_data;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\viola_core\DataMapper\DataMapperFactory;
use Drupal\viola_core\Queue\QueueManager;

/**
 * Class BatchService.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Batch process callback.
   *
   * @param array $identifiers
   *   Linked Data identifiers data for the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessIdentifiers($identifiers, $operation_details, &$context) {
    try {
      if (!empty($identifiers['saam_constituent_id'])) {
        $identifier = 'saam_constituent_id';
        $bundle = 'creator';
      }
      else {
        $identifier = 'saam_object_id';
        $bundle = 'artwork';
      }
      $mapper = DataMapperFactory::create('viola_linked_data', sprintf('%s_identifier', $bundle), $identifiers);
      $queued = QueueManager::queueJob(
        'collection_import_job',
        'update',
        'node',
        $bundle,
        $mapper->getData(),
        'viola_linked_data'
      );
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $identifiers[$identifier];
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(dt('Queued @count import jobs.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(dt('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
