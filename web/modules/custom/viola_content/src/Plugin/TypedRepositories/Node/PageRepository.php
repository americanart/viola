<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Node;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\IsNodeManagerTrait;

/**
* The repository for pages.
*
* @TypedRepository(
*    entity_type_id = "node",
*    bundle = "page",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Node\Page",
*    ),
*   description = @Translation("Repository that holds logic for pages")
* )
*/
final class PageRepository extends TypedRepositoryBase {

  use IsNodeManagerTrait;

}
