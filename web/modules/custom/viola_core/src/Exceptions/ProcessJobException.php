<?php

namespace Drupal\viola_core\Exceptions;

use Exception;

/**
 * Exceptions specific to processing queue jobs.
 */
final class ProcessJobException extends Exception {

  /**
   * Exception thrown when attempting to process a non-supported import action.
   *
   * @param string $action
   *
   * @return ProcessJobException
   */
  public static function invalidAction($action) {
    return new static('Invalid action: `{$action}, unable to process a job with this action.');
  }

}
