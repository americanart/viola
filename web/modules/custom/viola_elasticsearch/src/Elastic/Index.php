<?php

namespace Drupal\viola_elasticsearch\Elastic;

/**
 * Class Index
 *   Settings and mappings for the Elasticsearch index.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class Index {

  /**
   * The index name.
   */
  public $indexName;

  /**
   * The index settings.
   */
  public function getConfig() {
    $config = [
      'index' => $this->getIndexName(),
      'body'  => [
        'mappings' => [
          '_source' => [
            'enabled' => TRUE,
          ],
          // Set sane defaults for dynamic mapping.
          'dynamic_templates' => [
            [
              'integers' => [
                'match_mapping_type' => 'long',
                'mapping' => [
                  'type' => 'integer',
                ],
              ],
            ],
            [
              'strings' => [
                'match_mapping_type' => 'string',
                'mapping' => [
                  'type' => 'text',
                  'fields' => [],
                ],
              ],
            ],
          ],
        ],
        'settings' => $this->getSettings(),
      ]
    ];
    // Merge index configuration with command and content specific field mappings.
    $config['body']['mappings']['properties'] = array_merge(
      $this->getCommonFieldMappings(),
      $this->getArtworkFieldMappings(),
      $this->getCreatorMappings(),
      $this->getEventMappings(),
    );
    return $config;
  }

  /**
   * @return array
   */
  private function getSettings() {
    return  [
      'number_of_shards' => 2,
      'number_of_replicas' => 1,
      'refresh_interval' => '15s',
      'analysis' => [
        'filter' => [
          'english_stemmer' => [
            'type' => 'stemmer',
            'language' => 'english',
          ],
          'english_possessive_stemmer' => [
            'type' => 'stemmer',
            'language' => 'possessive_english',
          ],
        ],
        'tokenizer' => [
          'split_on_backslash' => [
            'type' => 'simple_pattern_split',
            'pattern' => addslashes('\\'),
          ]
        ],
        'analyzer' => [
          'xref_term_analyzer' => [
            'tokenizer' => 'split_on_backslash',
            'filter' => [
              'lowercase',
            ],
          ],
          'lowercase_not_analyzed' => [
            'tokenizer' => 'keyword',
            'filter' => [
              'lowercase',
            ],
          ],
          'english_markup' => [
            'tokenizer' => 'standard',
            'char_filter' => [
              'html_strip',
            ],
            'filter' => [
              'english_possessive_stemmer',
              'asciifolding',
              'lowercase',
              'english_stemmer',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @return array
   */
  private function getCommonFieldMappings() {
    return [
      '@timestamp' => [
        'type' => 'date',
        'format' => 'epoch_second',
      ],
      'categories' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      // Full text search field.
      'contents' => [
        'type' => 'text',
        'analyzer' => 'english_markup',
      ],
      // The document ID. In the format [content-type].[unique-identifier].
      'id' => [
        'type' => 'keyword',
      ],
      // A date range filter which can be the born/death dates of an individual or the creation date of an Artwork.
      'dateRange' => [
        'type' => 'date_range',
        "format" => 'strict_date_optional_time||epoch_second',
      ],
      // Default image asset.
      'image' => [
        'type' => 'object',
        'properties' => [
          'caption' => [
            'type' => 'text',
          ],
          'alt' => [
            'type' => 'text',
          ],
          'guid' => [
            'type' => 'keyword',
            'ignore_above' => 256,
          ],
          'url' => [
            'type' => 'text',
            'index' => FALSE,
          ],
          'sizes' => [
            'type' => 'nested',
            'properties' => [
              'xsmall' => [
                'type' => 'text',
                'index' => FALSE,
              ],
              'small' => [
                'type' => 'text',
                'index' => FALSE,
              ],
              'medium' => [
                'type' => 'text',
                'index' => FALSE,
              ],
              'large' => [
                'type' => 'text',
                'index' => FALSE,
              ],
              'xlarge' => [
                'type' => 'text',
                'index' => FALSE,
              ],
            ],
          ],
        ],
      ],
      'keywords' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      // A subset of the `venues` field representing a specific museum or collection.
      'collection' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'movements' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'path' => [
        'type' => 'text',
        'index' => FALSE,
      ],
      'subtitle' => [
        'type' => 'text',
      ],
      'summary' => [
        'type' => 'text',
        'index' => FALSE,
      ],
      'title' => [
        'type' => 'search_as_you_type',
        'boost' => 3,
      ],
      // The content type.
      'type' => [
        'type' => 'keyword',
      ],
    ];
  }

  /**
   * @return array
   */
  private function getArtworkFieldMappings() {
    return [
      'accessionNumber' => [
        'type' => 'search_as_you_type',
        'boost' => 2,
      ],
      'classifications' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      // A nested array of colors with "weight" for sort order.
      'colors' => [
        'type' => 'nested',
        'properties' => [
          'color' => [
            'type' => 'text',
            'fields' => [
              'keyword' => [
                'type' => 'keyword',
                'ignore_above' => 128,
              ],
            ],
          ],
          'weight' => [
            'type' => 'integer',
            'index' => FALSE,
          ],
        ],
      ],
      // A nested array of creators relationships.
      'creators' => [
        'type' => 'nested',
        'properties' => [
          'constituentId' => [
            'type' => 'integer',
          ],
          'creatorType' => [
            'type' => 'keyword',
          ],
          'name' => [
            'type' => 'text',
            'boost' => 3,
            'copy_to' => 'contents',
          ],
          'order' => [
            'type' => 'integer',
          ],
          'creatorRole' => [
            'type' => 'keyword',
          ],
        ]
      ],
      'credit' => [
        'type' => 'text',
      ],
      'depictions' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'genres' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'isOnView' => [
        'type' => 'boolean',
      ],
      'isOpenAccess' => [
        'type' => 'boolean',
      ],
      'location' => [
        'type' => 'text',
        'norms' => FALSE,
      ],
      'mediums' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'objectId' => [
        'type' => 'integer',
      ],
      'periods' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'ontology' => [
        'type' => 'text',
        'analyzer' => 'xref_term_analyzer',
        'boost' => 2,
      ],
      'usageRights' => [
        'type' => 'keyword',
      ],
    ];
  }

  /**
   * @return array
   */
  private function getCreatorMappings() {
    return [
      'creatorType' => [
        'type' => 'keyword',
      ],
      'gender' => [
        'type' => 'keyword',
      ],
      'alphabeticalName' => [
        'type' => 'text',
        'analyzer' => 'lowercase_not_analyzed',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ],
      ],
      'ethnicities' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'groups' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'influences' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'nationalities' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'occupations' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ]
        ]
      ],
      'places' => [
        'type' => 'text',
        'fields' => [
          'keyword' => [
            'type' => 'keyword',
            'ignore_above' => 128,
            'split_queries_on_whitespace' => FALSE,
          ],
        ]
      ],
    ];
  }

  /**
   * @return array
   */
  private function getEventMappings() {
    return [
      // A nested array of venues with a date range of the event for Events and Exhibitions.
      'venues' => [
        'type' => 'nested',
        'properties' => [
          'name' => [
            'type' => 'text',
            'fields' => [
              'keyword' => [
                'type' => 'keyword',
                'ignore_above' => 128,
                'split_queries_on_whitespace' => FALSE,
              ],
            ],
          ],
          'dateRange' => [
            'type' => 'date_range',
            "format" => 'strict_date_optional_time||epoch_second',
          ],
        ],
      ],
    ];
  }

  /**
   * @return string
   */
  public function getIndexName(): string {
    return $this->indexName ?? $_SERVER['ELASTICSEARCH_INDEX'];
  }

  /**
   * @param string $indexName
   */
  public function setIndexName(string $indexName): void {
    $this->indexName = $indexName;
  }

}
