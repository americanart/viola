<?php

namespace Drupal\viola_content\Formats\Node\Exhibition;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Exhibition;

final class SearchIndexFormat extends FormatBase {

  /**
   * @var array
   */
  private $document = [];

  /**
   * Node\Exhibition\SearchIndexFormat constructor.
   *   Format a Node for elasticsearch indexing.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Exhibition $exhibition
   *   The wrapped entity.
   */
  public function __construct(Exhibition $exhibition) {
    parent::__construct();
    $this->document['@timestamp'] = time();
    // TODO: Add content of Text Components to "contents".
    $this->document['contents'] = $exhibition->getBlurb();
    $this->document['path'] = $exhibition->getPathAlias();
    $this->document['summary'] = $exhibition->getSummary();
    $this->document['title'] = $exhibition->getTitle();
    $this->document['type'] = $exhibition->getBundle();
    $this->document['venues'] = [];
    $venues = $exhibition->getVenues();
    foreach ($venues as $venue) {
      if (!empty($venue['name'])) {
        $item['name'] = $venue['name'];
      }
      if (!empty($venue['dateRange'])) {
        $item['dateRange']['lte'] = $venue['dateRange']['end'];
        $item['dateRange']['gte'] = $venue['dateRange']['start'];
      }
      if ($item) {
        $this->document['venues'][] = $item;
      }
    }
  }

  /**
   * @return array;
   */
  public function transform() {
    return $this->document;
  }

}
