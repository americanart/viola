<?php

namespace Drupal\viola_tms;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\viola_core\DataMapper\DataMapperFactory;
use Drupal\viola_core\Queue\QueueManager;

/**
 * Class BatchService.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Batch process callback for color extraction.
   *
   * @param string $objectId
   *   TMS object ID.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessColor($objectId, $operation_details, &$context) {
    try {
      $queued = QueueManager::queueJob(
        'colors_extract_job',
        'update',
        'field_storage',
        'field_colors',
        ['field_object_id' => $objectId],
        'viola_tms'
      );
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $objectId;
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch process callback for TMS Import.
   *
   * @param string $bundle
   *   The content type.
   * @param array $data
   *   Raw data for the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessTmsImport($bundle, $data, $operation_details, &$context) {
    try {
      $mapper = DataMapperFactory::create('viola_tms', $bundle, $data);
      $jobData = $mapper->getData();
      $queued = QueueManager::queueJob(
        'collection_import_job',
        'create',
        'node',
        $bundle,
        $jobData,
        'viola_tms'
      );
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $jobData['field_checksum'];
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(dt('Queued @count import jobs.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(dt('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
