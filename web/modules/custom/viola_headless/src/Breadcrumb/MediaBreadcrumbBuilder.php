<?php
namespace Drupal\viola_headless\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\media\MediaInterface;

class MediaBreadcrumbBuilder implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $attributes) {
    $parameters = $attributes->getParameters()->all();
    if (!empty($parameters['media']) && $parameters['media'] instanceof MediaInterface) {
        return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $breadcrumb->addCacheContexts(['url.path']);
    $breadcrumb->addLink(Link::createFromRoute(' Home', '<front>'));
    $breadcrumb->addLink(Link::createFromRoute(' Administration', 'system.admin'));
    $breadcrumb->addLink(Link::createFromRoute('Content', 'system.admin_content'));
    $breadcrumb->addLink(Link::createFromRoute('Media', 'entity.media.collection'));
    return $breadcrumb;
  }

}

