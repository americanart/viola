<?php

namespace Drupal\viola_core\Controller\Data;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DataControllerBase.
 */
class DataControllerBase extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack;
   */
  protected $requestStack;

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requestStack = $container->get('request_stack');
    $instance->loggerFactory = $container->get('logger.factory');
    return $instance;
  }

  /**
   * Create a Cacheable Metadata Object.
   *
   * @return CacheableMetadata
   */
  public function getCacheableMetadata() {
    // Create cache metadata.
    $cache_metadata = new CacheableMetadata();
    $cache_metadata->setCacheMaxAge(86400);
    return $cache_metadata;
  }

}
