<?php

namespace Drupal\viola_migrate\Commands;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drush\Commands\DrushCommands;

class ViolaMigrateTaxonomyTermCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The path to data directory.
   *
   * @var string
   */
  protected $dataDirectory;

  /**
   * @var string[]
   */
  public $generatedVocabularies = [
    'ethnicities',
    'groups',
    'mediums',
    'nationalities',
    'occupations',
    'series',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->dataDirectory = dirname(__DIR__, 2) . '/data';
  }

  /**
   * Import taxonomy terms for a given Vocabulary name.
   *
   * @param string $vocab
   *   The Vocabulary machine name.
   *
   * @option all
   *   Import all Terms.
   * @usage viola-migrate:terms classifications
   *   Usage description
   *
   * @command viola-migrate:import-terms
   * @aliases viola-migrate:terms
   */
  public function importTerms($vocab = '', $options = ['all' => FALSE]) {
    $this->logger()->notice(dt("Running viola-migrate:import-terms ..."));
    // Get import file.
    $vocab = mb_strtolower($vocab);
    if ($vocab && !$options['all']) {
      // Run the import for a supplied Vocabulary.
      $importFile = sprintf('%s/%s/%s.%s', $this->dataDirectory, 'taxonomy_terms', $vocab, 'json');
      $reader = new JsonDataReader('file://', $importFile);
      $data = $reader->read()->getResults();
      if (!empty($data)) {
        /** @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository $termRepository */
        $termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
        $children = [];
        // Import parent Terms.
        foreach ($data as $item) {
          $item['vid'] = $vocab;
          $child_terms = $item['children'] ?? FALSE;
          unset($item['children']);
          if ($termRepository->getTermIdByName($vocab, $item['name'])) {
            $this->logger()->warning(dt('Skipped: @vocab Taxonomy Term `@name` already exists', ['@vocab' => ucfirst($vocab), '@name' => $item['name']]));
          }
          else {
            $status = $termRepository->createTerm($item);
            if ($status === SAVED_NEW) {
              $this->logger()->notice(dt('Created: @vocab Taxonomy Term `@name`', ['@vocab' => ucfirst($vocab), '@name' => $item['name']]));
            }
          }
          // Collect the child Terms.
          /** @var \Drupal\viola_content\WrappedEntities\TaxonomyTerm $parent */
          $parent = $termRepository->firstWhere(['name' => $item['name'], 'vid' => $vocab]);
          if ($parent) {
            if ($child_terms && $parent && $parent_tid = $parent->getDrupalId()) {
              foreach ($child_terms as $key => $child) {
                $child_terms[$key]['vid'] = $vocab;
                $child_terms[$key]['parent'] = $parent_tid;
                unset($child_terms[$key]['children']);
              }
              $children = array_merge($children, $child_terms);
            }
          }
        }
        // Import child Terms.
        foreach ($children as $item) {
          if ($termRepository->getTermIdByName($vocab, $item['name'], $item['parent'])) {
            $this->logger()->warning(dt('Skipped: @vocab Taxonomy Term `@name` already exists', ['@vocab' => ucfirst($vocab), '@name' => $item['name']]));
          }
          else {
            $status = $termRepository->createTerm($item);
            if ($status === SAVED_NEW) {
              $this->logger()->notice(dt('Created: @vocab Taxonomy Term `@name`', ['@vocab' => ucfirst($vocab), '@name' => $item['name']]));
            }
          }
        }
      }
      else {
        $this->logger()->warning('Nothing to import.');
      }
    }
    else {
      // Import ALL vocabularies.
      if (PHP_SAPI === 'cli') {
        $command = \Drupal::service('viola_migrate.taxonomy_term.commands');
        $command->importTerms('classifications');
        $command->importTerms('colors');
        $command->importTerms('contribution_types');
        $command->importTerms('creator_roles');
        $command->importTerms('creator_types');
        $command->importTerms('educational_organizations');
        $command->importTerms('event_series');
        $command->importTerms('ethnicities');
        $command->importTerms('genders');
        $command->importTerms('groups');
        $command->importTerms('highlights');
        $command->importTerms('keywords');
        $command->importTerms('mediums');
        $command->importTerms('media_directories');
        $command->importTerms('occupations');
        $command->importTerms('nationalities');
        $command->importTerms('series');
        $command->importTerms('states');
        $command->importTerms('subjects');
        $command->importTerms('time_periods');
        $command->importTerms('usage_rights');
        $command->importTerms('venues');
      }
    }
  }

  /**
   * Use the daily TMS export to generate a JSON import file for a given Vocabulary name.
   *
   * @param string $vocab
   *   The Vocabulary machine name.
   *
   * @option all
   *   Import all Terms.
   * @usage viola-migrate:generate-terms mediums
   *   Usage description
   *
   * @command viola-migrate:generate-terms-file
   * @aliases viola-migrate:generate-terms
   */
  public function generateTerms($vocab) {
    $this->logger()->notice(dt("Running viola-migrate:generate-terms-import ..."));
    if (!in_array($vocab, $this->generatedVocabularies)) {
      $message = sprintf('Invalid Vocabulary, %s. Allowed vocabularies are: %s', $vocab, implode(', ', $this->generatedVocabularies));
      throw new \InvalidArgumentException($message);
    }
    // The output file.
    $outFile = sprintf('%s/%s/%s.%s', $this->dataDirectory, 'taxonomy_terms', $vocab, 'json');
    $info = $this->getTmsInfo($vocab);
    [$bundle, $property, $prefix] = $info;
    // Get the TMS full export.
    $date = date("Y-m-d");
    $filepath = sprintf('tms/full_%s_%s.json', $bundle, $date);
    $reader = new JsonDataReader(
      's3://',
      $filepath,
    );
    $results = $reader->read()->getResults();
    if ($results) {
      // Convert the strings into a nested array of path terms as array keys.
      if ($prefix) {
        $data = $this->getDataFromPathTermField($results, $property, $prefix);
      }
      else {
        $data = $this->getDataFromPlaintextField($results, $property);
      }
      // Write the JSON import file to local file system.
      $json = json_encode($data, JSON_PRETTY_PRINT);
      $saved = \Drupal::service('file_system')->saveData($json, $outFile, FileSystemInterface::EXISTS_REPLACE);
      if ($saved) {
        $this->logger()->notice(dt('Generated import file for @vocab: @saved.', ['@vocab' => $vocab, '@saved' => $saved]));
      }
      else {
        $this->logger()->warning('Nothing was generated.');
      }
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

  /**
   * Create data from a list of Thesaurus "Path Terms" that can be exported as
   * JSON. Thesaurus terms can have parent/child hierarchy.
   *
   * @param array $results
   *   Content results to parse for values.
   * @param $property
   *   The field name that contains the value(s).
   * @param $prefix
   *   The Thesaurus path prefix (to exclude).
   *
   * @return mixed
   */
  protected function getDataFromPathTermField(array $results, $property, $prefix) {
    $tree = [];
    foreach($results as $result) {
      if (isset($result[$property])) {
        // The value is a Thesaurus Term.
        foreach($result[$property] as $term) {
          if (strpos($term['pathTerm'], $prefix) === 0) {
            if ($prefix === 'Authorities\\Attributes\\Objects\\Medium\\') {
              // Make "mediums" lowercase.
              $term['pathTerm'] = mb_strtolower($term['pathTerm']);
            }
            $path = substr($term['pathTerm'], strlen($prefix));
            $parts = explode('\\', $path) ?? [];
            // The current nested path Term.
            $branch = [];
            foreach ($parts as $k => $part) {
              $temp = &$branch;
              // Loop through the exploded values to create nested keys from those values.
              foreach($parts as $key) {
                $temp = &$temp[$key];
              }
              $temp = [];
            }
            // Add the branch to the tree.
            $tree = array_merge_recursive($tree, $branch);
          }
        }
      }
    }
    // Build an array of "term" objects which can be exported as a JSON file.
    // A term object can have two properties, "name" and "children" (itself an array of term objects).
    $data = [];
    array_walk( $tree, function($value, $key) use (&$data) {
      $obj = (object) ['name' => $key ];
      if (is_array($value) && !empty($value)) {
        foreach (array_keys($value) as $child) {
          $childObj = (object) ['name' => $child];
          $obj->children[] = $childObj;
        }
      }
      $data[] = $obj;
    });
    return $data;
  }

  /**
   * Create data from a plain text field that can be exported as JSON. This data
   * will have a flat hierarchy.
   *
   * @param array $results
   *   Content results to parse for values.
   * @param $property
   *   The field name that contains the value(s).
   *
   * @return mixed
   */
  protected function getDataFromPlaintextField(array $results, $property) {
    $values = [];
    foreach ($results as $result) {
      if (isset($result[$property]) && $result[$property]) {
        // The value is a regular plaintext field.
        $values[] = $result[$property];
      }
    }
    $data = [];
    foreach (array_unique($values) as $value) {
      $data[] = ['name' => $value];
    }
    return $data;
  }

  /**
   * Get TMS information about the vocabulary import:
   *   - Get the "bundle" to determine which TMS export file to parse.
   *   - The property name for each artwork/creator which contains the Thesaurus Terms.
   *   - The unused Path Term prefix.
   *
   * @param $vocab
   *
   * @return array
   */
  protected function getTmsInfo($vocab) {
    switch ($vocab) {
      case 'ethnicities':
        return [
          'creator',
          'ethnicity',
          'Authorities\\Attributes\\Constituents\\Ethnicity\\'
        ];
      case 'groups':
        return [
          'creator',
          'affiliation',
          'Authorities\\Attributes\\Constituents\\Affiliation\\'
        ];
      case 'mediums':
        return [
          'artwork',
          'mediums',
          'Authorities\\Attributes\\Objects\\Medium\\'
        ];
      case 'nationalities':
        return [
          'creator',
          'nationality',
          NULL
        ];
      case 'occupations':
        return [
          'creator',
          'occupation',
          'Authorities\\Attributes\\Constituents\\Occupation\\'
        ];
      case 'series':
        return [
          'artwork',
          'seriesTitle',
          NULL
        ];
      default:
        return [];
    }
  }

}
