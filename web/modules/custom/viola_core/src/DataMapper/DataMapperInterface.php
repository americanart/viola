<?php

namespace Drupal\viola_core\DataMapper;

/**
 * A Content DataMapper.
 *   Data and domain logic for managing content from different sources.
 */
interface DataMapperInterface {

  /**
   * @param array $data
   * @return void
   */
  public function setData(array $data);

  /**
   * @return array
   */
  public function getData();

}
