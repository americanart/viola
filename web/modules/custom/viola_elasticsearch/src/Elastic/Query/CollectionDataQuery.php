<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class CollectionDataQuery
 *   Build an Elasticsearch DSL Query with aggregations for data about
 *   artworks and creators.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class CollectionDataQuery extends QueryDslBase {

  public function __construct() {
    parent::__construct();
    // Setting size to zero returns aggregations only.
    $this->setSize(0);
    $aggs =  [
      'artwork' => [
        'filter' => [
          'term' => [
            'type' => 'artwork'
          ],
        ],
        'aggs' => [
          'collections' => [
            'terms' => [
              'field' => 'collection.keyword',
            ],
          ],
          'classifications' => [
            'terms' => [
              'field' => 'classifications.keyword',
              'size' => '10',
            ],
            // Sub aggregations of "Classifications"
            'aggs' => [
              'is_on_view' => [
                'terms' => [
                  'field' => 'isOnView',
                ],
              ],
              'is_open_access' => [
                'terms' => [
                  'field' => 'isOpenAccess',
                ],
              ],
            ]
          ],
          'is_open_access' => [
            'terms' => [
              'field' => 'isOpenAccess',
            ],
          ],
          'is_on_view' => [
            'terms' => [
              'field' => 'isOnView',
            ],
          ],
        ],
      ],
      'creator' => [
        'filter' => [
          'term' => [
            'type' => 'creator'
          ],
        ],
        'aggs' => [
          'gender' => [
            'terms' => [
              'field' => 'gender',
              'missing' => 'male',
            ],
          ],
          'creator_type' => [
            'terms' => [
              'field' => 'creatorType',
            ],
          ],
          'nationalities' => [
            'terms' => [
              'field' => 'nationalities.keyword',
              'size' => '100',
            ],
          ],
          'occupations' => [
            'terms' => [
              'field' => 'occupations.keyword',
              'size' => '50',
            ],
          ],
          'places' => [
            'terms' => [
              'field' => 'places.keyword',
              'size' => '100',
            ],
          ],
        ]
      ],
    ];
    $this->setAggregations($aggs);
  }
}
