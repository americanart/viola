<?php

namespace Drupal\viola_content\Formats\Node\Artwork;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Artwork;

final class SearchIndexFormat extends FormatBase {

  /**
   * @var array
   */
  private $document = [];

  /**
   * Node\Artwork\SearchIndexFormat constructor.
   *   Format a Node for elasticsearch indexing.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Artwork $artwork
   *   The wrapped entity.
   */
  public function __construct(Artwork $artwork) {
    parent::__construct();
    // Add timestamp.
    $this->document['@timestamp'] = time();
    // Accession Number.
    $this->document['accessionNumber'] = $artwork->getAccessionNumber();
    // Classifications.
    $this->document['classifications'] = $artwork->getClassifications();
    // The document type.
    $this->document['type'] = $artwork->getBundle();
    // Path alias.
    $this->document['path'] = $artwork->getPathAlias();
    // Title/Name.
    $this->document['title'] = $artwork->getTitle();
    // Collection.
    $this->document['collection'] = $artwork->getCollection();
    // Reformat the colors data, adding a "weight" property for sorting.
    $colors = $artwork->getColors();
    $numColors = count($colors);
    foreach ($colors as $key => $color) {
      $sortableColors[] = [
        'color' => $color['name'],
        'weight' =>  $numColors - $key,
      ];
    }
    $this->document['colors'] = $sortableColors ?? [];
    // Format the Creators as "nested" field data.
    if ($creators = $artwork->getCreators()) {
      $this->document['creators'] = [];
      foreach ($creators as $creator) {
        $new = ['type' =>  $creator['bundle']];
        $fields = ['constituentId', 'creatorType', 'dateDescription', 'name',
          'path', 'creatorRole', 'order', 'uuid'];
        // Only add specific fields to the new format.
        foreach($fields as $field) {
          if (isset($creator[$field])) {
            $new[$field] = $creator[$field];
          }
        }
        $this->document['creators'][] = $new;
      }
    }
    // Credit.
    $this->document['credit'] = $artwork->getCredit();
    // Format date range for filtering.
    if ($dateRange = $artwork->getDateRange()) {
      if (isset($dateRange['start'], $dateRange['end'])) {
        $this->document['dateRange']['lte'] = $dateRange['end'];
        $this->document['dateRange']['gte'] = $dateRange['start'];
      }
    }
    // Depictions
    $this->document['depictions'] = $artwork->getDepictions();
    // Genres.
    $this->document['genres'] = $artwork->getGenres();
    // Location.
    $this->document['location'] = $artwork->getLocationDescription();
    // Movements.
    $this->document['movements'] = $artwork->getMovements();
    // Build the full text search contents.
    $contents = $artwork->getDefaultDescription();
    // Exhibition History.
    if ($exhibitionHistory = $artwork->getExhibitionHistory()) {
      $contents .= ' ' . $exhibitionHistory;
    }
    if ($markings = $artwork->getMarkings()) {
      $contents .= ' ' . $markings;
    }
    if ($mediumsDescription = $artwork->getMediumsDescription()) {
      $contents .= ' ' . $mediumsDescription;
    }
    if ($translatedTitle = $artwork->getTranslatedTitle()) {
      $contents .= ' ' . $translatedTitle;
    }
    if ($translatedDescriptions = $artwork->getTranslatedDescriptions()) {
      foreach ($translatedDescriptions as $description) {
        $contents .= ' ' . $description['description'];
      }
    }
    if ($dateDescription = $artwork->getDateDescription()) {
      $contents .= ' ' . $dateDescription;
    }
    foreach($artwork->getCitations() as $citation) {
      $contents .= ' ' . trim($citation);
    }
    $this->document['contents'] = $contents;
    // Get the primary image.
    $images = $artwork->getImages();
    $this->document['image'] = reset($images) ?: [];
    // On View.
    $this->document['isOnView'] = $artwork->isOnView() ? 'true' : 'false';
    // Open Access.
    $this->document['isOpenAccess'] =  $artwork->isOpenAccess() ? 'true' : 'false';
    // Mediums.
    $this->document['mediums'] = $artwork->getMediums();
    // Object ID.
    $this->document['objectId'] = $artwork->getObjectId();
    // Periods.
    $this->document['periods'] = $artwork->getPeriods();
    // Ontology.
    $this->document['ontology'] = $artwork->getOntology();
    // Reformat usage rights as a term.
    $usageRights = $artwork->getUsageRights();
    if ($usageRights && isset($usageRights['name'])) {
      $this->document['usageRights'] = $usageRights['name'];
    }
  }

  /**
   * @return array;
   */
  public function transform() {
    return $this->document;
  }

}
