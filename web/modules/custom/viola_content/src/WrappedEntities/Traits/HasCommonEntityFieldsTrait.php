<?php

namespace Drupal\viola_content\WrappedEntities\Traits;

trait HasCommonEntityFieldsTrait {

  /**
   * @return string
   */
  public function getBundle() {
    return $this->getEntity()->bundle();
  }

  /**
   * @return int
   */
  public function getDrupalId() {
    return $this->getEntity()->id();
  }

  /**
   * @return string
   */
  public function getPathAlias() {
    return \Drupal::service('path_alias.manager')
      ->getAliasByPath('/node/'. $this->getEntity()->id());
  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->getEntity()->label();
  }

  /**
   * @return string
   */
  public function getUuid() {
    return $this->getEntity()->uuid();
  }

}
