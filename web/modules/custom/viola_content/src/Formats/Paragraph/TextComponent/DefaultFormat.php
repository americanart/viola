<?php

namespace Drupal\viola_content\Formats\Paragraph\TextComponent;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Paragraph\TextComponent;

final class DefaultFormat extends FormatBase {

  public $text;

  /**
   * Paragraph\TextComponent\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Paragraph\TextComponent $component
   *   The wrapped entity.
   */
  public function __construct(TextComponent $component) {
    parent::__construct();
    $this->bundle = $component->getEntity()->bundle();
    $this->type = $component->getEntity()->getEntityTypeId();
    $this->text = $component->getText();
  }

}
