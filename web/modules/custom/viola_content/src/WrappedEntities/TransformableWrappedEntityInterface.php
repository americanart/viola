<?php

namespace Drupal\viola_content\WrappedEntities;

use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;

interface TransformableWrappedEntityInterface extends WrappedEntityInterface {

  /**
   * Transform a WrappedEntity.
   *
   * @param string $format
   * @return mixed
   */
  public function transformInto($format);

}
