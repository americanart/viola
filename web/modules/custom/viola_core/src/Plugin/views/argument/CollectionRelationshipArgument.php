<?php

namespace Drupal\viola_core\Plugin\views\argument;

use Drupal\views\Annotation\ViewsArgument;
use Drupal\views\Views;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument\NumericArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Database\Query\Condition;

/**
 * Argument handler to accept an entity id.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("collection_relationship_arg")
 */
class CollectionRelationshipArgument extends NumericArgument implements ContainerFactoryPluginInterface {

  /**
   * Database service object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * @var array
   */
  protected $allowedBundles = [
    'artwork',
    'creator'
  ];

  /**
   * Constructs the SimilarReferencesArgument object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $connection, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->connection = $connection;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Define default values for options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['target_bundle'] = ['default' => []];

    return $options;
  }

  /**
   * Build options settings form.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['target_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Target bundle'),
      '#description' => $this->t('The type of content to return from a collection relationship.'),
      '#options' => ['artwork' => 'Artwork', 'creator' => 'Creator'],
      '#default' => 'artwork',
    ];
  }


  /**
   * Add filter(s).
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function query($group_by = FALSE) {
    $nid = reset($this->value);
    $node = $this->entityTypeManager->getStorage('node')->load($nid);
    if (!$node
      || !in_array($node->bundle(), $this->allowedBundles)
      || !$node->bundle() == $this->options['target_bundle']) {
      return;
    }
    $this->ensureMyTable();
    $identifier = $node->bundle() == 'artwork' ? 'object_id' : 'constituent_id';
    $id = $node->get(sprintf('field_%s', $identifier))->value;

    if ($id) {
      // Create a sub query to retrieve ID's of entities with a Collection Relationship from the JSON native field.
      $subQuery = $this->getSubQuery($this->options['target_bundle'], $identifier, $id);
      if ($subQuery) {
        $results = $subQuery->fetchAllAssoc('entity_id');
        $ids = array_keys($results);
        if ($ids) {
          // Add the sub query results as a WHERE condition to the View query.
          $this->query->addWhere(0, "node.nid", $ids, 'IN');
        }
      }
    }
  }

  /**
   * Get the subquery for a the desired content type.
   *
   * @param string $target_bundle
   * @param string $identifier
   * @param int $id
   *
   * @return \Drupal\Core\Database\StatementInterface
   */
  protected function getSubQuery($target_bundle, $identifier, $id) {
    if ($target_bundle == 'artwork') {
      // Filter Artworks with relationship to a Creator.
      return $this->connection->query("
        SELECT entity_id
        FROM
            `node__field_collection_relationships`,
            JSON_TABLE(
                `field_collection_relationships_value`,
                '$[*]' COLUMNS(
                    object_id INT PATH '$.objectId',
                    constituent_id INT PATH '$.constituentId',
                    role VARCHAR(64) PATH '$.role'
                )
            ) AS relationships
        WHERE bundle = 'artwork'
        AND constituent_id = :id",
        [
          ':id' => $id,
        ]
      );
    }
    elseif ($target_bundle == 'creator') {

    }
    return NULL;
  }

  /**
   * Get the artwork/creator id associated with a given Node ID.
   *
   * @param int $entity_id
   *   The entity ID.
   *
   * @return array
   *   An array of entity ids.
   */
//  public function getCollectionId($entity_id) {
//    // We're filtering on the opposite of the target bundle.
//    $bundle = $this->getFilterBundle($this->options['target_bundle']);
//    $field = $bundle == 'artwork' ? 'field_object_id' : 'field_constituent_id';
//    $table = sprintf('node__%s', $field);
//    $column = sprintf('%s_value', $field);
//    $select = $this->connection->select($table, 'fd');
//    $select->addField('fd', $column);
//    $select->condition('bundle', $bundle);
//    $select->condition('entity_id', $entity_id);
//    $select->distinct();
//    $results = $select->execute()->fetchCol();
//    return reset($results);
//  }

  /**
   * Get the bundle to filter on when returning results for a target bundle.
   *
   * @param string $target_bundle
   *
   * @return string
   */
//  public function getFilterBundle($target_bundle) {
//    switch ($target_bundle) {
//      case 'artwork':
//        return 'creator';
//      case 'creator':
//        return 'artwork';
//      default:
//        return '';
//    }
//  }



}
