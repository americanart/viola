<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class TypeAheadQuery
 *   Build an Elasticsearch DSL Query to return "type ahead" search suggestions.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class SearchSuggestionsQuery extends QueryDslBase {

  public function __construct($params) {
    parent::__construct();
    $query = [
      'bool' => [
        'must' => [
          'multi_match' => [
            'query' => $params['q'],
            'type' => 'bool_prefix',
            'fields' => [
              'title',
              'title._2gram',
              'title._3gram',
              'object_number'
            ],
          ],
        ],
        'filter' => [
          'terms' => [
            'type' => $params['types'],
          ],
        ],
      ],
    ];
    $this->setQuery($query);
  }
}
