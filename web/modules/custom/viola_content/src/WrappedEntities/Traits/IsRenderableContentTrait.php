<?php

namespace Drupal\viola_content\WrappedEntities\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\RendererInterface;

trait IsRenderableContentTrait {

  /**
   * Get a Node's rendered HTML output.
   *
   * @param EntityInterface $entity
   * @param string $view_mode
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The rendered HTML.
   */
  public function getRenderedContents($entity, $view_mode = 'full') {
    $renderArray = \Drupal::service('entity_type.manager')->getViewBuilder('node')->view($entity, $view_mode);
    /** @var RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    return $renderer->renderPlain($renderArray);
  }

}
