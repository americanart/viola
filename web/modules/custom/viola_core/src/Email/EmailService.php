<?php

namespace Drupal\viola_core\Email;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * Class EmailService.
 */
class EmailService implements EmailServiceInterface {

  /**
   * Drupal\Core\Mail\MailManagerInterface definition.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $pluginManagerMail;

  /**
   * LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Headers required for HTML email.
   * @var array
   */
  protected static $htmlHeaders = [
    'MIME-Version' => '1.0',
    'Content-Type' => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
  ];

  /**
   * Constructs a new EmailService object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $plugin_manager_mail
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   */
  public function __construct(MailManagerInterface $plugin_manager_mail, LoggerChannelFactory $logger_factory) {
    $this->pluginManagerMail = $plugin_manager_mail;
    $this->logger = $logger_factory->get('viola_core');
  }

  /**
   * Wrap an email message in HTML tags.
   *
   * @param string $message
   * @param array $tags
   *   Allow HTML tags.
   *
   * @return string
   */
  public function prepareHtmlMessage($message, $tags = ['a', 'br', 'em', 'strong', 'table', 'td', 'th', 'tr']) {
    $message = Xss::filter($message, $tags);
    return sprintf('<html><body>%s</body></html>', $message);
  }

  /**
   * @inheritDoc
   */
  public function send($key, $recipients, $subject, $message) {
    try {
      // The email body is composed in hook_mail function.
      $params['subject'] = $subject;
      $params['message'] = $message;
      $params['headers'] = self::$htmlHeaders;

      $result = $this->pluginManagerMail->mail('viola_core', $key, $recipients, 'en', $params, NULL, TRUE);
      return $result['result'];
    }
    catch (\Exception $e) {
      $this->logger->log('error', $e->getMessage());
      return [];
    }
  }

}
