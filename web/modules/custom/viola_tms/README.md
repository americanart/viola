# Viola TMS

## Commands

### `drush viola-tms:import`

Import artworks and creators from the TMS extract.

```shell script
// The daily "audited" artwork import.
drush viola-tms:import artwork
```

```shell script
// The daily "audited" creator import.
drush viola-tms:import creator
```

```shell script
// The "audited" artwork import from a specific date.
drush viola-tms:import artwork --date=1970-11-30
```

```shell script
// The "full" artwork import from a specific date.
drush viola-tms:import artwork --date=1970-11-30 --full
```

```shell script
// Force the update of an artwork that would normally be skipped.
drush viola-tms:import artwork --ids=90044 --force --full
```

### `drush viola-tms:color`

Extract colors from an Artwork image, and store results as color Terms.

```shell
// Extract colors from two specific Nodes.
drush viola-tms:color 19876,65491
```

