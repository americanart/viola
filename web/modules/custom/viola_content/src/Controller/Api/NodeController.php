<?php

namespace Drupal\viola_content\Controller\Api;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Cache\CacheableJsonResponse;
use Drupal\Core\Cache\CacheableMetadata;

/**
 * Returns responses for routes.
 */
class NodeController extends ControllerBase {


  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack;
   */
  protected $requestStack;
  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;
  protected $uuid;
  protected $bundle;
  protected $node;
  protected $contents;
  protected $format;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requestStack = $container->get('request_stack');
    $instance->loggerFactory = $container->get('logger.factory');
    return $instance;
  }

  /**
   * Retrieve a Node's default JSON payload.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   */
  public function get($format) {
    try {
      $this->bundle = $this->requestStack->getCurrentRequest()->get('bundle');
      $this->uuid = $this->requestStack->getCurrentRequest()->get('uuid');
      if (in_array($format, ['default', 'json_ld'])) {
        $this->format = $format;
      }
      // Workaround for caching issue
      // @see: https://www.lullabot.com/articles/early-rendering-a-lesson-in-debugging-drupal-8
      /* @var \Drupal\Core\Cache\CacheableDependencyInterface $result */
      $context = new RenderContext();
      $result = \Drupal::service('renderer')->executeInRenderContext($context, function() {
        /** @var \Drupal\viola_content\Plugin\TypedRepositories\ArtworkRepository $repository */
        $repository = \Drupal::service(RepositoryManager::class)->repository('node', $this->bundle);
        $wrappedEntity = $repository->firstWhere(['uuid' => $this->uuid]);
        if ($wrappedEntity instanceof TransformableWrappedEntity) {
          $this->contents = $wrappedEntity->transformInto($this->format);
          $this->node = $wrappedEntity->getEntity();
        }
      });
      // Handle any bubbled cacheability metadata.
      if (!$context->isEmpty()) {
        $bubbleableMetadata = $context->pop();
        BubbleableMetadata::createFromObject($result)->merge($bubbleableMetadata);
      }
      $response = new CacheableJsonResponse($this->contents);
      // Add Cache settings.
      if ($this->node instanceof NodeInterface) {
        $cacheMetadata = new CacheableMetadata();
        $cacheMetadata->addCacheableDependency($this->node);
        $cacheMetadata->addCacheContexts($this->node->getCacheContexts());
        $cacheMetadata->addCacheTags($this->node->getCacheTags());
        $response->addCacheableDependency($cacheMetadata);
      }
      return $response;
    }
    catch (RequestException $exception) {
      $this->loggerFactory->get('viola_content')->error($exception->getMessage());
      return new JsonResponse([
        'code' => $exception->getCode(),
        'message' => $exception->getMessage(),
      ]);
    } catch (\Exception $exception) {
      $this->loggerFactory->get('viola_content')->error($exception->getMessage());
    }
    return null;
  }

}
