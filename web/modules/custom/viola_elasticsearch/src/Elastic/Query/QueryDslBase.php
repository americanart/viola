<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class QueryDslBase
 *   Build an Elasticsearch DSL Query.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class QueryDslBase implements QueryDslInterface {

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Index
   */
  static protected $index;
  /**
   * @var int
   */
  static protected $start = 0;
  /**
   * @var int
   */
  static protected $size = 20;
  /**
   * @var array
   */
  static protected $sort = [];
  /**
   * @var array
   */
  static protected $aggregations = [];
  /**
   * @var array
   */
  static protected $query = [];

  /**
   * Class constructor.
   */
  public function __construct() {
    self::$index = new Index();
  }

  /**
   * @return array
   */
  public static function build() {
    $dsl = [
      'index' => self::$index->getIndexName(),
      'body' => [
        'from' => self::$start,
        'size' => self::$size,
        'sort' => self::$sort,
        'query' => self::$query,
      ],
    ];
    if (self::$aggregations) {
      $dsl['body']['aggs'] = self::$aggregations;
    }
    return $dsl;
  }

  /**
   * @param array $sort
   */
  public function setSort(array $sort) {
    self::$sort = $sort;
    return $this;
  }

  /**
   * @param array $aggregations
   */
  public function setAggregations(array $aggregations) {
    self::$aggregations = $aggregations;
    return $this;
  }

  /**
   * @param array $query
   */
  public function setQuery(array $query) {
    self::$query = $query;
    return $this;
  }

  /**
   * @param int $start
   */
  public function setStart(int $start) {
    self::$start = $start;
    return $this;
  }

  /**
   * @param int $size
   */
  public function setSize(int $size) {
    self::$size = $size;
    return $this;
  }

}
