# Viola Linked Data

## Commands

### `drush viola-ld:ids`

Update Creators with Linked Data identifiers.

```shell script
// The full import of Artist or Artwork linked data ids.
drush viola-ld:ids creator
```

```shell script
// Update a specific Artwork by Object ID.
drush viola-ld:ids artwork --ids 1
```
