<?php

namespace Drupal\viola_tms\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_core\Queue\QueueManager;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaTmsColorCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var int
   */
  public $queued;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Extract colors from TMS artwork images.
   *
   * @param array $ids
   *   Comma-separated list of specific Node id(s) to extract colors.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option id
   *   Specific content id(s) to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @usage viola-tms:color
   *
   * @command viola-tms:color-extraction
   * @aliases viola-tms:color
   */
  public function color($ids = '', $options = [
    'limit' => 55000,
    'offset' => 0
  ]) {
    $this->logger()->notice(dt("Running viola-tms:color ..."));
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($ids) ?? [];
    if (!$ids) {
      $query = \Drupal::entityQuery('node');
      $query
        ->condition('status', 1)
        ->condition('type', 'artwork')
        ->condition('field_data', '', '<>');
      if ($options['limit'] || $options['offset']) {
        $query->range($options['offset'], $options['limit']);
      }
      $ids = $query->execute();
    }

    if ($ids) {
      $batch = [
        'title' => dt('Queue import of TMS color extraction processing ...'),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_tms\BatchService::batchFinished',
      ];
      $chunks = array_chunk($ids, 10);
      foreach ($chunks as $key => $chunk) {
        $nodes = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($chunk);
        foreach ($nodes as $node) {
          if ($objectId = $node->get('field_object_id')->value) {
            // Prepare the operations.
            $batch['operations'][] = [
              'Drupal\viola_tms\BatchService::batchProcessColor', [
                $objectId,
                dt('Queue color extraction for Object: @id', ['@id' => $objectId,]),
              ],
            ];
          }
          unset($node);
        }
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

}
