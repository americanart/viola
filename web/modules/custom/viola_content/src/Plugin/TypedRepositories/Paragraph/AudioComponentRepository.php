<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Paragraph;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for `audio_component` paragraphs.
 *
 * @TypedRepository(
 *    entity_type_id = "paragraph",
 *    bundle = "audio_component",
 *    wrappers = @ClassWithVariants(
 *      fallback = "Drupal\viola_content\WrappedEntities\Paragraph\AudioComponent",
 *    ),
 *   description = @Translation("Repository that holds logic for audio components")
 * )
 */
final class AudioComponentRepository extends TypedRepositoryBase {}
