<?php

namespace Drupal\viola_linked_data\DataMapper;

use Drupal\viola_core\DataMapper\DataMapper;

/**
 * ArtistLinkedData DataMapper
 *
 * Map data from custom "artist linked data" data to Creator node fields.
 *
 */
class CreatorIdentifier extends DataMapper {

  /**
   * @param array $field_data
   */
  public function __construct($field_data = []){
    $this->setData($field_data);
  }

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {
    $this->data = [];

    // Constituent ID.
    if (self::isValidDataKey('saam_constituent_id', $field_data)) {
      $this->data['field_constituent_id'] = $field_data['saam_constituent_id'];
    }
    // ULAN ID.
    if (self::isValidDataKey('ulan_id', $field_data)) {
      $this->data['field_ulan_id'] = $field_data['ulan_id'];
    }
    // VIAF ID.
    if (self::isValidDataKey('viaf_id', $field_data)) {
      $this->data['field_viaf_id'] = $field_data['viaf_id'];
    }
    // Library of Congress ID.
    if (self::isValidDataKey('library_of_congress_id', $field_data)) {
      $this->data['field_loc_id'] = $field_data['library_of_congress_id'];
    }
    // Wikidata ID.
    if (self::isValidDataKey('wikidata_id', $field_data)) {
      $this->data['field_wikidata_id'] = $field_data['wikidata_id'];
    }
    // Wikipedia Title.
    if (self::isValidDataKey('wikipedia_title', $field_data)) {
      $this->data['field_wikipedia_title'] = $field_data['wikipedia_title'];
    }

  }

}
