<?php

namespace Drupal\viola_content\Plugin\TypedRepositories\Traits;

/**
 * Trait handling Entity operation status messages.
 */
trait OutputsStatusMessageTrait {

  /**
   * Return status message of a CRUD operation.
   *
   * @param int $status
   *   The status of a CRUD operation.
   *
   * @return string
   *   The human word representing the supplied status integer.
   */
  public static function getStatusMessage($status = 0) {
    switch ($status) {
      case SAVED_NEW:
        return 'Created';

      case SAVED_UPDATED:
        return 'Updated';

      case SAVED_DELETED:
        return 'Deleted';

      case -1:
        return 'Skipped';

      default:
        return 'Failed';
    }
  }

}
