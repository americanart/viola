<?php

namespace Drupal\viola_content\Formats\Paragraph\AudioComponent;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Paragraph\AudioComponent;

final class DefaultFormat extends FormatBase {

  public array $audio = [];

  /**
   * Paragraph\AudioComponent\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Paragraph\AudioComponent $component
   *   The wrapped entity.
   */
  public function __construct(AudioComponent $component) {
    parent::__construct();
    $this->bundle = $component->getEntity()->bundle();
    $this->type = $component->getEntity()->getEntityTypeId();
    // Get audio media entity data.
    $wrapped = $component->getAudio();
    foreach($wrapped as $component) {
      $this->audio[] = $component->transformInto('default');
    }
  }

}
