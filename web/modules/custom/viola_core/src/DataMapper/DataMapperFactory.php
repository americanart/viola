<?php

namespace Drupal\viola_core\DataMapper;

use Drupal\viola_core\Utils\Strings;

/**
 * Class DataMapperFactory.
 */
class DataMapperFactory {

  /**
   * Instantiate an DataMapper.
   *
   * @param string $module
   *   The custom module that implements the DataMapper (e.g. "viola_tms")
   * @param string $type
   *   The content type (e.g. "artwork")
   * @param array $data
   *
   * @return DataMapperInterface
   *
   * @throws \Exception
   */
  public static function create($module, $type, $data) {
    try {
      if (!\Drupal::service('module_handler')->moduleExists($module)) {
        throw new \Exception(sprintf('The module %s does not exist, or is not enabled.', $module));
      }
      // Convert the type from snake_case to TitleCase.
      $type = Strings::titleCase($type);
      $namespace = __NAMESPACE__;
      if ($module !== 'viola_core') {
        $namespace = str_replace('viola_core', $module, $namespace);
      }
      $classPath = sprintf("\\%s\\%s", $namespace, $type);
      return new $classPath($data);
    }
    catch (\Exception $e) {
      throw $e;
    }
  }

}
