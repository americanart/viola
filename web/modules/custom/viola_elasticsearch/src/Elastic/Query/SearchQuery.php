<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_core\SystemInfo;
use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class SearchQuery
 *   Build an Elasticsearch DSL Query to return all content matching a search phrase.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class SearchQuery extends QueryDslBase {

  public function __construct($params) {
    parent::__construct();
    $aggs = $this->getAggs();
    $this->setAggregations($aggs);

    $query = [
      'bool' => [
        //'minimum_should_match' => 1,
        'filter' => [],
        'must' => [],
        'should' => [],
      ],
    ];
    // The search phrase.
    if (!empty($params['q'])) {
      $query['bool']['should'][] = [
          'multi_match' => [
            'query' => $params['q'],
            '_name' => 'search_match',
            'fields' => [
              'title^3',
              'contents',
              'accessionNumber',
              'classifications^2',
              'colors',
              'creatorType',
              'credit',
              'depictions',
              'keywords^2',
              'ontology',
              'type',
              'places'
            ],
          ],
        ];
      // Increase score of title matches even if misspelled. (fuzzy)
      $query['bool']['should'][] = [
        'match' => [
          'title' => [
            'query' => $params['q'],
            '_name' => 'fuzzy_title_match',
            'fuzziness' => 'auto',
          ],
        ],
      ];
      // Increase score if words are in order and close to one another.
      $query['bool']['should'][] = [
        'match_phrase' => [
          'title' => [
            'query' => $params['q'],
            'slop' => 5,
            '_name' => 'title_proximity_boost',
          ],
        ],
      ];
      // Increase score if matches the name of a related creator.
      // For example: Display an artist's artworks when searching for an artist by name.
      $query['bool']['should'][] = [
        'nested' => [
          'path' => 'creators',
          '_name' => 'related_creator_boost',
          'query' => [
            'multi_match' => [
              'query' => $params['q'],
              'type' => 'best_fields',
              'fields' => ['creators.name^3'],
            ],
          ],
        ],
      ];
    }

    // Date range filters.
    if (!empty($params['event_start_date']) && !empty($params['event_end_date'])) {
      $query['bool']['should'][] = [
        'nested' => [
          'path' => 'venues',
          'query' => [
            'bool' => [
              // TODO: Add venue name filter.
              'must' => [
                [
                  'range' => [
                    'venues.dateRange' => [
                      'gte' => $params['event_start_date'],
                      'lte' => $params['event_end_date'],
                      'boost' => 2,
                    ],
                  ],
                ]
              ],
            ],
          ],
        ],
      ];
    }

    // Filters.
    $contentTypes = SystemInfo::getAllContentTypes();
    if (!empty($params['type']) && in_array($params['type'], array_keys($contentTypes))) {
      $query['bool']['filter'][] = [
        'terms' => [
          'type' => [$params['type']],
        ],
      ];
      // Artwork sub filters.
      if ($params['type'] == 'artwork') {
        if (!empty($params['open_access']) && $params['open_access'] == 'true') {
          $query['bool']['filter'][] = [
            'term' => [
              'isOpenAccess' => 'true',
            ],
          ];
        }
        if (!empty($params['on_view']) && $params['on_view'] == 'true') {
          $query['bool']['filter'][] = [
            'term' => [
              'isOnView' => 'true',
            ],
          ];
        }
        // Artwork sub filters that allow multiple values.
        $keywords = ['classifications', 'mediums', 'periods', 'collection'];
        foreach ($params as $key => $value) {
          if (in_array($key, $keywords)) {
            // Filter by keyword.
            if(is_array($value)) {
              foreach ($value as $key2 => $value2) {
                $size = sizeof($query['bool']['filter']);
                $query['bool']['filter'][$size] =
                  ['term' => [
                    $key . '.keyword' => $value2
                  ]];
              }
            } else {
              $query['bool']['filter'][] = [
                'term' => [
                  $key . '.keyword' => $value
                ]
              ];
            }
          }
          elseif ($key == 'colors') {
            $colors = is_array($value) ? $value : [$value];
            $nestedQueries = [];
            foreach ($colors as $color) {
              $nestedQueries[] = [
                'nested' => [
                  'path' => 'colors',
                  'query' => [
                    'bool' => [
                      'filter' => [
                        [
                          'term' => [
                            'colors.color' => $color,
                          ]
                        ],
                      ]
                    ]
                  ],
                ],
              ];
            }
            $query['bool']['must'] = $nestedQueries;
            // Create a sort on the "weight" field in nested "colors" objects
            // @link: https://www.elastic.co/guide/en/elasticsearch/reference/6.8/search-request-sort.html#_nested_sorting_examples
            $sorts = [
              'colors.weight' => [
                'mode' => 'max',
                'order' => 'desc',
                'nested' => [
                  'path' => 'colors',
                  'filter' => [
                    'terms' => [
                      'colors.color.keyword' => $colors,
                    ],
                  ],
                ],
              ]
            ];
          }
        }

      }
      // Creator sub filters.
      if ($params['type'] == 'creator') {
        if (!empty($params['gender'])) {
          $query['bool']['filter'][] = [
            'terms' => [
              'gender' => is_array($params['gender']) ? $params['gender'] : [$params['gender']],
            ],
          ];
        }
        // Creator sub filters that allow multiple values.
        $keywords = ['groups', 'nationalities', 'occupations', 'places', 'ethnicities'];
        foreach ($params as $key => $value) {
          if (in_array($key, $keywords)) {
            // Filter by keyword.
            if(is_array($value)) {
              foreach ($value as $key2 => $value2) {
                $size = sizeof($query['bool']['filter']);
                $query['bool']['filter'][$size] =
                  ['term' => [
                    $key . '.keyword' => $value2
                  ]];
              }
            } else {
              $query['bool']['filter'][] = [
                'term' => [
                  $key . '.keyword' => $value
                ]
              ];
            }
          }
        }
      }

    }
    $this->setQuery($query);
  }

  protected function getAggs() {
    return [
      'artwork' => [
        'filter' => [
          'term' => [
            'type' => 'artwork'
          ],
        ],
        'aggs' => [
          'collections' => [
            'terms' => [
              'field' => 'collection.keyword',
            ],
          ],
          'is_open_access' => [
            'terms' => [
              'field' => 'isOpenAccess',
            ],
          ],
          'is_on_view' => [
            'terms' => [
              'field' => 'isOnView',
            ],
          ],
        ],
      ],
      'creator' => [
        'filter' => [
          'term' => [
            'type' => 'creator'
          ],
        ],
        'aggs' => [
          'gender' => [
            'terms' => [
              'field' => 'gender',
              'missing' => 'male',
            ],
          ],
          'creator_type' => [
            'terms' => [
              'field' => 'creatorType',
            ],
          ],
        ]
      ],
    ];
  }
}
