<?php

namespace Drupal\viola_content\WrappedEntities\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\Traits\IsRenderableContentTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\viola_core\Utils\Strings;

/**
 * The wrapped entity for the page content type.
 */
class Page extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;
  use IsRenderableContentTrait;
  use HasCommonEntityFieldsTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Web Typography utilities service.
   *
   * @var \Drupal\viola_core\Typography\TypographyService
   */
  protected $typography;

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   */
  public function __construct(EntityInterface $entity, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity);
    $this->entityTypeManager = $entityTypeManager;
    $this->typography = \Drupal::service('viola.typography');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityInterface $entity): self {
    return new static(
      $entity,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @return string
   */
  public function getBlurb() {
    return $this->getEntity()->get('field_blurb')->value ?? '';
  }

  /**
   * @return array
   */
  public function getComponents() {
    return $this->wrapReferencedEntites($this->getEntity(), 'field_components');
  }

  /**
   * @return string
   */
  public function getFormattedTitle() {
    return $this->getEntity()->get('field_formatted_title')->value ?? '';
  }

  /**
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   */
  public function getImage() {
    $images = $this->wrapReferencedEntites($this->getEntity(), 'field_image');
    return reset ($images);
  }

  /**
   * @return string
   */
  public function getSubtitle() {
    return $this->getEntity()->get('field_subtitle')->value ?? '';
  }

  /**
   * @return string
   */
  public function getSummary() {
    return $this->getEntity()->get('field_summary')->value ?? '';
  }

}
