<?php

namespace Drupal\viola_tms\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Annotation\AdvancedQueueJobType;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;
use Drupal\viola_core\Plugin\AdvancedQueue\JobType\ImportJobBase;
use Exception;

/**
 * @AdvancedQueueJobType(
 *   id = "collection_import_job",
 *   label = @Translation("Collection Import Job"),
 *   max_retries = 3,
 *   retry_delay = 79200,
 * )
 */
class CollectionImportJob extends ImportJobBase {

  /**
   * @var array
   *   Allowed Node bundles to act upon in a job.
   */
  protected static $allowedNodeBundles = [
    'artwork',
    'creator',
  ];

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function process(Job $job) {
    try {
      $payload = $job->getPayload();
      $status = 0;
      $node = NULL;

      if ($payload['entity_type'] != 'node') {
        throw new Exception(sprintf('Invalid entity type: %s.', $payload['entity_type']));
      }
      if (!in_array($payload['bundle'], self::$allowedNodeBundles)) {
        throw new Exception(sprintf('Invalid bundle: %s.', $payload['bundle']));
      }

      /** @var \Drupal\viola_content\Plugin\TypedRepositories\ArtworkRepository|\Drupal\viola_content\Plugin\TypedRepositories\CreatorRepository $repository */
      $repository = \Drupal::service(RepositoryManager::class)->repository($payload['entity_type'], $payload['bundle']);

      if (!isset($payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME])
        || empty($payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME])) {
        throw new Exception('Missing required fields.');
      }

      if (isset($payload['data']['nid'])) {
        // Get existing Node from id.
        $node = Node::load($payload['data']['nid']);
      }
      else {
        // Get existing Node from alternative id.
        if (isset($payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME])) {
          $existingNode = $repository->firstWhere([
            $repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME => $payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]
          ]);
          if ($existingNode) {
            $node = $existingNode->getEntity();
          }
        }
      }
      if ($node && $node instanceof NodeInterface) {
        // Skip if checksums match and the "force" flag is not set.
        if (!isset($payload['data']['force'])
          && isset($payload['data']['field_checksum'])
          && self::skipImport($node, $payload['data']['field_checksum'])
        ) {
          return JobResult::success($repository::getStatusMessage(self::SKIPPED));
        }
        unset($payload['data']['force']);
        if ($payload['action'] == 'delete') {
          $status = $repository->deleteNode($node);
        }
        else {
          $status = $repository->updateNode($payload['data'], $node);
        }
      }
      else {
        // Create a new Node.
        unset($payload['data']['nid']);
        if ($payload['action'] == 'create') {
          $status = $repository->createNode($payload['data']);
        }
      }
      if ($status) {
        return JobResult::success($repository::getStatusMessage($status));
      }
      // Default.
      return JobResult::failure($repository::getStatusMessage($status));
    }
    catch (Exception $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}
