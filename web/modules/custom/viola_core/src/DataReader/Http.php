<?php

namespace Drupal\viola_core\DataReader;

/**
 * Retrieve files via HTTP(S).
 */
class Http {

  /**
   * Get a file via HTTP Request.
   *
   * @param string $url
   *   The url of the resource.
   * @param array $options
   *   Optional request headers.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   */
  public static function getContents($url, $options = []) {
    try {
      $client = \Drupal::httpClient();
      return $client->get($url, $options);
    }
    catch (\Exception $e) {
      \Drupal::logger('http_reader')->error(sprintf('HTTP DataReader: Failed to retrieve %s, Code: %s, Message: %s', $url, $e->getCode(), $e->getMessage()));
      return NULL;
    }
  }

}
