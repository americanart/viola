<?php

namespace Drupal\viola_content\Formats\Paragraph\EmbedComponent;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Paragraph\EmbedComponent;

final class DefaultFormat extends FormatBase {

  public $aspectRatio;
  public $embedCode;

  /**
   * Paragraph\EmbedComponent\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Paragraph\EmbedComponent $component
   *   The wrapped entity.
   */
  public function __construct(EmbedComponent $component) {
    parent::__construct();
    $this->bundle = $component->getEntity()->bundle();
    $this->type = $component->getEntity()->getEntityTypeId();
    $this->aspectRatio = $component->getAspectRatio();
    $this->embedCode = $component->getEmbedCode();
  }

}
