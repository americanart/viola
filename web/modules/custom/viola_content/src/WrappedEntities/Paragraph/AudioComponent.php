<?php

namespace Drupal\viola_content\WrappedEntities\Paragraph;

use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the `audio_component` paragraph.
 */
class AudioComponent extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;

  /**
   * @return array
   */
  public function getAudio() {
    return $this->wrapReferencedEntites($this->getEntity(), 'field_audio');
  }

}
