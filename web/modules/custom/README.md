# Viola Custom Modules

## Modules

- [viola_content](./viola_content/README.md) contains functionality for working with Viola content.
- [viola_core](./viola_core/README.md) contains core functionality common to all custom modules.
- [viola_elasticsearch](./viola_elasticsearch/README.md) provides integration with Elasticsearch service(s).
- [viola_headless](./viola_headless/README.md) implements customizations related to Headless CMS functionality.
- [viola_linked_data](./viola_linked_data/README.md) contains functionality related to LOD.
- [viola_migrate](./viola_migrate/README.md) contains project installation and content migration tools.
- [viola_media](./viola_media/README.md) contains media customizations.
- [viola_tms](./viola_tms/README.md) tools for importing content from a TMS (collection management software).
- [viola_trumba](./viola_trumba/README.md) tools for importing content from a Trumba (event management software).
- [viola_wikimedia](./viola_wikimedia/README.md) tools for integrating with Wikimedia projects.


