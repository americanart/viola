<?php

namespace Drupal\viola_elasticsearch\Controller;

use Drupal\Component\Utility\Tags;
use Drupal\Core\Controller\ControllerBase;
use Drupal\viola_core\SystemInfo;
use Drupal\viola_elasticsearch\Elastic\EsClient;
use Drupal\viola_elasticsearch\Elastic\Query\ArtistGlossaryQuery;
use Drupal\viola_elasticsearch\Elastic\Query\CollectionDataQuery;
use Drupal\viola_elasticsearch\Elastic\Query\FilterArtistsQuery;
use Drupal\viola_elasticsearch\Elastic\Query\FilterArtworksQuery;
use Drupal\viola_elasticsearch\Elastic\Query\QueryDslBase;
use Drupal\viola_elasticsearch\Elastic\Query\SearchQuery;
use Drupal\viola_elasticsearch\Elastic\Query\SearchSuggestionsQuery;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class SearchController.
 */
class QueryController extends ControllerBase {

  /**
   * @var \Symfony\Component\HttpFoundation\RequestStack;
   */
  protected $requestStack;
  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;
  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Index
   */
  protected $index;
  /**
   * @var int
   */
  protected $start = 0;
  /**
   * @var int
   */
  protected $size = 20;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->requestStack = $container->get('request_stack');
    $instance->loggerFactory = $container->get('logger.factory');
    $instance->index = new Index();
    return $instance;
  }

  /**
   * Get search results with aggregate counts.
   *
   * @return JsonResponse
   *   Given a partial return an array of matching phrases.
   */
  public function search() {
    $allowed = [
      'classifications',
      'collection',
      'colors',
      'ethnicities',
      'event_end_data',
      'event_start_data',
      'gender',
      'groups',
      'mediums',
      'museum',
      'nationalities',
      'occupations',
      'on_view',
      'open_access',
      'periods',
      'places',
      'q',
      'type',
    ];
    $params = array_filter(
      $this->requestStack->getCurrentRequest()->query->all(),
      fn ($key) => in_array($key, $allowed),
      ARRAY_FILTER_USE_KEY
    );
    $dslQuery = new SearchQuery($params);

    $offset = $this->requestStack->getCurrentRequest()->query->get('offset');
    $start = is_numeric($offset) ? $offset : $this->start;
    $dslQuery->setStart($start);
    $size = $this->requestStack->getCurrentRequest()->query->get('size');
    $size = is_numeric($size) ? $size : $this->size;
    $dslQuery->setSize($size);

    $dsl = $dslQuery::build();
    // Exclude specific fields from source.
    $dsl['body']['_source'] = [
      'excludes' => [
        'creators.objectId',
        'creators.order',
        'dimensions',
      ],
    ];

    return $this->execute($dsl);
  }

  /**
   * Get autocomplete suggestions.
   *
   * @return JsonResponse
   *   Given a partial return an array of matching phrases.
   */
  public function suggestions() {
    $params = [];
    $params['q'] = $this->getQueryParam('q');
    // TODO: Move the default types to configuration?
    $defaultTypes = ['artwork', 'creator', 'exhibition'];
    // Limit results by a given content type, or by default types.
    $type = $this->getQueryParam('type');
    $params['types'] = ($type && in_array($type, $defaultTypes)) ? [$type] : $defaultTypes;

    $queryDsl = new SearchSuggestionsQuery($params);
    $size = $this->requestStack->getCurrentRequest()->query->get('size');
    $size = is_numeric($size) ? $size : $this->size;
    $queryDsl->setSize($size);

    $dsl = $queryDsl::build();
    return $this->execute($dsl);
  }

  /**
   * Filter Artwork content by terms.
   *
   * @return JsonResponse
   *   Given a partial return an array of matching phrases.
   */
  public function filterArtworks() {
    $params = $this->requestStack->getCurrentRequest()->query->all();

    $queryDsl = new FilterArtworksQuery($params);
    $offset = $this->requestStack->getCurrentRequest()->query->get('offset');
    $start = is_numeric($offset) ? $offset : $this->start;
    $queryDsl->setStart($start);
    $size = $this->requestStack->getCurrentRequest()->query->get('size');
    $size = is_numeric($size) ? $size : $this->size;
    $queryDsl->setSize($size);
    $dsl = $queryDsl::build();

    return $this->execute($dsl);
  }

  /**
   * Filter Artists content by terms.
   *
   * @return JsonResponse
   *   Given a partial return an array of matching phrases.
   */
  public function filterArtists() {
    $params = $this->requestStack->getCurrentRequest()->query->all();
    $queryDsl = new FilterArtistsQuery($params);
    $dsl = $queryDsl::build();

    return $this->execute($dsl);
  }

  /**
   * Get alphabetic listing of artists by letter, with aggregated number of artworks.
   *
   * @return JsonResponse
   */
  public function glossary() {
    $params = [];
    $params['letter'] = $this->getQueryParam('letter');
    $queryDsl = new ArtistGlossaryQuery($params);
    $dsl = $queryDsl::build();

    return $this->execute($dsl);
  }

  /**
   * Get aggregations with information about the indexed content.
   *
   * @return JsonResponse
   */
  public function data() {
    $queryDsl = new CollectionDataQuery();
    $dsl = $queryDsl::build();
    unset($dsl['body']['query']);

    return $this->execute($dsl);
  }

  /**
   * Get a specific content item.
   *
   * @param string $document_id
   *   The Elasticsearch document ID to lookup.
   *
   * @return JsonResponse
   */
  public function load($document_id = '') {
    $queryDsl = new QueryDslBase();
    $queryDsl->setQuery([
      'ids' => [
        'values' => [$document_id]
      ]
    ]);
    $dsl = $queryDsl::build();
    return $this->execute($dsl);
  }

  /**
   * Execute a Query DSL Search against an Elasticsearch Index.
   *
   * @param array $query_dsl
   * @return JsonResponse
   * @throws \Exception
   */
  private function execute($query_dsl) {
    try {
      $elasticClient = EsClient::getInstance();
      $results = $elasticClient->search($query_dsl);
      return new JsonResponse($results);
    }
    catch (RequestException $exception) {
      $this->loggerFactory->get('viola_elasticsearch')->error($exception->getMessage());
      return new JsonResponse([
        'code' => $exception->getCode(),
        'message' => $exception->getMessage(),
      ]);
    } catch (\Exception $exception) {
      $this->loggerFactory->get('viola_elasticsearch')->error($exception->getMessage());
    }
  }

  /**
   * Get a properly escaped search phrase from the query string.
   *
   * @param string $query_param
   * @return mixed
   */
  private function getQueryParam($query_param = 'q') {
    $searchPhrase = $this->requestStack->getCurrentRequest()->query->get($query_param);
    $searchPhrase = Tags::explode($searchPhrase);
    return mb_strtolower(array_pop($searchPhrase));
  }

}
