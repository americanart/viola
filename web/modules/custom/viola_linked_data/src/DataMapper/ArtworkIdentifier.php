<?php

namespace Drupal\viola_linked_data\DataMapper;

use Drupal\viola_core\DataMapper\DataMapper;

/**
 * ArtistLinkedData DataMapper
 *
 * Map data from custom "artist linked data" data to Artwork node fields.
 *
 */
class ArtworkIdentifier extends DataMapper {

  /**
   * @param array $field_data
   */
  public function __construct($field_data = []){
    $this->setData($field_data);
  }

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {
    $this->data = [];

    // Object ID.
    if (self::isValidDataKey('saam_object_id', $field_data)) {
      $this->data['field_object_id'] = $field_data['saam_object_id'];
    }
    // Wikidata ID.
    if (self::isValidDataKey('wikidata_id', $field_data)) {
      $this->data['field_wikidata_id'] = $field_data['wikidata_id'];
    }
  }

}
