<?php

namespace Drupal\viola_wikimedia\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Annotation\AdvancedQueueJobType;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\Component\Serialization\Json;
use Drupal\node\NodeInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\Plugin\AdvancedQueue\JobType\ImportJobBase;
use Exception;

/**
 * @AdvancedQueueJobType(
 *   id = "wikimedia_import_job",
 *   label = @Translation("Wikimedia Import Job"),
 *   max_retries = 3,
 *   retry_delay = 79200,
 * )
 */
class WikimediaImportJob extends ImportJobBase {

  /**
   * @var array
   *   Allowed Node bundles to act upon in a job.
   */
  protected static $allowedNodeBundles = [
    'artwork',
    'creator',
  ];

  /**
   * {@inheritdoc}
   * @throws \Exception
   */
  public function process(Job $job) {
    try {
      $payload = $job->getPayload();
      $status = 0;

      if ($payload['entity_type'] != 'node') {
        throw new Exception(sprintf('Invalid entity type: %s.', $payload['entity_type']));
      }
      if (!in_array($payload['bundle'], self::$allowedNodeBundles)) {
        throw new Exception(sprintf('Invalid bundle: %s.', $payload['bundle']));
      }
      if (empty($payload['data']['source'])) {
        throw new Exception('Missing required import source. (wikipedia or wikidata)');
      }

      /** @var \Drupal\viola_content\Plugin\TypedRepositories\CreatorRepository $repository */
      $repository = \Drupal::service(RepositoryManager::class)->repository($payload['entity_type'], $payload['bundle']);

      // Validate the id field.
      if (!isset($payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]) || empty($repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME)) {
        throw new Exception('Missing required fields.');
      }
      else {
        $wrappedEntity = $repository->firstWhere([
          $repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME => $payload['data'][$repository::ALTERNATIVE_IDENTIFIER_FIELD_NAME]
        ]);
        if ($wrappedEntity) {
          $node = $wrappedEntity->getEntity();
        }
      }
      // Update the Node with Wikidata data.
      if ($node && $node instanceof NodeInterface) {
        $source = $payload['data']['source'];
        unset($payload['data']['source']);
        // Skip import if data has not changed from the stored version.
        $updates = Json::decode($payload['data']['field_data']);
        if (!isset($payload['data']['force']) && !empty($updates[$source]['checksum'])) {
          $data = Json::decode($node->get('field_data')->value);
          if (!empty($data[$source]['checksum']) && $data[$source]['checksum'] === $updates[$source]['checksum']) {
            return JobResult::success($repository::getStatusMessage(self::SKIPPED));
          }
        }
        unset($payload['data']['force']);
        if ($payload['action'] === 'update') {
          $status = $repository->updateNode($payload['data'], $node);
        }
      }

      if ($status) {
        return JobResult::success($repository::getStatusMessage($status));
      }
      // Default.
      return JobResult::failure($repository::getStatusMessage($status));
    }
    catch (Exception $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}
