<?php

namespace Drupal\viola_content\WrappedEntities\Traits;

use Drupal\Core\Entity\EntityInterface;
use Drupal\media\MediaInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\typed_entity\RepositoryManager;

trait HasEntityReferencesTrait {

  /**
   * Get referenced entities from a Node Entity Reference field.
   *
   * @param EntityInterface $entity
   * @param string $field_name.
   *   The field machine name
   *
   * @return array
   */
  public function getReferencedEntities($entity, $field_name) {
    if ($entity instanceof EntityInterface && $entity->hasField($field_name)) {
      return $entity->$field_name->referencedEntities();
    }
    return [];
  }

  /**
   * Get Taxonomy Terms labels from an Entity Reference field.
   *
   * @param EntityInterface $entity
   * @param string $field_name.
   *   The field machine name
   *
   * @return array
   */
  public function getReferencedTaxonomyTermLabels($entity, $field_name) {
    $entities = $this->getReferencedEntities($entity, $field_name);
    $labels = [];
    foreach ($entities as $entity) {
      if ($entity instanceof TermInterface) {
        $labels[] = $entity->label();
      }
    }

    return $labels ?: [];
  }

  /**
   * Get the first Taxonomy Term label from an Entity Reference field.
   *
   * @param EntityInterface $entity
   * @param string $field_name.
   *   The field machine name
   *
   * @return string
   */
  public function getReferencedTaxonomyTermLabel($entity, $field_name) {
    $labels = $this->getReferencedTaxonomyTermLabels($entity, $field_name);
    return reset($labels) ?: '';
  }

  /**
   * Get referenced entities as wrapped entities.
   *
   * @param EntityInterface $entity
   * @param string $field_name.
   *   The field machine name
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]
   *   The wrapped entities.
   */
  public function wrapReferencedEntites($entity, $field_name) {
    $entities = $this->getReferencedEntities($entity, $field_name);
    $wrappedEntities = \Drupal::service(RepositoryManager::class)->wrapMultiple($entities);
    return $wrappedEntities;
  }


}
