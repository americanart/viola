(function($) {
  'use strict';

  Drupal.behaviors.jsonViewInit = {
    attach: function(context, settings) {
      var $container = $(".field.field--name-field-data .field__item > pre > code", context);
      var json = $container.text();
      if (json) {
        $container.empty();
        $container.JSONView(json, { collapsed: true });
      }
    }
  };
})(jQuery);
