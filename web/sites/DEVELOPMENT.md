# Local Settings and Development

## Environment Variables

Copy the variables in .env.example to .env and populate with real values. These variables are used in the main Drupal
settings file. (`/web/sites/default/settings.php`)

### Override settings.

Create a file in the **project root** named `/settings-overrides.php` to override individual settings keys in Drupal's
settings.php

Example settings-overrides.php

```php
<?php

$settings['hash_salt'] = 'CHANGE_THIS';

$settings['trusted_host_patterns'] = [
  '^.+\.example\.com$',
  '^viola\.lndo\.site',
];
```

### Drush configuration

Add Drush aliases to PROJECT_ROT/drush/sites/self.site.yml
See the [Drush example](https://github.com/drush-ops/drush/blob/master/examples/example.site.yml) for reference.

### Jedi Console

Additional development utilities are included in the "[Jedi Console](https://gitlab.com/ericpugh/jedi-console)" module.

On a fresh install enable the "jedi console" module
```shell script
drush pm:enable jedi_console
```
Now other utilities are available. Such as the `drupal jedi:dev` command to install
other development tools, and `drupal jedi:up` to update the local environment.
Read more about [Jedi Console](https://gitlab.com/ericpugh/jedi-console/-/blob/master/README.md).

## Deployment

An example deployment script is included in `./deploy.php` using [Deployer](https://deployer.org/). The script loads
remote hosts from a configuration file at `/etc/deployer/hosts.yml`. Read more from the deployer docs about [configuring
hosts](https://deployer.org/docs/hosts.html). Deployer deploys a new release using a "capistrano style" atomic deployment,
and includes common tasks for deploying a Drupal site, like configuration management, environment variables, etc.
