<?php

namespace Drupal\viola_content\WrappedEntities\Paragraph;

use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the `embed_component` paragraph.
 */
class EmbedComponent extends TransformableWrappedEntity {

  /**
   * @return string
   */
  public function getAspectRatio() {
    return $this->getEntity()->get('field_aspect_ratio')->value ?? '';
  }

  /**
   * @return string
   */
  public function getEmbedCode() {
    return $this->getEntity()->get('field_embed_code')->value ?? '';
  }

}

