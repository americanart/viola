<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Paragraph;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for `embed_component` paragraphs.
 *
 * @TypedRepository(
 *    entity_type_id = "paragraph",
 *    bundle = "embed_component",
 *    wrappers = @ClassWithVariants(
 *      fallback = "Drupal\viola_content\WrappedEntities\Paragraph\EmbedComponent",
 *    ),
 *   description = @Translation("Repository that holds logic for embed components")
 * )
 */
final class EmbedComponentRepository extends TypedRepositoryBase {}
