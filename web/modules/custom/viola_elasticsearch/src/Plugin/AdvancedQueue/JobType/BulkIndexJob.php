<?php

namespace Drupal\viola_elasticsearch\Plugin\AdvancedQueue\JobType;

use Drupal\advancedqueue\Annotation\AdvancedQueueJobType;
use Drupal\advancedqueue\Job;
use Drupal\advancedqueue\JobResult;
use Drupal\viola_elasticsearch\Elastic\EsClient;
use Drupal\viola_core\Plugin\AdvancedQueue\JobType\ImportJobBase;

/**
 * @AdvancedQueueJobType(
 *   id = "bulk_index_job",
 *   label = @Translation("Bulk Index Job"),
 *   max_retries = 0,
 * )
 *
 * BulkIndexJob
 *
 * A job to perform an bulk index for Elasticsearch.
 *
 */
class BulkIndexJob extends ImportJobBase {

  /**
   * {@inheritdoc}
   */
  public function process(Job $job) {
    try {
      $errorMessage = '';
      $payload = $job->getPayload();
      // Get the action, "index" or "delete"
      $action = array_key_first($payload['body'][0]);
      $client = EsClient::getInstance();

      // Bulk index the payload data.
      $response = $client->bulk($payload);
      if ($response) {
        if ($response['errors']) {
          foreach ($response['items'] as $item) {
            if (isset($item[$action]['error'])) {
              $errorMessage = sprintf('%s: %s', $item[$action]['error']['type'], $item[$action]['error']['reason']);
              continue;
            }
          }
        }
        else {
          return JobResult::success();
        }
      }

      // Default.
      return JobResult::failure($errorMessage);
    }
    catch (\Exception $e) {
      return JobResult::failure($e->getMessage());
    }
  }

}

