<?php

namespace Drupal\viola_core\Controller\Data;

use Drupal\Core\Cache\CacheableJsonResponse;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Output a JSON endpoint with information about the collection.
 */
class CollectionController extends DataControllerBase {

  /**
   * Get JSON response for a Tour.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   * @return CacheableJsonResponse
   */
  public function get(Request $request) {
    $response = new CacheableJsonResponse();
    $cache_metadata = $this->getCacheableMetadata();
    try {
      $data = [];
      $data['total_num_artworks'] = $this->getTotalNumArtworks();
      $data['total_num_creators'] = $this->getTotalNumCreators();
      $data['total_num_artists'] = $this->getTotalNumCreators('individual');
      $data['total_num_institutions'] = $this->getTotalNumCreators('institution');
      $data['total_num_female_artists'] = $this->getNumArtistsByGender('female');
      $data['total_num_artists_by_state'] = $this->getNumArtistsByState();
      $data['total_num_artists_by_nationality'] = $this->getNumArtistsByNationality();
      $data['artwork_classifications'] = $this->getArtworkClassifications();
      $data['artist_fields'] = $this->getArtistFields();
      $response->setContent(json_encode($data, JSON_NUMERIC_CHECK));
      $response->setStatusCode(200);
      // Add cache dependency on the current controller name.
      $cache_dependencies = ['controller:' . get_class()];
      $cache_metadata->setCacheTags($cache_dependencies);
      $response->addCacheableDependency($cache_metadata);
    }
    catch (RequestException $exception) {
      $this->loggerFactory->get('viola_core')->error($exception);
      $response->setContent(\GuzzleHttp\json_encode([
        'code' => 500,
        'message' => $exception->getMessage(),
      ]));
      $response->setStatusCode(500);
    }
    return $response;
  }

  /**
   * Get the total number of Artworks.
   *
   * @return int
   */
  private function getTotalNumArtworks(): int {
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.type', 'artwork', '=');
    $query->condition('n.status', 1);
    $query->distinct();
    $count = $query->countQuery()->execute()->fetchCol();
    return reset($count);
  }

  /**
   * Get the total number of Creators.
   *
   * @param string $type
   *   The creator type.
   *
   * @return int
   */
  private function getTotalNumCreators($type = ''): int {
    // Get the Term ID of the given type.
    if ($type) {
      $type = ucfirst(mb_strtolower($type));
      $termQuery = \Drupal::database()->select('taxonomy_term_field_data', 't');
      $termQuery->fields('t', ['tid']);
      $termQuery->condition('t.vid', 'creator_types', '=');
      $termQuery->condition('t.name', $type, '=');
      $result = $termQuery->execute()->fetchCol();
      $tid = reset($result);
    }
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.type', 'creator', '=');
    $query->condition('n.status', 1);
    if ($type && $tid) {
      $query->join('node__field_creator_type', 'ct', 'ct.entity_id = n.nid');
      $query->condition('ct.field_creator_type_target_id', $tid, '=');
    }
    $query->distinct();
    $count = $query->countQuery()->execute()->fetchCol();
    return reset($count);
  }

  private function getArtworkClassifications() {
    $classifications = [];
    $terms = \Drupal::service('entity_type.manager')
      ->getStorage("taxonomy_term")
      ->loadTree('classifications', $parent = 0, $max_depth = 2, $load_entities = FALSE);
    foreach ($terms as $term) {
      if (is_object($term)) {
        $classifications[] = [
          'name' => $term->name,
          'tid' => $term->tid,
          'parent' => reset($term->parents),
          'total' => $this->getCountNodeWithTaxonomyTerm($term->tid),
        ];
      }
    }
    return $classifications;
  }

  /**
   * Get the number of Artists by Gender.
   *
   * @return int
   */
  private function getNumArtistsByGender($gender = 'female'): int {
    // TODO: (Refactor Break) Fix this to retrieve value, field was changes to a Taxonomy Term.
    return 0;
//    $query = \Drupal::database()->select('node_field_data', 'n');
//    $query->fields('n', ['nid']);
//    $query->condition('n.type', 'creator', '=');
//    $query->condition('n.status', 1);
//    $query->join('node__field_gender', 'fg', 'fg.entity_id = n.nid');
//    $query->condition('fg.field_gender', $gender, '=');
//    $query->distinct();
//    $count = $query->countQuery()->execute()->fetchCol();
//    return reset($count);
  }

  /**
   * Get the number of Artists in each U.S. State.
   *
   * @return array
   */
  private function getNumArtistsByState(): array {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 't');
    $query->fields('t', ['tid', 'name']);
    $query->condition('t.vid', 'states', '=');
    $query->distinct();
    $states = $query->execute()->fetchAllKeyed();
    $results = [];
    foreach($states as $tid => $name) {
      $query = \Drupal::database()->select('node_field_data', 'n');
      $query->fields('n', ['nid']);
      $query->condition('n.type', 'creator', '=');
      $query->condition('n.status', 1);
      $query->join('node__field_birth_place', 'fb', 'fb.entity_id = n.nid');
      $query->join('node__field_place_of_death', 'fd', 'fd.entity_id = n.nid');
      $query->join('node__field_associated_places', 'fa', 'fa.entity_id = n.nid');
      $conditions = $query->orConditionGroup()
        ->condition('fb.field_birth_place_target_id', $tid, '=')
        ->condition('fd.field_place_of_death_target_id', $tid, '=')
        ->condition('fa.field_associated_places_target_id', $tid, '=');
      $query->condition($conditions);
      $query->distinct();
      $count = $query->countQuery()->execute()->fetchCol();
      $results[$name] = reset($count);
    }
    asort($results);
    return $results;
  }

  /**
   * Get the number of Artists in each U.S. State.
   *
   * @return array
   */
  private function getNumArtistsByNationality(): array{
    // TODO: Verify this is working correctly, after the field was changed to a single field_data.
    $db = \Drupal::database();
    $query = $db->query("SELECT DISTINCT JSON_UNQUOTE(JSON_EXTRACT(field_data_value, CONCAT('$.', 'nationality'))) AS nationalities
      FROM node__field_data
      WHERE JSON_EXTRACT(field_data_value, '$.nationality') IS NOT NULL
      AND bundle = 'creator'
      ORDER BY `nationalities`;");
    $nationalities = $query->fetchCol();
    $results = [];
    foreach($nationalities as $nationality) {
      $sql = "SELECT COUNT(DISTINCT  entity_id) AS num
        FROM `node__field_data`
        WHERE JSON_EXTRACT(`field_data_value`, '$.nationality') = :nationality
        AND bundle = 'creator'";
      $query = $db->query($sql, [':nationality' => (string) $nationality]);
      $count = $query->fetchCol();
      $results[$nationality] = reset($count);
    }
    arsort($results);
    return $results;
  }

  private function getArtistFields() {
    $data = [];
    $data['Description'] = $this->getCountPopulatedTextField('field_collection_description', 'creator');
    $data['Exact Birth Date'] = $this->getCountPopulatedTextField('field_date_of_birth', 'creator');
    $data['Exact Death Date'] = $this->getCountPopulatedTextField('field_date_of_death', 'creator');
    $data['Wikidata ID'] = $this->getCountPopulatedTextField('field_wikidata_id', 'creator');
    $data['ULAN ID'] = $this->getCountPopulatedTextField('field_ulan_id', 'creator');
    $data['VIAF ID'] = $this->getCountPopulatedTextField('field_viaf_id', 'creator');
    $data['LOC ID'] = $this->getCountPopulatedTextField('field_loc_id', 'creator');
    $data['Wikipedia Extract'] = $this->getCountPopulatedEntityReferenceField('field_quoted_text', 'creator');
    $data['Birth State'] = $this->getCountPopulatedEntityReferenceField('field_birth_place', 'creator');
    $data['Place of Death'] = $this->getCountPopulatedEntityReferenceField('field_place_of_death', 'creator');
    $data['Associated States'] = $this->getCountPopulatedEntityReferenceField('field_associated_places', 'creator');
    return $data;
  }

  private function getCountPopulatedTextField($field, $content_type) {
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.type', $content_type, '=');
    $query->condition('n.status', 1);
    $query->join('node__' . $field, 'fsd', 'fsd.entity_id = n.nid');
    $query->condition('fsd.' . $field . '_value', '', '<>');
    $query->distinct();
    $count = $query->countQuery()->execute()->fetchCol();
    return reset($count);
  }

  private function getCountPopulatedEntityReferenceField($field, $content_type) {
    $query = \Drupal::database()->select('node_field_data', 'n');
    $query->fields('n', ['nid']);
    $query->condition('n.type', $content_type, '=');
    $query->condition('n.status', 1);
    $query->join('node__' . $field, 'f', 'f.entity_id = n.nid');
    $query->condition('f.deleted', '0', '=');
    $query->distinct();
    $count = $query->countQuery()->execute()->fetchCol();
    return reset($count);
  }

  private function getCountNodeWithTaxonomyTerm($tid) {
    $query = \Drupal::database()->select('taxonomy_index', 'ti');
    $query->fields('ti', ['nid']);
    $query->condition('ti.tid', $tid, '=');
    $query->condition('ti.status', 1, '=');
    $query->distinct();
    $count = $query->countQuery()->execute()->fetchCol();
    return reset($count);
  }

}
