<?php

namespace Drupal\viola_core;

use Drupal\media\Entity\MediaType;
use Drupal\node\Entity\NodeType;
use Exception;

/**
 * Retrieve information about Drupal internals.
 */
class SystemInfo {

  /**
   * The singleton instance.
   *
   * @var self
   */
  private static $instance;

  /**
   * Disable instantiation.
   */
  private function __construct() {}

  /**
   * Create or retrieve the instance.
   *
   * @return self
   *   Return the current instance of SystemInfo.
   */
  public static function getInstance() {
    if (!static::$instance) {
      static::$instance = new self();
    }
    return static::$instance;
  }

  /**
   * Get a list of all Drupal content types.
   *
   * @return array
   *   Array of Drupal content type labels keyed by machine name.
   */
  public static function getAllContentTypes() {
    $types = NodeType::loadMultiple();
    $list = [];
    foreach ($types as $type) {
      $list[$type->id()] = $type->label();
    }
    return $list;
  }

  /**
   * Get a list of all Drupal media entity types.
   *
   * @return array
   *   Array of Drupal media type labels keyed by machine name.
   */
  public static function getAllMediaTypes() {
    $types = MediaType::loadMultiple();
    $list = [];
    foreach ($types as $type) {
      $list[$type->id()] = $type->label();
    }
    return $list;
  }

  /**
   * Get a list field names of a given type for a specific entity.
   *
   * @param $type
   *  The field type, i.e. 'string', 'boolean', 'integer', 'entity_reference', 'json_native', etc.
   * @param string $entity_type
   *   The entity type, i.e. 'node' or 'media'
   * @param string $bundle
   *   The entity bundle, i.e. 'artwork'
   *
   * @return array
   *   Array of fields of a given type.
   */
  public static function getEntityFieldsByType($type, $entity_type, $bundle) {
    $fields = [];
    /** @var \Drupal\Core\Field\FieldDefinitionInterface[] $definitions */
    $definitions = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type, $bundle);
    foreach ($definitions as $name => $definition) {
      if (!empty($definition->getTargetBundle())) {
        if ($definition->getType() == $type) {
          $fields[] = $name;
        }
      }
    }
    return $fields;
  }

  /**
   * Disable the cloning of this class.
   *
   * @throws \Exception
   */
  final public function __clone() {
    throw new Exception('Feature disabled.');
  }

  /**
   * Disable the wakeup of this class.
   *
   * @throws \Exception
   */
  final public function __wakeup() {
    throw new Exception('Feature disabled.');
  }

}
