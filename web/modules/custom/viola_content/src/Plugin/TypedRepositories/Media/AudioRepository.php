<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Media;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
* The repository for audio.
*
* @TypedRepository(
*    entity_type_id = "media",
*    bundle = "audio",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Media\Audio",
*    ),
*   description = @Translation("Repository that holds logic for auido")
* )
*/
final class AudioRepository extends TypedRepositoryBase {}
