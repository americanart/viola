<?php

namespace Drupal\viola_core\Typography;

use PHP_Typography\PHP_Typography;
use PHP_Typography\Settings;

/**
 * Web Typography Utilities Service class.
 */
class TypographyService {

  /**
   * @var \PHP_Typography\Settings
   */
  protected $settings;

  /**
   * @var \PHP_Typography\PHP_Typography
   */
  protected $typography;

  /**
   * Constructor.
   */
  public function __construct() {
    $this->settings = new Settings();
    $this->typography = new PHP_Typography();
    $this->settings->set_hyphenation(FALSE);
    $this->settings->set_hyphenation_language( 'en-US' );
    $this->settings->set_initial_quote_tags(['blockquote']);
  }

  public function process($text = '') {
    return $this->typography->process($text, $this->settings);
  }

}
