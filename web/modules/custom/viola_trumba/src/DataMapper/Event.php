<?php

namespace Drupal\viola_trumba\DataMapper;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\typed_entity\RepositoryManager;
use Drupal\typed_entity\WrappedEntities\WrappedEntityInterface;
use Drupal\viola_core\DataMapper\DataMapper;
use Drupal\viola_core\DataMapper\Traits\HasDateRangeTrait;
use Drupal\viola_core\Utils\Strings;
use Drupal\viola_core\Utils\Markup;
use Drupal\viola_core\Utils\DateTimes;

/**
 * Trumba Event DataMapper.
 */
class Event extends DataMapper {

  use HasDateRangeTrait;

  /**
   * Event constructor.
   *
   * @param array $field_data
   */
  public function __construct(array $field_data = []) {
    $this->setData($field_data);
  }

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository $termRepository */
    $termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
    $this->data = [];
    if (isset($field_data['force'])) {
      $this->data['force'] = TRUE;
    }
    unset($field_data['force']);
    // Checksum.
    $this->data['field_checksum'] = DataMapper::generateChecksum($field_data);
    // Title.
    if (self::isValidDataKey('title', $field_data)) {
      if (strlen($field_data['title']) > 255) {
        // Ensure the title doesn't exceed field length constraint.
        $field_data['title'] = Strings::truncateText($field_data['title'], 255);
      }
      $this->data['title'] = $field_data['title'] ? Strings::decodeSpecialChars($field_data['title']) : 'Untitled';
      if($field_data['title'] != strip_tags($field_data['title'])) {
        // Set the formatted title when a title contains markup.
        $this->data['field_formatted_title'] = $field_data['title'];
      }
      unset($field_data['title']);
    }
    // Image URL.
    // Add the image url as a non-field-data field to be handled by the import job.
    if (is_array($field_data['detailImage']) && isset($field_data['detailImage']['url'])) {
      $this->data['image_url'] = $field_data['detailImage']['url'];
    }
    // Description.
    if (self::isValidDataKey('description', $field_data) && !empty($field_data['description'])) {
      $this->data['field_description'] = [
        'value' => trim(Markup::clean($field_data['description'])),
        'format' => 'full_html',
      ];
      $plainTextDescription = trim(Markup::clean($this->data['field_description']['value'], []));
      $this->data['field_summary'] = Strings::truncateText($plainTextDescription, 255);
      unset($field_data['description']);
    }
    // Trumba Event ID.
    if (self::isValidDataKey('eventID', $field_data)) {
      $this->data['field_trumba_event_id'] = $field_data['eventID'];
      $this->data['field_trumba_event'] = TRUE;
    }
    // Event Schedule (Datetime range).
    $timezone ='America/New_York';
    if (self::isValidDataKey('startDateTime', $field_data)) {
      $this->data['field_event_schedule']['value'] = DateTimes::getFormattedDateTime($field_data['startDateTime'], $timezone);
      unset($field_data['startDateTime']);
    }
    if (self::isValidDataKey('endDateTime', $field_data) && isset($this->data['field_event_schedule'])) {
      $this->data['field_event_schedule']['end_value'] = DateTimes::getFormattedDateTime($field_data['endDateTime'], $timezone);
      unset($field_data['endDateTime']);
    }
    // All day event.
    if (self::isValidDataKey('allDay', $field_data)) {
      $this->data['field_all_day'] = filter_var($field_data['allDay'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['allDay']);
    }
    // Cancelled.
    if (self::isValidDataKey('canceled', $field_data)) {
      $this->data['field_canceled'] = filter_var($field_data['canceled'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['canceled']);
    }
    // Open Sign-up.
    if (self::isValidDataKey('openSignUp', $field_data)) {
      $this->data['field_open_sign_up'] = filter_var($field_data['openSignUp'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['openSignUp']);
    }
    // Reservations Full.
    if (self::isValidDataKey('reservationFull', $field_data)) {
      $this->data['field_reservation_full'] = filter_var($field_data['reservationFull'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['reservationFull']);
    }
    // Past deadline.
    if (self::isValidDataKey('pastDeadline', $field_data)) {
      $this->data['field_past_deadline'] = filter_var($field_data['pastDeadline'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['pastDeadline']);
    }
    // Requires payment.
    if (self::isValidDataKey('requiresPayment', $field_data)) {
      $this->data['field_requires_payment'] = filter_var($field_data['requiresPayment'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['requiresPayment']);
    }
    // Refunds are allowed.
    if (self::isValidDataKey('refundsAllowed', $field_data)) {
      $this->data['field_refunds_allowed'] = filter_var($field_data['refundsAllowed'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['refundsAllowed']);
    }
    // Waiting list avaialble.
    if (self::isValidDataKey('waitingListAvailable', $field_data)) {
      $this->data['field_waiting_list_available'] = filter_var($field_data['waitingListAvailable'], FILTER_VALIDATE_BOOLEAN);
      unset($field_data['waitingListAvailable']);
    }
    // Get event series Taxonomy Terms IDs.
    $eventSeries = [];
    if (isset($field_data['eventSeries']) && is_array($field_data['eventSeries'])) {
      foreach ($field_data['eventSeries'] as $series) {
        // Mapping for Event Series names that don't match the Taxonomy Term name.
        $map = [
          'America InSight' => 'Verbal Description Tours',
          'Art Signs' => 'ASL Tours: Art Signs',
          'Conservation' => 'Conservation Programs',
          'Family Programs' => 'Family Programs and Festivals',
          'Handi-Hour' => 'Crafting Happy Hour: Handi-Hour',
          'See Me at SAAM' => 'Dementia Programs',
          'Take Five!' => 'Jazz Series: Take Five!',
          'The Clarice Smith Distinguished Lectures in American Art' => 'Clarice Smith Distinguished Lectures in American Art'
        ];
        $name = array_key_exists($series, $map) ? $map[$series] : $series;
        $tid = $termRepository->getTermIdByName('event_series', trim(htmlspecialchars_decode($name)));
        if ((int) $tid > 0) {
          $eventSeries[] = $tid;
        }
      }
      unset($field_data['eventSeries']);
    }
    // Set an Event Series Taxonomy Term based on the event calendar.
    if (self::isValidDataKey('categoryCalendar', $field_data)) {
      // Set "Tours" event series based on "category calendar".
      $calendars = [
        'American Art Museum SI|Ongoing Tours & Activities',
        'Renwick Gallery|Ongoing Tours',
      ];
      if (in_array((trim($field_data['categoryCalendar'])), $calendars)) {
        $tid = $termRepository->getTermIdByName('event_series', 'Tours');
        if ((int) $tid > 0) {
          $eventSeries[] = $tid;
        }
      }
      unset($field_data['categoryCalendar']);
    }
    $this->data['field_event_series'] = array_unique($eventSeries);

    // Handle Trumba "Custom Fields".
    if (isset($field_data['customFields'])) {
      $customFields = [];
      // Create an array keyed by field label.
      if (is_array($field_data['customFields'])) {
        foreach ($field_data['customFields'] as $field) {
          if (isset($field['fieldID'])) {
            $customFields[$field['label']] = $field;
          }
        }
      }
      if (isset($customFields['Venue']) && isset($customFields['Venue']['value'])) {
        if ($customFields['Venue']['value'] == 'American Art Museum') {
          $venue = 'Smithsonian American Art Museum';
        }
        else {
          $venue = $customFields['Venue']['value'];
        }
        $tid = $termRepository->getTermIdByName('venues', $venue);
        $this->data['field_venue'][] = $tid;
      }
      // Parse the "Related Exhibition" custom field markup and attempt to create an Entity Reference to an existing Exhibition Node.
      if (isset($customFields['Related Exhibition']) && isset($customFields['Related Exhibition']['value'])) {
        $string = Markup::clean(trim($customFields['Related Exhibition']['value']), ['<a>']);
        // Get the path from a possible link to the exhibition, which is the most reliable identifier.
        preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $string, $links);
        if (is_array($links) && isset($links['href']) && !empty($links['href'])) {
          $link = reset($links['href']);
          $path = parse_url($link, PHP_URL_PATH);
          if ($path) {
            $internalPath = \Drupal::service('path_alias.manager')->getPathByAlias($path);
            $params = Url::fromUri("internal:" . $internalPath)->getRouteParameters();
            if (is_array($params) && isset($params['node'])) {
              $this->data['field_exhibitions'] = $params['node'];
            }
          }
        }
        else {
          // Try to use the string to find an Exhibition by title.
          $string = filter_var($string, FILTER_SANITIZE_FULL_SPECIAL_CHARS);
          /** @var \Drupal\viola_content\Plugin\TypedRepositories\EventRepository $exhibitionRepository */
          $exhibitionRepository = \Drupal::service(RepositoryManager::class)->repository('node', 'exhibition');
          /** @var \Drupal\viola_content\WrappedEntities\Exhibition $exhibition */
          $exhibition = $exhibitionRepository->firstWhere(['status' => 1, 'title' => $string]);
          if ($exhibition) {
            $this->data['field_exhibitions']($exhibition->getDrupalId());
          }
        }
      }
    }
    // Data JSON field. (Remaining fields are added as JSON data keyed by source).
    $data = ['trumba' => $field_data];
    $this->data['field_data'] = json_encode($data, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_PRETTY_PRINT);
  }

}
