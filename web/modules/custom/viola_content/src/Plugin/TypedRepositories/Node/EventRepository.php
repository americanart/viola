<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Node;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\IsNodeManagerTrait;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\OutputsStatusMessageTrait;

/**
* The repository for events.
*
* @TypedRepository(
*    entity_type_id = "node",
*    bundle = "event",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Node\Event",
*    ),
*   description = @Translation("Repository that holds logic for events")
* )
*/
final class EventRepository extends TypedRepositoryBase {

  use IsNodeManagerTrait;
  use OutputsStatusMessageTrait;

  const ALTERNATIVE_IDENTIFIER_FIELD_NAME = 'field_trumba_event_id';

}
