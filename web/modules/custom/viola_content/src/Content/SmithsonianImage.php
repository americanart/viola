<?php

namespace Drupal\viola_content\Content;

/**
 * {@inheritDoc}
 */
class SmithsonianImage {

  /**
   * A special case where the data doesn't come from Drupal Entity, but is rather populated by properties that
   * point to an image served by the Smithsonian's "Image Delivery Service".
   *
   * @param array $data
   * @return array
   */
  public static function fromProperties(array $data) {
    if (!isset($data['fileName'])) {
      return [];
    }
    $url = self::getUrl($data['fileName']);
    return [
      'title' => $data['objectNumber'] ?: '',
      'alt' => isset($data['caption']) ? $data['caption'] : '', // TODO: what should the alt text be?
      'caption' => isset($data['caption']) ? $data['caption'] : '',
      'url' => $url,
      'sizes' => [
        // TODO: Dynamically get the image style max widths from \Drupal\viola_core\SystemInfo.
        'default' => sprintf('%s&max=%s', $url, '960'),
        'xsmall' => sprintf('%s&max=%s', $url, '320'),
        'small' => sprintf('%s&max=%s', $url, '640'),
        'medium' => sprintf('%s&max=%s', $url, '960'),
        'large' => sprintf('%s&max=%s', $url, '1200'),
        'xlarge' => sprintf('%s&max=%s', $url, '2600'),
      ],
      'primary' => $data['primaryDisplay'] ?: '',
      'type' => 'smithsonian_image',
      'orientation' => '',
      'id' => $data['fileID'] ?: '',
      'guid' => $data['guid'] ?: '',
      'zoom' => self::getUrl($data['fileName'], TRUE)
    ];
  }

  /**
   * Returns the IDS url using a DAMS ID.
   *
   * @param string $id
   *   The DAMS Unique Asset Number.
   * @param bool $container
   *   Whether to use the "zoomable" tiled image or static asset.
   *
   * @return string $url
   */
  public static function getUrl($id, $container = FALSE) {
    $url = $container ? '//ids.si.edu/ids/dynamic?container.fullpage&id=' : '//ids.si.edu/ids/deliveryService?id=';
    return $url . $id;
  }

}
