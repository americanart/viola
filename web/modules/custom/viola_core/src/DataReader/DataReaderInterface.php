<?php

namespace Drupal\viola_core\DataReader;

/**
 * Class ReaderInterface.
 */
interface DataReaderInterface {

  /**
   * Reads contents of data file.
   *
   * @return mixed
   */
  public function read();

  /**
   * Return results from a read operation.
   *
   * @return array
   */
  public function getResults();

}
