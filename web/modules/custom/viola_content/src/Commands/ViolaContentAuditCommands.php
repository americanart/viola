<?php

namespace Drupal\viola_content\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\typed_entity\RepositoryManager;
use Drush\Commands\DrushCommands;

class ViolaContentAuditCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The allowed content types.
   *
   * @var array
   */
  protected $types = [
    'artwork',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Audit content.
   *
   * @param string $type
   *   The content type.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option limit
   *   Limit number of items to import.
   * @usage viola-content:audit-keywords
   *   Usage description
   *
   * @command viola-content:audit-keywords
   */
  public function auditKeywords($type, $options = ['limit' => -1]) {
    $this->logger()->notice(dt("Running viola-core:audit-keywords ..."));
    $bundle = strtolower($type);
    if (!in_array($bundle, $this->types)) {
      throw new \InvalidArgumentException(dt(
        'Invalid type, @type. Allowed types are: @types',
        [
          '@type' => $bundle,
          '@types' => implode(', ', $this->types)
        ]
      ));
    }
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository $termRepository */
    $termRepository = \Drupal::service(RepositoryManager::class)->repository('taxonomy_term');
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\ArtworkRepository $artworkRepository */
    $artworkRepository = \Drupal::service(RepositoryManager::class)->repository('node', $bundle);

    $results = $artworkRepository->all();
    $this->logger->notice(dt('Audit @count Nodes ...', ['@count' => count($results)]));

    $keywords = [];
    $totalArtworksWithMatch = 0;
    $chunks = array_chunk($results, 50);
    $count = 0;
    foreach ($chunks as $key => $chunk) {
      /** @var \Drupal\viola_content\WrappedEntities\Artwork $artwork */
      foreach ($chunk as $artwork) {
        if (is_int($options['limit']) && $count > $options['limit']) {
          continue;
        }
        // Run audit for each item.
        // Test the Ontology path terms of each artwork to see if the last part matches a Term from "keywords".
        $matched = FALSE;
        $properties = $artwork->getData();
        if (is_array($properties) && isset($properties['ontology'])) {
          foreach ($properties['ontology'] as $ontology) {
            $terms = explode('\\', $ontology['pathTerm']);
            // Check if the last term matches a keyword.
            $term = end($terms);
            if ($termRepository->termNameExists('keywords', $term)) {
              $matched = TRUE;
              $keywords[] = $term;
            }
            // Check if the second to last term matches a keyword.
            $term = prev($terms);
            if ($termRepository->termNameExists('keywords', $term)) {
              $matched = TRUE;
              $keywords[] = $term;
            }
          }
          if ($matched) {
            $totalArtworksWithMatch++;
          }
        }
        $count++;
      }
    }
    asort($keywords);
    // dump(json_encode(array_values(array_unique($keywords))));
    // TODO: Output result as a table.
    $this->logger->notice('Finished audit.');
    $this->logger->notice(dt(
      '@num keywords were matched in @total Artworks.',
      [
        '@num' => count(array_unique($keywords)),
        '@total' => $totalArtworksWithMatch,
      ]
    ));
  }

}
