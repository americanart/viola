# Viola Trumba

## Commands

### `drush viola-trumba:import`

Import events from a [trumba](https://trumba.com) JSON "feed". ([Trumba documentation](https://www.trumba.com/help/api/customfeeds.aspx))

Examples:
```shell script
// Full import of events.
drupal viola:trumba:import
```

```shell script
// Import two specific events by ID.
drush viola-trumba:import 133297177,133297178
```

```shell script
// Limit the number of items to import.
drush viola-trumba:import --limit 1
```

