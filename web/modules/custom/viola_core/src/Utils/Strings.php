<?php

namespace Drupal\viola_core\Utils;

class Strings {

  /**
   * Decode special characters in a given string.
   *
   * @param string $string
   *
   * @return string
   */
  public static function decodeSpecialChars($string) {
    return trim(html_entity_decode(htmlspecialchars_decode($string), ENT_QUOTES));
  }

  /**
   * Returns the parsed Ontology path as a friendly string with dashes separating terms.
   *
   * @param string $xref_term_path
   *   The Term Path. (For example: `Authorities\Attributes\Objects\Subject Specific\Occupation\political\senator`)
   * @param bool $typography
   *   Process return value with web typography filters.
   *
   * @return string
   */
  // TODO: This isn't really a string utility, where should it live?
  public static function getFriendlyOntology($xref_term_path, $typography = TRUE) {
    $output = '';
    $allowed_subjects = ['Subject Specific', 'Subject General', 'Patronage', 'Medium'];
    foreach ($allowed_subjects as $subject) {
      if (strpos($xref_term_path, $subject) !== FALSE) {
        $parts = explode($subject, $xref_term_path);
        $termPath = ltrim(end($parts), '\\');
        if ($termPath) {
          $separator = sprintf(' %s ', '--');
          $output = str_replace('\\', $separator, $termPath);
        }
      }
    }
    return $typography ? \Drupal::service('viola.typography')->process($output) : $output;
  }

  /**
   * Limit the length of a text string to the nearest word ending.
   *
   * @param  string $string
   * @param  int    $max_length
   * @return string
   */
  public static function truncateText(string $string, int $max_length): string {
    if (strlen($string) > $max_length) {
      $limit = $max_length ?? -1;
      $parts = preg_split('/([\s\n\r]+)/u', $string, $limit, PREG_SPLIT_DELIM_CAPTURE);
      $parts_count = count($parts);
      $length = 0;
      $last_part = 0;
      for (; $last_part < $parts_count; ++$last_part) {
        $length += strlen($parts[$last_part]);
        if ($length > $max_length) {
          break;
        }
      }
      return implode(array_slice($parts, 0, $last_part));
    } else {
      return $string;
    }
  }

  /**
   * Filter characters in a string.
   *
   * @param string $input
   *   The input string.
   * @param array $filter
   *   An array of string replacements to use on the identifier.
   *
   * @return string
   *   The filtered string.
   */
  public static function filter($input, array $filter = [
    ' ' => '-',
    '  ' => '-',
    '_' => '-',
    '/' => '-',
    '[' => '-',
    ']' => '',
  ]) {
    // Replace filter values in input string.
    $input = str_replace(array_keys($filter), array_values($filter), $input);
    // Strip invalid characters.
    // Valid characters are:
    // - the hyphen (U+002D)
    // - a-z (U+0030 - U+0039)
    // - A-Z (U+0041 - U+005A)
    // - the underscore (U+005F)
    // - 0-9 (U+0061 - U+007A)
    // - ISO 10646 characters U+00A1 and higher
    // We strip out any character not in the above list.
    $input = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', $input);
    return $input;
  }

  /**
   * Convert a string to Kebab Case.
   *
   * @param $string
   * @return string
   */
  public static function kebabCase($string) {
    // Replace invalid characters with a space.
    $string = self::filter($string);
    $string = trim($string);
    $string = strtolower($string);
    $string = str_replace(' ', '-', $string);

    return $string;
  }

  /**
   * Convert a string to Camel Case.
   *
   * @param $string
   * @return string
   */
  public static function camelCase($string) {
    $string = self::kebabCase($string);
    // Replace separator with spaces.
    $string = str_replace('-', ' ', $string);
    // uppercase the first character of each word
    $string = ucwords($string);
    $string = str_replace(' ', '', $string);
    $string = lcfirst($string);

    return $string;
  }

  /**
   * Convert a string to Title Case.
   *
   * @param $string
   * @return string
   */
  public static function titleCase($string) {
    $string = self::camelCase($string);
    $string = ucfirst($string);

    return $string;
  }

  /**
   * Convert a string to Snake Case.
   *
   * @param $string
   * @return string
   */
  public static function snakeCase($string) {
    $string = self::kebabCase($string);
    $string = str_replace('-', '_', $string);

    return $string;
  }

}

