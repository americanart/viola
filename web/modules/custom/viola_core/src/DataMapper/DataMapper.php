<?php

namespace Drupal\viola_core\DataMapper;

use Drupal\Component\Serialization\Json;

/**
 * DataMapper
 *  An in-memory representation of data from different sources.
 */
class DataMapper implements DataMapperInterface {

  /**
   * @var string
   *   The entity type
   */
  protected static $entityType;

  /**
   * @var string
   *   The entity bundle
   */
  protected static $bundle;

  /**
   * @var string
   *   The hash used to detect changes in data payload.
   */
  protected static $checksum;

  /**
   * @var array $data;
   */
  public $data = [];

  /**
   * @inheritDoc
   */
  public function setData(array $field_data) {}

  /**
   * @inheritDoc
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Get the current field data as a JSON string.
   *
   * @return string
   */
  public function toJson() {
    return Json::encode($this->data);
  }

  /**
   * Generate a checksum for data integrity checks.
   *
   * @param mixed $data
   *
   * @return string
   */
  public static function generateChecksum($data) {
    return md5(serialize($data));
  }

  /**
   * Check if an array key is valid.
   *
   * @param string $key
   * @param array $array
   *
   * @return boolean
   */
  public static function isValidDataKey($key, $array) {
    return (is_array($array) && isset($array[$key])) ? TRUE : FALSE;
  }

}
