<?php

namespace Drupal\viola_core\Utils;

class Markup {

    /**
     * Strip style markup from text.
     *
     * @param string $markup
     * @param array  $allowed
     *
     * @return string
     */
    public static function clean($markup, $allowed = ['<p>','<span>','<b>','<strong>',
    '<i>','<em>', '<br>','<a>','<cite>','<blockquote>','<cite>','<ul>','<ol>',
    '<li>','<dl>', '<dt>','<dd>','<h2>','<h3>','<h4>','<h5>','<h6>'])
    {
        $tags = '';
        // Add uppercase and lowercase of all allowed tags as a string.
        foreach ($allowed as $tag) {
            $tags .= strtolower($tag) . strtoupper($tag);
        }
        // Remove all but allowed tags.
        $stripped = strip_tags($markup, $tags);
        // Remove class and style attributes (double quoted).
        $stripped = preg_replace('/(<[^>]+) (style|class)=".*?"/i', '$1', $stripped);
        // Remove class and style attributes (single quoted).
        $stripped = preg_replace('/(<[^>]+) (style|class)=\'.*?\'/i', '$1', $stripped);
        // Remove specific (unquoted) class "MsoNormal".
        $stripped = preg_replace('/\s?\bclass=MsoNormal\b\s?/i', '$1', $stripped);
        // Remove empty </p> and </span> tags.
        $stripped = preg_replace('/<p[^>]*>[\s|&nbsp;]*<\/p>/i', '', $stripped);
        $stripped = preg_replace('/<span[^>]*>[\s|&nbsp;]*<\/span>/i', '', $stripped);

        return $stripped;
    }

    /**
     * Get the url from an "href" attribute of an anchor tag in a given string of HTML markup.
     *   If the given markup contains multiple hrefs the first match is returned.
     *
     * @param string $markup
     *
     * @return string
     */
    public static function getUrl($markup)
    {
        $string = self::clean(trim($markup), ['<a>']);
        preg_match_all('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $string, $links);
        if (is_array($links) && isset($links['href']) && !empty($links['href'])) {
            // The first found url.
            return reset($links['href']);
        } else {
            return false;
        }
    }
}
