<?php

namespace Drupal\viola_elasticsearch\EventSubscriber;

use Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent;
use Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent;
use Drupal\viola_elasticsearch\Elastic\Indexer;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Class EntityEventSubscriber.
 *
 * A service to subscribe to entity events dispatched by the "hook_event_dispatcher" contrib module.
 *
 */
class EntityEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Indexer
   */
  protected $indexer;

  /**
   * The content types (machine names) enabled.
   *
   * @var array
   */
  protected $enabled;

  /**
   * @param \Drupal\Core\Database\Connection $connection
   *   The Drupal default database connection.
   */
  public function __construct(Indexer $indexer) {
    $this->indexer = $indexer;
    $contentTypeSettings = \Drupal::config('viola_elasticsearch.settings')->get('content_types_enabled');
    $this->enabled = is_array($contentTypeSettings) ? array_values(array_filter($contentTypeSettings)) : [];
  }

  /**
   * Entity create.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityInsertEvent $event
   *   The event.
   * @throws \Exception
   */
  public function entityInsert(EntityInsertEvent $event): void {
    $entity = $event->getEntity();
    if ($entity->getEntityTypeId() == 'node' and in_array($entity->bundle(), $this->enabled)) {
      if (in_array($entity->bundle(), $this->indexer->nodeBundles)) {
        \Drupal::messenger()->addStatus('Queued a job to index a new document in Elasticsearch.');
        $this->indexer->singleOperation($entity, 'index');
      }
    }
  }

  /**
   * Entity delete.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityDeleteEvent $event
   *   The event.
   * @throws \Exception
   */
  public function entityDelete(EntityDeleteEvent $event): void {
    $entity = $event->getEntity();
    if ($entity->getEntityTypeId() == 'node' and in_array($entity->bundle(), $this->enabled)) {
      if (in_array($entity->bundle(), $this->indexer->nodeBundles)) {
        \Drupal::messenger()->addStatus('Queued a job to remove a deleted document from Elasticsearch.');
        $this->indexer->singleOperation($entity, 'delete');
      }
    }
  }

  /**
   * Entity update.
   *
   * @param \Drupal\core_event_dispatcher\Event\Entity\EntityUpdateEvent $event
   *   The event.
   * @throws \Exception
   */
  public function entityUpdate(EntityUpdateEvent $event): void {
    $entity = $event->getEntity();
    if ($entity->getEntityTypeId() == 'node' and in_array($entity->bundle(), $this->enabled)) {
      if (in_array($entity->bundle(), $this->indexer->nodeBundles)) {
        \Drupal::messenger()->addStatus('Queued a job to re-index a modified document in Elasticsearch.');
        $this->indexer->singleOperation($entity, 'index');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      HookEventDispatcherInterface::ENTITY_INSERT => 'entityInsert',
      HookEventDispatcherInterface::ENTITY_DELETE => 'entityDelete',
      HookEventDispatcherInterface::ENTITY_UPDATE => 'entityUpdate',
    ];
  }

}
