<?php

namespace Drupal\viola_content\Formats\Paragraph\HeadingComponent;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Paragraph\HeadingComponent;

final class DefaultFormat extends FormatBase {

  public $size;
  public $title;

  /**
   * Paragraph\HeadingComponent\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Paragraph\HeadingComponent $component
   *   The wrapped entity.
   */
  public function __construct(HeadingComponent $component) {
    parent::__construct();
    $this->bundle = $component->getEntity()->bundle();
    $this->type = $component->getEntity()->getEntityTypeId();
    $this->size = $component->getSize();
    $this->title = $component->getTitle();
  }

}
