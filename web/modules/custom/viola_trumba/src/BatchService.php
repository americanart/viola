<?php

namespace Drupal\viola_trumba;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\viola_core\DataMapper\DataMapperFactory;
use Drupal\viola_core\Queue\QueueManager;

/**
 * Class BatchService.
 */
class BatchService {

  use StringTranslationTrait;

  /**
   * Batch process callback for Trumba Import.
   *
   * @param array $data
   *   Import data for the batch.
   * @param string $operation_details
   *   Details of the operation.
   * @param object $context
   *   Context for operations.
   */
  public static function batchProcessTrumbaImport($data, $operation_details, &$context) {
    try {
      // Remove unused content import fields.
      unset($data['eventImage']);
      $event = DataMapperFactory::create('viola_trumba', 'event', $data);
      $queued = QueueManager::queueJob(
        'trumba_import_job',
        'create',
        'node',
        'event',
        $event->getData(),
        'viola_trumba'
      );
      // Store some results for post-processing in the 'finished' callback.
      if ($queued) {
        $context['results'][] = $data['eventID'];
        // Optional message displayed under the progressbar.
        $context['message'] = t('@details', [
          '@details' => $operation_details,
        ]);
      }
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

  /**
   * Batch Finished callback.
   *
   * @param bool $success
   *   Success of the operation.
   * @param array $results
   *   Array of results for post processing.
   * @param array $operations
   *   Array of operations.
   */
  public static function batchFinished($success, array $results, array $operations) {
    if ($success) {
      \Drupal::messenger()->addMessage(dt('Queued @count import jobs.', [
        '@count' => count($results),
      ]));
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      \Drupal::messenger()->addError(dt('An error occurred while processing @operation with arguments : @args', [
        '@operation' => $error_operation[0],
        '@args' => print_r($error_operation[0], TRUE),
      ]));
    }
  }

}
