<?php
namespace Drupal\viola_content\Plugin\TypedRepositories;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermInterface;
use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_core\SystemInfo;

/**
  * The repository for taxonomy terms.
  *
  * @TypedRepository(
  *   entity_type_id = "taxonomy_term",
  *   wrappers = @ClassWithVariants(
  *      fallback = "Drupal\viola_content\WrappedEntities\TaxonomyTerm",
  *    ),
  *   description = @Translation("Repository that holds logic for taxonomy terms")
  * )
 *
*/
final class TaxonomyTermRepository extends TypedRepositoryBase {

  /**
   * Prepare supplied fields and field values for entity create/update.
   *
   * @param array $fields
   *
   * @return array
   *   Combined array of default and supplied fields.
   */
  protected function prepareFields(array $fields) {
    $commonFields = [
      'langcode' => 'en',
      'changed' => \Drupal::time()->getRequestTime(),
      'uid' => 1,
      'status' => 1,
    ];
    return array_merge($commonFields, $fields);
  }

  /**
   * Load entities filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]
   */
  public function findWhere($fields = []) {
    try {
      $query = $this->entityTypeManager->getStorage($this->getEntityType()->id());
      foreach ($fields as $field_name => $field_value) {
        $conditions[$field_name] = $field_value;
      }
      $results = $query->loadByProperties($conditions);
      $entities = [];
      foreach ($results as $item) {
        $entities[] = $item;
      }
      var_dump(count($entities));
      return $this->wrapMultiple($entities);
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return [];
  }

  /**
   * Load the first entity from results filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   */
  public function firstWhere($fields = []) {
    if ($results = $this->findWhere($fields)) {
      return reset($results);
    }
    return NULL;
  }

  /**
   * Update an existing Term.
   *
   * @param array $fields
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *
   * @return int
   */
  public function updateTerm(array $fields, EntityInterface $entity) {
    $status = 0;
    try {
      $fields = $this->prepareFields($fields);
      foreach ($fields as $field_name => $field_value) {
        $entity->set($field_name, $field_value);
      }
      $status = $entity->save();
      if ($status == SAVED_UPDATED && $entity instanceof TermInterface) {
        // Update the Node with a path alias.
        \Drupal::service('pathauto.generator')->updateEntityAlias($entity, 'update');
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }

  /**
   * Create taxonomy terms from an array of term's fields.
   *
   * @param array $termsFieldsList
   *   An array of Terms fields.
   *   Example:
   *   [
   *    'vid' => 'tags',
   *    'langcode' => 'en',
   *    'name' => 'new york',
   *    'parent' => 0,
   *    'children' => [],
   *   ];.
   *
   * @return array
   *   Array of tids with nested array of child terms under 'children' key.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createTermsFromFields(array $termsFieldsList) {
    $terms = [];
    $isNested = FALSE;
    foreach ($termsFieldsList as $fields) {
      $children = (isset($fields['children']) && !empty($fields['children'])) ? $fields['children'] : [];
      if (is_array($children)) {
        $isNested = TRUE;
        unset($fields['children']);
      }
      // If nested, the current tid is used as the parent of the next item.
      $currentId = $this->createTermFromFields($fields);
      $terms[] = $currentId;
      if ($isNested) {
        foreach ($children as $child) {
          $child['parent'] = $currentId;
          // Recurse to create nested terms after setting the parent.
          $currentId = $this->createTermsFromFields([$child]);
          $terms[] = array_values($currentId);
        }
      }
    }
    // Recursion creates nested term id results, so flatten and return.
    $finalTerms = [];
    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($terms));
    foreach ($iterator as $value) {
      $finalTerms[] = $value;
    }
    return $finalTerms;
  }

  /**
   * Create a new Term populated by the provided field values.
   *
   * @param array $fields
   *
   * @return int
   */
  public function createTerm(array $fields) {
    $status = 0;
    try {
      $fields = $this->prepareFields($fields);
      $entity = $this->entityTypeManager->getStorage($this->getEntityType()->id())->create($fields)->enforceIsNew();
      $status = $entity->save();
      if ($status == SAVED_NEW && $entity instanceof NodeInterface) {
        // Update the Node with a path alias.
        \Drupal::service('pathauto.generator')->updateEntityAlias($entity, 'update');
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }

  /**
   * Create or update a Term given an Assoc array of fields.
   *
   * @param array $fields
   *   The entity field data.
   *
   * @return int|bool
   *   The Term ID.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function createTermFromFields(array $fields = []) {
    if (isset($fields['tid']) && !empty($fields['tid'])) {
      $existingTerm = Term::load($fields['tid']);
      // Update existing term.
      $updated = $this->updateTerm($fields, $existingTerm);
      if (!$updated) {
        $fields['tid'] = FALSE;
      }
    }
    else {
      // Create a new Term.
      $new = $this->createTerm($fields);
      if ($new) {
        $newId = $this->getTermIdByName($fields['vid'], $fields['name'], $fields['parent']);
        $fields['tid'] = $newId;
      }
    }
    return $fields['tid'] ? $fields['tid'] : FALSE;
  }

  /**
   * Given a string list of taxonomy terms, build an array of Term fields for each item.
   *
   *   Nested terms are attached to a parent term keyed by "children".
   *
   * @param string $vocab
   *   The Vocabulary ID.
   * @param string $list
   *   A comma-separated string of term names.
   * @param bool $nested
   *   The order of the list items denote parent/child relationship.
   *
   * @return array
   *   Array of terms fields info including vid, tid, name, and parent ID.
   */
  public function getTermFieldsList($vocab, $list, $nested = FALSE) {
    $items = explode(',', $list);
    $terms = [];
    foreach ($items as $name) {
      if ($name && $name != "<null>") {
        // Remove "and" from seperated string elements.
        $name = ltrim($name, 'and ');
        $term = [];
        $term['name'] = trim($name);
        $term['vid'] = $vocab;
        $term['langcode'] = 'en';
        $term['parent'] = 0;
        $tid = $this->getTermIdByName($vocab, $term['name'], $term['parent']);
        $term['tid'] = is_numeric($tid) ? $tid : 0;
        $terms[] = $term;
      }
    }
    // Nest the terms array, the order indicating parent/child relationship.
    $nestedTerms = [];
    if ($nested && !empty($terms)) {
      $i = count($terms);
      $lastTerm = array_key_exists($i, $terms) ? $terms[$i] : [];
      $i--;
      while ($i >= 0) {
        $nestedTerms = [];
        $terms[$i]['children'] = $lastTerm;
        $nestedTerms[] = $terms[$i];
        $lastTerm = $nestedTerms;
        $i--;
      }
    }
    $terms = !empty($nestedTerms) ? $nestedTerms : $terms;
    return $terms;
  }

  /**
   * Get Term's parent id.
   *
   * @param int $tid
   *   The Term ID.
   *
   * @return int
   *   The parent id.
   */
  public function getTermParentId($tid) {
    $query = \Drupal::database()->select('taxonomy_term__parent', 'tp');
    $query->fields('tp', ['parent_target_id']);
    $query->condition('entity_id', $tid, '=');
    $results = $query->execute()->fetchCol();
    return reset($results);
  }

  /**
   * Get child Terms.
   *
   * @param int $pid
   *   The Parent Term ID.
   *
   * @return array
   *   The term ids.
   */
  public function getTermChildren($pid) {
    $query = \Drupal::database()->select('taxonomy_term_hierarchy', 'th');
    $query->fields('th', ['tid']);
    $query->condition('parent', $pid, '=');
    $results = $query->execute()->fetchCol();
    return $results;
  }

  /**
   * Get the Term ID of a given name.
   *
   * @param string $vid
   *   The Vocabulary vid (i.e. tags)
   * @param string $name
   *   The term name.
   * @param int $parentId
   *   The term parent id.
   *
   * @return int
   *   The term id.
   */
  public function getTermIdByName($vid, $name, $parentId = 0) {
    $query = \Drupal::database()->select('taxonomy_term_field_data', 'tf');
    $query->fields('tf', ['tid', 'vid', 'name']);
    $query->condition('tf.vid', $vid);
    $query->condition('tf.name', $name);
    if ($parentId > 0) {
      $query->join('taxonomy_term__parent', 'tp', 'tp.entity_id = tf.tid');
      $query->addField('tp', 'parent_target_id');
      $query->condition('tp.parent_target_id', $parentId, '=');
    }
    // Get the terms as a tid-indexed array.
    $tids = $query->execute()->fetchAllKeyed();
    reset($tids);
    return key($tids) ? key($tids) : 0;
  }

  /**
   * Check existence of a particular taxonomy term name.
   *
   * @param string $vocab
   *   The Vocabulary vid (i.e. tags)
   * @param string $name
   *   The term name.
   *
   * @return bool
   *   Boolean answer to if a supplied name exists.
   */
  public function termNameExists($vocab, $name) {
    return $this->getTermIdByName($vocab, $name) ? TRUE : FALSE;
  }

}
