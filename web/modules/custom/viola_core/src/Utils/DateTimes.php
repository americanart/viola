<?php

namespace Drupal\viola_core\Utils;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

class DateTimes {

    /**
     * Convert a date string from one format to another.
     *
     * @param  $string
     * @param  string $fromFormat
     * @param  string $toFormat
     * @return string
     */
    public static function convertDateFormat($string, $fromFormat = 'Y-m-d', $toFormat = 'F j, Y') {
        $date = \DateTime::createFromFormat($fromFormat, $string);
        return ($date instanceof \DateTime) ? $date->format($toFormat) : '';
    }

  /**
   * Get a Drupal formatted date string.
   *
   * @param string $dateString
   * @param string $timezone
   *   The timezone abbreviation (i.e. "America/New_York")
   *
   * @return string
   */
  public static function getFormattedDateTime($dateString, $timezone = '') {
    if (!$timezone) {
      $timezone = DateTimeItemInterface::STORAGE_TIMEZONE;
    }
    $tz = new \DateTimeZone($timezone);
    $date = new DrupalDateTime($dateString, $tz);
    return \Drupal::service('date.formatter')
      ->format(
        $date->getTimestamp(),
        'custom',
        DateTimeItemInterface::DATETIME_STORAGE_FORMAT,
        \Drupal::config('system.date')->get('timezone')['default']
      );
  }

}
