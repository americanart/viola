<?php

namespace Drupal\viola_wikimedia\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaWikimediaCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\viola_wikimedia\HttpClient\WikidataClient
   */
  public $wikidataClient;

  /**
   * @var int
   */
  public $queued;

  /**
   * The allowed import content types.
   *
   * @var array
   */
  protected $types = [
    'artwork',
    'creator',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->wikidataClient = \Drupal::service('viola.wikidata_client');
  }

  /**
   * Queue imports of select Wikipedia fields to store on content.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option $ids
   *   Comma-separated list of specific Node id(s) to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @option force
   *   Force import, bypassing the checksum comparison.
   * @usage viola-wikimedia:wikipedia-import
   *
   * @command viola-wikimedia:wikipedia-import
   * @aliases viola-wiki:wikipedia
   */
  public function wikipediaImport($type = '', $options = [
    'ids' => '',
    'limit' => 100000,
    'offset' => 0,
    'force' => FALSE,
    ]
  ) {
    $this->logger()->notice(dt("Running viola-wikimedia:wikipedia-import ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    if ($bundle == 'creator') {
      $identifier = 'field_constituent_id';
    }
    else {
      $identifier = 'field_object_id';
    }
    $connection = \Drupal::database();
    $query = $connection->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'type']);
    $query->condition('n.type', $bundle, '=');
    $query->condition('n.status', 1);
    $query->join(sprintf('node__%s', $identifier), 'fid', 'fid.entity_id = n.nid');
    $query->addField('fid', sprintf('%s_value', $identifier));
    // TODO: Add `field_wikipedia_title` to Artwork, or figure out a better ID for Wikipedia (like "pageid")
    $query->join('node__field_wikipedia_title', 'fw', 'fw.entity_id = n.nid');
    $query->addField('fw', 'field_wikipedia_title_value');
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    if ($ids) {
      // Add condition for supplied Node ID(s).
      $query->condition('n.nid', $ids, 'IN');
    }
    if ($options['limit'] || $options['offset']) {
      $query->range($options['offset'], $options['limit']);
    }
    $results = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);

    if ($results) {
      $batch = [
        'title' => dt('Import processing ...'),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_wikimedia\BatchService::batchFinished',
      ];
      foreach ($results as $result) {
        if ($options['force']) {
          $result['force'] = TRUE;
        }
        // Prepare the operations, keyed by process method call.
        $batch['operations'][] = [
          'Drupal\viola_wikimedia\BatchService::batchProcessWikipedia', [
            $result,
            dt('Queued import for @bundle with Constituent ID: @id, and Wikipedia Title: @title', [
              '@bundle' => $bundle,
              '@id' => $result[sprintf('%s_value', $identifier)],
              '@title' => $result['field_wikipedia_title_value'],
            ]),
          ],
        ];
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

  /**
   * Queue imports of select Wikidata fields to store on content.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option ids
   *   A comma-separated list of specific drupal Node ID's to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @usage viola-wikimedia:wikidata-import
   * @option force
   *   Force import, bypassing the checksum comparison.
   *
   * @command viola-wikimedia:wikidata-import
   * @aliases viola-wiki:wikidata
   */
  public function wikidataImport($type = '', $options = [
    'ids' => '',
    'limit' => 100000,
    'offset' => 0,
    'force' => FALSE,
  ]
  ) {
    $this->logger()->notice(dt("Running viola-wikimedia:wikidata-import ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    if ($bundle == 'creator') {
      $identifier = 'field_constituent_id';
    }
    else {
      $identifier = 'field_object_id';
    }
    $connection = \Drupal::database();
    $query = $connection->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'type']);
    $query->condition('n.type', $bundle, '=');
    $query->condition('n.status', 1);
    $query->join(sprintf('node__%s', $identifier), 'fid', 'fid.entity_id = n.nid');
    $query->addField('fid', sprintf('%s_value', $identifier));
    $query->join('node__field_wikidata_id', 'fw', 'fw.entity_id = n.nid');
    $query->addField('fw', 'field_wikidata_id_value');
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    if ($ids) {
      // Add condition for supplied Node ID(s).
      $query->condition('n.nid', $ids, 'IN');
    }
    if ($options['limit'] || $options['offset']) {
      $query->range($options['offset'], $options['limit']);
    }
    $results = $query->execute()->fetchAllAssoc('nid', \PDO::FETCH_ASSOC);
    if ($results) {
      $batch = [
        'title' => dt('Import processing ...'),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_wikimedia\BatchService::batchFinished',
      ];
      foreach ($results as $result) {
        if ($options['force']) {
          $result['force'] = TRUE;
        }
        // Prepare the operations, keyed by process method call.
        $batch['operations'][] = [
          'Drupal\viola_wikimedia\BatchService::batchProcessWikidata', [
            $result,
            dt('Queued import for @bundle with ID: @id and Wikidata ID: @wikidata_id', [
              '@bundle' => $bundle,
              '@id' => $result[sprintf('%s_value', $identifier)],
              '@wikidata_id' => $result['field_wikidata_id_value'],
            ]),
          ],
        ];
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

}
