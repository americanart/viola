<?php
namespace Drupal\viola_content\Plugin\TypedRepositories;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
* The repository for menu links.
*
* @TypedRepository(
*   entity_type_id = "menu_link_content",
*   description = @Translation("Repository that holds logic for menus")
* )
*/
final class MenuLinkContentRepository extends TypedRepositoryBase {

  /**
   * Create a new Menu Link entity populated by the provided field values.
   *
   * @param array $fields
   *
   * @return int
   */
  public function createLink(array $fields) {
    $status = 0;
    try {
      $commonFields = [
        'langcode' => 'en',
        'changed' => \Drupal::time()->getRequestTime(),
        'uid' => 1,
        'status' => 1,
      ];
      $fields = array_merge($commonFields, $fields);
      $entity = $this->entityTypeManager->getStorage($this->getEntityType()->id())->create($fields)->enforceIsNew();
      $status = $entity->save();
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }


  /**
   * Get the Link ID of a MenuLinkContent entity by name.
   *
   * @param string $title
   *   The title for a menu link.
   * @param string $menu
   *   The menu machine name.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entity objects indexed by their ids.
   */
  public function findByTitle($title, $menu = '') {
    try {
      $storage = $this->entityTypeManager->getStorage($this->getEntityType()->id());
      $conditions = ['title' => $title];
      if ($menu) {
        $conditions['menu_name'] = $menu;
      }
      return $storage->loadByProperties($conditions);
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return [];
  }

}
