<?php

namespace Drupal\viola_content\Formats\Media\Audio;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Media\Audio;

final class DefaultFormat extends FormatBase {

  public $copyright;
  public $description;
  public $directory;
  public $fileSize;
  public array $image = [];
  public $mimeType;
  public array $tags = [];
  public $transcript;
  public $usageRights;

  /**
   * Media\Audio\DefaultFormat constructor.
   *   The default format gets this object's properties as a PHP array.
   *
   * @param \Drupal\viola_content\WrappedEntities\Media\Audio $audio
   *   The wrapped entity.
   */
  public function __construct(Audio $audio) {
    parent::__construct();
    $this->bundle = $audio->getBundle();
    $this->type = $audio->getEntity()->getEntityTypeId();
    $this->copyright = $audio->getCopyright();
    $this->description = $audio->getDescription();
    $this->directory = $audio->getDirectory();
    $this->fileSize = $audio->getFileSize();
    // Get image data.
    if ($wrappedImage = $audio->getImage()) {
      $this->image = $wrappedImage->transformInto('default');
    }
    $this->mimeType = $audio->getMimeType();
    $this->tags = $audio->getTags();
    $this->transcript = $audio->getTranscript();
    $this->usageRights = $audio->getUsageRights();
  }

}
