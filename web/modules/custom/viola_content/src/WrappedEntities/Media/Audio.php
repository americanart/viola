<?php

namespace Drupal\viola_content\WrappedEntities\Media;

use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the audio media.
 */
class Audio extends TransformableWrappedEntity {

  use HasCommonEntityFieldsTrait;
  use HasEntityReferencesTrait;

  const MEDIA_FIELD_NAME = 'field_media_audio_file';

  /**
   * @return string
   */
  public function getCopyright() {
    return $this->getEntity()->get('field_copyright')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->getEntity()->get('field_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDirectory() {
    $labels = $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_directory'
    );
    return reset($labels);
  }

  /**
   * @return string
   */
  public function getFileSize() {
    return $this->getEntity()->get('field_file_size')->value ?? '';
  }

  /**
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface
   */
  public function getImage() {
    $images = $this->wrapReferencedEntites($this->getEntity(), 'field_image');
    return reset ($images);
  }

  /**
   * @return string
   */
  public function getMimeType() {
    return $this->getEntity()->get('field_mime_type')->value ?? '';
  }

  /**
   * @return array
   */
  public function getTags() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_tags'
    );
  }

  /**
   * @return string
   */
  public function getTranscript() {
    return $this->getEntity()->get('field_transcript')->value ?? '';
  }

  /**
   * @return string
   */
  public function getUsageRights() {
    $labels = $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_usage_rights'
    );
    return reset($labels);
  }

}
