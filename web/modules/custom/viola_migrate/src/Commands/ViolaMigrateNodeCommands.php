<?php

namespace Drupal\viola_migrate\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drupal\viola_core\Utils\DateTimes;
use Drush\Commands\DrushCommands;
use Jenssegers\ImageHash\ImageHash;
use Jenssegers\ImageHash\Implementations\DifferenceHash;

class ViolaMigrateNodeCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The path to data directory.
   *
   * @var string
   */
  protected $dataDirectory;

  /**
   * The allowed content types.
   *
   * @var array
   */
  protected $types = [
    'exhibition',
    'person',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->dataDirectory = dirname(__DIR__, 2) . '/data';
  }

  /**
   * Import menu link content for a given menu..
   *
   * @param string $type
   *   The menu machine name.
   *
   * @option limit
   *   Limit number of items to import.
   * @option ids
   *   A comma-separated list of specific content ids to import.
   * @usage viola-migrate:node exhibition
   *
   * @command viola-migrate:node-import
   * @aliases viola-migrate:node
   */
  public function importNode($type, $options = ['limit' => 55000, 'ids' => '']) {
    $this->logger()->notice(dt("Running viola-migrate:node-import ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    $importFile = sprintf('%s/%s/%s.%s', $this->dataDirectory, 'nodes', $bundle, 'json');
    $reader = new JsonDataReader('file://', $importFile);
    $data = $reader->read()->getResults();

    if (!empty($data)) {
      $count = 0;
      foreach ($data as $item) {
        if ($options['limit'] && $count >= (int) $options['limit']) {
          break;
        }
        // TODO: Should I move these methods to "DataMapper" classes?
        switch ($bundle) {
          case 'exhibition':
            $status = $this->createExhibition($item);
            break;
          case 'person':
            $status = $this->createPerson($item);
            break;
          default:
            $status = FALSE;
        }
        if ($status === SAVED_NEW) {
          $count++;
        }
        else {
          $this->logger->error(dt('There was a problem importing @bundle `@title`', ['@bundle' => $bundle, '@title' => $item['title']]));
        }
      }
      if ($count) {
        $this->logger->success(dt('Processed @count @bundle nodes.', ['@count' => $count, '@bundle' => $bundle]));
      }
      $this->logger->notice(dt('Done.'));
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

  /**
   * Create an Exhibition Node.
   *
   * @param $data
   * @return int
   *   Either SAVED_NEW, SAVED_DELETED, or SAVED_UPDATED, depending on the operation performed.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createExhibition($data) {
    // TODO: Handle Creators, Publications, and Curators relationships.
    $created = FALSE;
    $fields = [];
    $fields['type'] = 'exhibition';
    $fields['field_description'] = [
      'value' => $data['body'],
      'summary' => $data['summary'],
      'format' => 'full_html',
    ];
    $fields['title'] = $data['title'];
    $fields['field_date_description'] = $data['field_display_date'] ?? '';
    if ($nid = $data['nid']) {
      $fields['field_legacy_id'] = sprintf('node:%s', $nid);
    }
    // Create the default image.
    $data['image']['title'] = sprintf('Exhibition Image: %s', $data['title']);
    if ($mid = $this->createImage($data['image'])) {
      $fields['field_image'] = $mid;
    }
    // Create the Node.
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\ExhibitionRepository $exhibitionRepository */
    $exhibitionRepository = \Drupal::service(RepositoryManager::class)->repository('node', 'exhibition');
    $created = $exhibitionRepository->createNode($fields);
    // Create an "Event Venue" paragraph that will be attached to the Exhibition Node.
    if ($created == SAVED_NEW && $fields['field_legacy_id']) {
      $wrappedEntity = $exhibitionRepository->firstWhere(['field_legacy_id' => $fields['field_legacy_id']]);
      if ($wrappedEntity) {
        if ($node = $wrappedEntity->getEntity()) {
          $venueFields = [
            'type' => 'event_venue',
            'langcode' => 'en',
            'status' => 1,
          ];
          /** @var \Drupal\viola_content\Plugin\TypedRepositories\TaxonomyTermRepository $termRepository */
          $termRepository = \Drupal::service(RepositoryManager::class)
            ->repository('taxonomy_term');
          if ($tid = $termRepository->getTermIdByName('venues', $data['field_venue'])) {
            $venueFields['field_venue'] = $tid;
          }
          // Schedule (Datetime range).
          $timezone ='America/New_York';
          $venueFields['field_schedule'] = [
            'value' => DateTimes::getFormattedDateTime($data['field_open_date'], $timezone),
            'end_value' => DateTimes::getFormattedDateTime($data['field_close_date'], $timezone),
          ];
          $paragraph = Paragraph::create($venueFields)->enforceIsNew();
          $new = $paragraph->save();
          if ($new == SAVED_NEW) {
            // Append the new paragraph to parent Node.
            $node->get('field_venues')->appendItem($paragraph);
            $node->save();
          }
        }
      }
    }
    return $created;
  }

  /**
   * Create a Person Node.
   *
   * @param $data
   * @return int
   *   Either SAVED_NEW, SAVED_DELETED, or SAVED_UPDATED, depending on the operation performed.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createPerson($data) {
    $fields = [];
    if (isset($data['uid'])) {
      // Create "person" node from an export of previous User entities.
      $fields['field_legacy_id'] = sprintf('user:%s', $data['uid']);
      $fields['title'] = sprintf('%s %s', $data['field_first_name'] ?: '', $data['field_last_name'] ?: '');
      $fields['field_alphabetical_name'] = sprintf('%s, %s', $data['field_last_name'] ?: '', $data['field_first_name'] ?: '');
      $fields['field_biography'] = [
        'value' => $data['field_biography'],
        'format' => 'basic_html',
      ];
      $fields['field_summary'] = $data['field_summary'];
      $fields['field_job_title'] = $data['field_job_title'];
      if ($mid = $this->createImage($data['user_picture'])) {
        $fields['field_image'] = $mid;
      }
    }
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\PersonRepository $personRepository */
    $personRepository = \Drupal::service(RepositoryManager::class)->repository('node', 'person');
    return $personRepository->createNode($fields);
  }

  /**
   * Create an image.
   *
   * @param $data
   *   The image data.
   *
   * @return false|int
   *   The media entity ID or FALSE.
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createImage($data) {
    /** @var \Drupal\viola_content\Plugin\TypedRepositories\ImageRepository $repository */
    $repository = \Drupal::service(RepositoryManager::class)->repository('media', 'image');
    // Get the pHash of provided image.
    if (isset($data['download_src']) && filter_var($data['download_src'], FILTER_VALIDATE_URL)) {
      $hasher = new ImageHash(new DifferenceHash());
      $hash = $hasher->hash($data['download_src']);
      $phash = $hash->toHex();
      // Find an existing image with a matching perceptual hash.
      // Note: Slight changes to an image will cause a different hash, and would require a more complex query
      // to find images with hashes within a "difference" range.
      // @see: https://github.com/jenssegers/imagehash/issues/55#issuecomment-620790751
      /** @var \Drupal\viola_content\WrappedEntities\Image $existingImage */
      $existingImage = $repository->firstWhere(['field_phash' => $phash]);
      if ($existingImage) {
        return $existingImage->getDrupalId();
      }
      else {
        // Create a new image, setting the pHash and usage values.
        $image = [];
        $image['name'] = $data['title'] ?? sprintf('Image: %s', $phash);
        $image['field_phash'] = $phash;
        $image['image_url'] = $data['download_src'];
        $image['alt'] = $data['alt'] ?? '';
        $image['field_caption'] = $data['caption'] ?? '';
        $status = $repository->createMedia($image);
        if ($status) {
          /** @var \Drupal\viola_content\WrappedEntities\Image $newImage */
          $newImage = $repository->firstWhere(['field_phash' => $phash]);
          if ($newImage) {
            return $newImage->getDrupalId();
          }
        }
      }
    }
    return FALSE;
  }

}
