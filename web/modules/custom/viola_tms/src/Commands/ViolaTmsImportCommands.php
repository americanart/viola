<?php

namespace Drupal\viola_tms\Commands;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_core\DataMapper\DataMapperFactory;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drupal\viola_core\Queue\QueueManager;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaTmsImportCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * The allowed import content types.
   *
   * @var array
   */
  protected $types = [
    'artwork',
    'creator',
  ];

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Queue imports of TMS content.
   *
   * @param string $type
   *   The content type to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @option date
   *   Date, import date to retrieve. Format: Y-m-d (i.e. "2001-09-11").
   * @option full
   *   Run a full content import.
   * @option ids
   *   A comma-separated list of specific TMS Ids (object or constituent) to import.
   * @option force
   *   Force import, bypassing the checksum comparison.
   * @usage viola-tms:import
   *
   * @command viola-tms:import
   */
  public function import($type = '', $options = [
    'limit' => 100000,
    'offset' => 0,
    'date' => '',
    'ids' => '',
    'full' => FALSE,
    'force' => FALSE
  ]) {
    $this->logger()->notice(dt("Running viola-tms:import ..."));
    $bundle = mb_strtolower($type);
    if (!in_array($bundle, $this->types)) {
      $message = sprintf('Invalid type, %s. Allowed types are: %s', $bundle, implode(', ', $this->types));
      throw new \InvalidArgumentException($message);
    }
    $exportType = ($bundle == 'creator') ? 'constituent' : 'artwork';
    // TODO: Set a timezone on the date to ensure accuracy.
    $dateTime = $options['date'] ? strtotime($options['date']) : time();
    $date = date('Y-m-d', $dateTime);
    // Build the import file URL, daily import files are prefixed with "audited_".
    $prefix = $options['full'] ? 'full_' : 'audited_';
    $filepath = sprintf('tms/%s%s_%s.json', $prefix, $exportType, $date);
    // Retrieve and queue import results for processing.
    $reader = new JsonDataReader(
      's3://',
      $filepath,
      $options['limit'],
      $options['offset']
    );
    // Filter results by supplied ids (if any).
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    $identifierKey = $bundle === 'artwork' ? 'objectId' : 'constituentId';
    $results = $reader->read()->filter($identifierKey, $ids)->getResults();
    if ($results) {
      $batch = [
        'title' => dt('Import processing ...'),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_tms\BatchService::batchFinished',
      ];
      foreach ($results as $result) {
        if ($options['force']) {
          $result['force'] = TRUE;
        }
        $valid = TRUE;
        if ($bundle == 'creator' && strpos(strtolower($result['displayName']), 'unidentified') !== FALSE) {
          // Don't import "Unidentified" Constituents.
          $valid = FALSE;
        }
        if ($valid) {
          // Prepare the operations, keyed by process method call.
          $batch['operations'][] = [
            'Drupal\viola_tms\BatchService::batchProcessTmsImport', [
              $bundle,
              $result,
              dt('Queued import for @type: @id', [
                '@type' => ucfirst($bundle),
                '@id' => $result[$identifierKey],
            ]),
            ],
          ];
        }
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

}
