<?php
/**
 * Override Drupal settings in `/web/sites/default/settings.php`
 */

$settings['trusted_host_patterns'] = [
  '^example\.com',
  '^viola\.lndo\.site',
];

