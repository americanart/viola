<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Media;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\IsMediaManagerTrait;
use Drupal\viola_content\Plugin\TypedRepositories\Traits\OutputsStatusMessageTrait;

/**
* The repository for image.
*
* @TypedRepository(
*    entity_type_id = "media",
*    bundle = "image",
*    wrappers = @ClassWithVariants(
*      fallback = "Drupal\viola_content\WrappedEntities\Media\Image",
*    ),
*   description = @Translation("Repository that holds logic for images")
* )
*/
final class ImageRepository extends TypedRepositoryBase {

  use IsMediaManagerTrait;
  use OutputsStatusMessageTrait;

}
