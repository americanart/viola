<?php
namespace Drupal\viola_content\Plugin\TypedRepositories\Paragraph;

use Drupal\typed_entity\Annotation\TypedRepository;
use Drupal\typed_entity\TypedRepositories\TypedRepositoryBase;

/**
 * The repository for `heading_component` paragraphs.
 *
 * @TypedRepository(
 *    entity_type_id = "paragraph",
 *    bundle = "heading_component",
 *    wrappers = @ClassWithVariants(
 *      fallback = "Drupal\viola_content\WrappedEntities\Paragraph\HeadingComponent",
 *    ),
 *   description = @Translation("Repository that holds logic for heading components")
 * )
 */
final class HeadingComponentRepository extends TypedRepositoryBase {}
