<?php

namespace Drupal\viola_content\Formats\Node\Artwork;

use Drupal\viola_content\Formats\FormatBase;
use Spatie\SchemaOrg\Schema;
use Drupal\viola_content\WrappedEntities\Node\Artwork;

final class JsonLdFormat extends FormatBase {

  /**
   * @var object \Spatie\SchemaOrg\Schema
   */
  private $schema;

  /**
   * Node\Artwork\JsonLdFormat constructor.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Artwork $artwork
   *   The wrapped entity.
   */
  public function __construct(Artwork $artwork) {
    parent::__construct();
    $this->schema = Schema::visualArtwork()
      ->name($artwork->getTitle())
      ->url($artwork->getPathAlias())
      ->description($artwork->getDefaultDescription())
      ->artMedium($artwork->getMediums())
      ->artform($artwork->getClassifications())
      ->sameAs(sprintf(
        'https://edan.si.edu/saam/id/object/%s',
        $artwork->getAccessionNumber()
      ));
  }

  /**
   * @return string;
   */
  public function transform() {
    return $this->schema;
  }

}
