<?php

namespace Drupal\viola_content\Formats\Node\Page;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Page;

final class SearchIndexFormat extends FormatBase {

  /**
   * @var array
   */
  private $document = [];

  /**
   * Node\Page\SearchIndexFormat constructor.
   *   Format a Node for elasticsearch indexing.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Page $page
   *   The wrapped entity.
   */
  public function __construct(Page $page) {
    parent::__construct();
    $this->document['@timestamp'] = time();
    $this->document['contents'] = $page->getRenderedContents($page->getEntity());
    $this->document['path'] = $page->getPathAlias();
    $this->document['subtitle'] = $page->getSubtitle();
    $this->document['summary'] = $page->getSummary();
    $this->document['title'] = $page->getTitle();
    $this->document['type'] = $page->getBundle();
  }

  /**
   * @return array;
   */
  public function transform() {
    return $this->document;
  }

}
