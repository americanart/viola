<?php

namespace Drupal\viola_content\Formats\Node\Exhibition;

use Drupal\viola_content\Formats\FormatBase;
use Spatie\SchemaOrg\Schema;
use Drupal\viola_content\WrappedEntities\Node\Exhibition;

final class JsonLdFormat extends FormatBase {

  /**
   * @var object \Spatie\SchemaOrg\Schema
   */
  private $schema;

  /**
   * Node\Exhibition\JsonLdFormat constructor.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Exhibition $exhibition
   *   The wrapped entity.
   */
  public function __construct(Exhibition $exhibition) {
    parent::__construct();

    $this->schema = Schema::exhibitionEvent()
      ->name($exhibition->getTitle())
      ->description($exhibition->getBlurb())
      ->url($exhibition->getPathAlias());
    if ($dateRange = $exhibition->getDateRange()) {
      if (isset($dateRange['start'], $dateRange['end'])) {
        $this->schema->startDate($dateRange['start'])->endDate($dateRange['end']);
      }
    }
  }

  /**
   * @return string;
   */
  public function transform() {
    return $this->schema;
  }

}
