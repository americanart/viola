<?php

namespace Drupal\viola_tms\EventSubscriber;

use Drupal\advancedqueue\Event\AdvancedQueueEvents;
use Drupal\advancedqueue\Event\JobEvent;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber for Advanced queue Jobs "post process" events.
 */
class JobPostProcessEventSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;
  /**
   * Drupal\Core\Logger\LoggerChannelInterface definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * JobPostProcessEventSubscriber constructor.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The Drupal default database connection.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   The Drupal logger channel factory.
   */
  public function __construct(
    Connection $connection,
    LoggerChannelFactory $logger_factory
  ) {
    $this->database = $connection;
    $this->logger = $logger_factory->get('viola_tms');
  }

  /**
   * Method that is triggered on the response event.
   *
   * @param \Drupal\advancedqueue\Event\JobEvent $event
   *
   * @return bool
   *   Successful response to event.
   *
   * @throws \Exception
   */
  public function onRespond(JobEvent $event) {
    $job = $event->getJob();
    $state = $job->getState();
    $payload = $job->getPayload();

    // Don't spawn any additional Jobs from a deleted Job.
    if (
      ($job->getType() == 'collection_import_job' || $job->getType() == 'trumba_import_job')
      && $payload['action'] == 'delete') {
      return FALSE;
    }

    // Delete "skipped" jobs record from the database.
    if ($state == 'success' && strtolower($job->getMessage()) == 'skipped') {
      return $this->deleteJob($job->getId()) > 0 ? TRUE : FALSE;
    }

    // TODO: Decide where/how the color extraction should run. (This seems like a bad place).
    // Queue a new Colors Job when new Artwork is imported.
//    if ($state == 'success'
//      && $job->getType() == 'collection_import_job'
//      && $payload['entity_type'] == 'node'
//      && $payload['bundle'] == 'artwork') {
//      if (isset($payload['data']['field_collection_media_assets']) && $payload['data']['field_collection_media_assets']) {
//        $data['field_object_id'] = $payload['data']['field_object_id'];
//        QueueManager::queueJob('colors_job', 'update', 'field_storage', 'field_colors', $data);
//      }
//    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AdvancedQueueEvents::POST_PROCESS][] = ['onRespond'];
    return $events;
  }

  /**
   * Delete a specific job.
   *
   * @param $id
   *   The job id.
   *
   * @return int
   *   The number of rows deleted.
   */
  public function deleteJob($id) {
    return $this->database->delete('advancedqueue')->condition('job_id', $id)->execute();
  }

}
