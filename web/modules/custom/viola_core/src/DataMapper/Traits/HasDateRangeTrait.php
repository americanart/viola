<?php

namespace Drupal\viola_core\DataMapper\Traits;

/**
 * Trait adding functionality for content with a Date Range field.
 */
trait HasDateRangeTrait {

  /**
   * Create a date range array given start and end years.
   *
   * @param string $begin_year
   *   A beginning date string.
   * @param string $end_year
   *  An ending date string.
   * @return array
   */
  private function getDateRangeValuesFromYears($begin_year, $end_year, $timezone = NULL) {
    $range = [];
    $begin = $end = FALSE;
    if (is_numeric($begin_year) && strlen($begin_year) == 4) {
      $begin = \DateTime::createFromFormat('Y-m-d', $begin_year . '-01-01', $timezone);
    }
    if (is_numeric($end_year) && strlen($end_year) == 4) {
      $end = \DateTime::createFromFormat('Y-m-d', $end_year . '-12-31', $timezone);
    }
    if ($begin && $end) {
      $range['value'] = $begin->format('Y-m-d');
      $range['end_value'] = $end->format('Y-m-d');
    }
    return $range;
  }

}
