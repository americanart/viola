<?php

namespace Drupal\viola_core\Color;

/**
 * A Color Service.
 */
interface ColorServiceInterface {

  /**
   * Get representative hex colors from an image file.
   *
   * @param string $file
   *   The file url.
   * @param int $num
   *   The number of colors to extract.
   *
   * @return array
   *   List of hex colors codes.
   */
  public function getColorsFromFile($file, $num);

}
