<?php

namespace Drupal\viola_trumba\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_core\DataReader\JsonDataReader;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaTrumbaCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var int
   */
  public $queued;

  public function __construct(LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Viola Trumba commands.
   *
   * @param array $ids
   *   Comma-separated list of specific Trumba event id(s) to import.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option id
   *   Specific content id(s) to import.
   * @option limit
   *   Limit number of items to import.
   * @option offset
   *   Number of items to offset.
   * @option url
   *   URL to identifiers import data file.
   * @option force
   *   Force import, bypassing the checksum comparison.
   * @usage viola-trumba:import
   *
   * @command viola-trumba:import
   */
  public function import($ids = '', $options = [
    'limit' => 55000,
    'offset' => 0,
    'url' => 'http://www.trumba.com/calendars/American_Art_Museum.json',
    'force' => FALSE
  ]) {
    $this->logger()->notice(dt("Running viola-trumba:import ..."));
    // Retrieve and queue import results for processing.
    $reader = new JsonDataReader(
      'https://',
      $options['url'],
      $options['limit'],
      $options['offset']
    );
    // Filter results by supplied ids.
    $ids = StringUtils::csvToArray($ids) ?? [];
    $results = $reader->read()->filter('eventID', $ids)->setLimit($options['limit'])->getResults();
    if ($results) {
      $batch = [
        'title' => dt('Queue import of trumba events processing ...'),
        'operations' => [],
        'progress_message' => dt('Queued @current out of @total.'),
        'finished' => 'Drupal\viola_trumba\BatchService::batchFinished',
      ];

      foreach ($results as $result) {
        if ($options['force']) {
          $result['force'] = TRUE;
        }
        // Prepare the operations.
        $batch['operations'][] = [
          'Drupal\viola_trumba\BatchService::batchProcessTrumbaImport', [
            $result,
            dt('Queue import for Event: @id', ['@id' => $result['eventID'],]),
          ],
        ];
      }
      // Add batch operations as new batch sets.
      batch_set($batch);
      // Process the batch sets.
      drush_backend_batch_process();
    }
    else {
      $this->logger()->warning('Nothing to import.');
    }
  }

}
