# Viola Content

Content and data transformation utilities for Viola. This module requires and is organized as suggested by the
[Typed Entity module](https://www.drupal.org/project/typed_entity).

## Repositories

Repository classes in `src/Plugin/TypedRepositories` are the entry point for working with content collections.
The repository allows you to create WrappedEntities from Drupal Entities (Nodes, Media, etc.), and contains all the
applications business logic for performing actions on all content of a particular entity type.

Example:
```php
  /** @var \Drupal\viola_content\Plugin\TypedRepositories\ArtworkRepository $artworkRepository */
  $artworkRepository = \Drupal::service(\Drupal\typed_entity\RepositoryManager::class)->repository('node', 'artwork');
  $allArtworks = $artworkRepository->all();
  // Output the titles.
  /** @var \Drupal\viola_content\WrappedEntities\Artwork $artwork */
  foreach ($allArtworks as $artwork) {
    echo $artwork->getTitle();
  }
```

## Wrapped Entities

Wrapped entities in `src/WrappedEntities` contain act as wrapper around a Drupal Entity, containing all the business
logic for that specific entity type. These are the primary objects to work with when working with Viola content. Most
WrappedEntities are extended as a `TranformableWrappedEntity` which allows an Entity to be easily transformed into a
particular data format.

## Formats

Formats in `src/Formats` are classes that transform a WrappedEntity into some format. For example: an array, an array formatted
for indexing in a search index, a JSON-LD string, etc.

Example:
```php
  $node = \Drupal\node\Entity\Node::load(1);
  /** @var \Drupal\viola_content\WrappedEntities\Artwork $artwork */
  $artwork = \Drupal::service(\Drupal\typed_entity\RepositoryManager::class)->wrap($node);
  // Transform the artwork into it's JSON-LD representation
  $json = $artwork->transformInto('json_ld');
  dump($json);
```

## API endpoints

- `/api/node/{bundle}/{uuid}`: Retrieve JSON data for a Node given bundle name and UUID.
- `/services/metadata/node/{bundle}/{uuid}`: Retrieve JSON-LD data for a Node given bundle name and UUID.
