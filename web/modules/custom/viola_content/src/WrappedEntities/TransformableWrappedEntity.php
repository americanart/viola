<?php

namespace Drupal\viola_content\WrappedEntities;

use Drupal\typed_entity\WrappedEntities\WrappedEntityBase;

/**
 * Base class for transformable WrappedEntity subclasses.
 */
class TransformableWrappedEntity extends WrappedEntityBase implements TransformableWrappedEntityInterface {

  /**
   * Transform an Entity into a particular format.
   * @param string $format
   *
   * @return mixed
   */
  public function transformInto($format = '') {
    try {
      $className = (new \ReflectionClass($this))->getShortName();
      $type = ucfirst($this->getEntity()->getEntityTypeId());
      switch (mb_strtolower($format)) {
        case 'search_index':
          $classPath = sprintf(
            '\Drupal\viola_content\Formats\%s\%s\SearchIndexFormat',
            $type,
            $className
          );
          break;
        case 'json_ld':
          $classPath = sprintf(
            '\Drupal\viola_content\Formats\%s\%s\JsonLdFormat',
            $type,
            $className
          );
          break;
        case 'default':
          $classPath = sprintf(
            '\Drupal\viola_content\Formats\%s\%s\DefaultFormat',
            $type,
            $className
          );
          break;
        default:
          return $this->getEntity()->toArray();
      }
      $format = new $classPath($this);
      return $format->transform();
    }
    catch (\Exception $exception) {
      \Drupal::logger('viola_content')->error($exception->getMessage());
    }
  }

}
