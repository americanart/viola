<?php

namespace Drupal\viola_content\WrappedEntities\Node;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\TermInterface;
use Drupal\viola_content\WrappedEntities\Traits\HasDateRangeTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The wrapped entity for the exhibition content type.
 */
class Exhibition extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;
  use HasDateRangeTrait;
  use HasCommonEntityFieldsTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Web Typography utilities service.
   *
   * @var \Drupal\viola_core\Typography\TypographyService
   */
  protected $typography;

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   */
  public function __construct(EntityInterface $entity, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity);
    $this->entityTypeManager = $entityTypeManager;
    $this->typography = \Drupal::service('viola.typography');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityInterface $entity): self {
    return new static(
      $entity,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @return array
   */
  public function getDateRange() {
    // TODO: retrieve Date range from Paragraphs reference in `field_venues`
    return $this->getDateRangeFromStrings(
      $this->getEntity()->get('field_schedule')->value,
      $this->getEntity()->get('field_schedule')->end_value
    );
  }

  /**
   * @return string
   */
  public function getBlurb() {
    return $this->getEntity()->get('field_blurb')->value ?? '';
  }

  /**
   * @return string
   */
  public function getSummary() {
    return $this->getEntity()->get('field_summary')->value ?? '';
  }

  /**
   * @return array
   */
  public function getVenues() {
    $venues = [];
    $paragraphs = $this->getReferencedEntities(
      $this->getEntity(),
      'field_venues'
    );
    foreach ($paragraphs as $paragraph) {
      if ($paragraph instanceof Paragraph) {
        $terms = $this->getReferencedEntities($paragraph, 'field_venue');
        $term = reset($terms);
        if ($term instanceof TermInterface) {
          $venue = [
            'name' => $term->label(),
            'tid' => $term->id(),
            'path' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])->toString(),
            'address' => $term->get('field_address')->getValue(),
            'dateRange' => [
              'start' => $paragraph->get('field_schedule')->value,
              'end' => $paragraph->get('field_schedule')->end_value,
            ],
            'description' =>  $term->get('description')->value
          ];
          $venues[] = $venue;
        }
      }
    }
    return $venues;
  }

}
