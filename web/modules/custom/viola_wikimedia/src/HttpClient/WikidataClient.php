<?php

namespace Drupal\viola_wikimedia\HttpClient;

use Drupal\node\Entity\Node;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A class to pull data about a topic from Wikidata.
 *
 * @see: https://www.wikidata.org/wiki/Special:ApiSandbox
 */
class WikidataClient {

  /**
   * An http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Depicted entity or subject matter.
   * @see: https://www.wikidata.org/wiki/Property:P180
   */
  const DEPICTS_PROPERTY = 'P180';
  /**
   * Creative work's genre.
   * @see: https://www.wikidata.org/wiki/Property:P136
   */
  const GENRE_PROPERTY = 'P136';
  /**
   * A person or idea that influenced another Person.
   * @see: https://www.wikidata.org/wiki/Property:P737
   */
  const INFLUENCED_BY_PROPERTY = 'P737';
  /**
   * Image representing a given entity.
   * @see: https://www.wikidata.org/wiki/Property:P18
   */
  const IMAGE_PROPERTY = 'P18';
  /**
   * Subject's spouse (husband, wife, partner).
   * @see: https://www.wikidata.org/wiki/Property:P26
   */
  const SPOUSE_PROPERTY = 'P26';
  /**
   * Artistic movement or scene associated with a subject.
   * @see: https://www.wikidata.org/wiki/Property:P135
   */
  const MOVEMENT_PROPERTY = 'P135';

  /**
   * Construct a Wikipedia client object.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The Guzzle client.
   */
  public function __construct(ClientInterface $httpClient) {
    $this->http_client = $httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * Helper to get the Wikidata entity data formatted for import.
   *
   * @param string $wikidata_id
   *   The Wikidata "Q" ID of the entity to retrieve.
   *
   * @return array
   *
   * @see https://www.wikidata.org/wiki/Property:P737
   */
  public function getData($wikidata_id) {
    $results = [];
    $data = $this->getEntityData($wikidata_id);
    if (isset($data['claims'])) {
      // Get values of "depicts" property.
      if (isset($data['claims'][self::DEPICTS_PROPERTY])) {
        $labels = [];
        $ids = [];
        foreach ($data['claims'][self::DEPICTS_PROPERTY] as $claim) {
          $ids[] = $claim['mainsnak']['datavalue']['value']['id'];
        }
        if ($ids) {
          $entities = $this->getEntities($ids, ['labels'])['entities'];
          foreach($entities as $entity) {
            $labels[] = $entity['labels']['en']['value'];
          }
          $results['depicts'] = $labels;
        }
      }
      // Get values of "movement" property.
      if (isset($data['claims'][self::MOVEMENT_PROPERTY])) {
        $labels = [];
        $ids = [];
        foreach ($data['claims'][self::MOVEMENT_PROPERTY] as $claim) {
          $ids[] = $claim['mainsnak']['datavalue']['value']['id'];
        }
        if ($ids) {
          $entities = $this->getEntities($ids, ['labels'])['entities'];
          foreach($entities as $entity) {
            $labels[] = $entity['labels']['en']['value'];
          }
          $results['movement'] = $labels;
        }
      }
      // Get values of "genre" property.
      if (isset($data['claims'][self::GENRE_PROPERTY])) {
        $labels = [];
        $ids = [];
        foreach ($data['claims'][self::GENRE_PROPERTY] as $claim) {
          $ids[] = $claim['mainsnak']['datavalue']['value']['id'];
        }
        if ($ids) {
          $entities = $this->getEntities($ids, ['labels'])['entities'];
          foreach($entities as $entity) {
            $labels[] = $entity['labels']['en']['value'];
          }
          $results['genre'] = $labels;
        }
      }
      // Get values of "influenced_by" property.
      if (isset($data['claims'][self::INFLUENCED_BY_PROPERTY])) {
        $ids = [];
        foreach ($data['claims'][self::INFLUENCED_BY_PROPERTY] as $claim) {
          $ids[] = $claim['mainsnak']['datavalue']['value']['id'];
        }
        // Create a list of people's name and Q id, keyed by Q id.
        $results['influencedBy'] = [];
        $entities = $this->getEntities($ids, ['labels'])['entities'];
        foreach($entities as $key => $entity) {
          $results['influencedBy'][$key] = [
            'wikidata_id' => $key,
            'name' => $entity['labels']['en']['value'],
            'is_permanent_collection' => FALSE,
          ];
        }
        // Update influencedBy results with Viola content data (if available).
        $query = \Drupal::entityQuery('node')
          ->condition('status', 1)
          ->condition('type', 'creator')
          ->condition('field_wikidata_id', $ids, 'IN');
        $nids = $query->execute();
        foreach (array_values($nids) as $nid) {
          $node = Node::load($nid);
          $key = $node->get('field_wikidata_id')->value;
          $results['influencedBy'][$key]['uuid'] = $node->uuid();
          $results['influencedBy'][$key]['name'] = $node->label();
          $results['influencedBy'][$key]['constituent_id'] = $node->get('field_constituent_id')->value;
          $results['influencedBy'][$key]['is_permanent_collection'] = $node->get('field_is_permanent_collection')->value ? TRUE : FALSE;
          $results['influencedBy'][$key]['path'] = \Drupal::service('path_alias.manager')
            ->getAliasByPath('/node/'. $node->id());
        }
      }
      // Get values of "spouse" property.
      if (isset($data['claims'][self::SPOUSE_PROPERTY])) {
        $ids = [];
        foreach ($data['claims'][self::SPOUSE_PROPERTY] as $claim) {
          $ids[] = $claim['mainsnak']['datavalue']['value']['id'];
        }
        // Create a list of people's name and Q id, keyed by Q id.
        $results['spouse'] = [];
        $entities = $this->getEntities($ids, ['labels'])['entities'];
        foreach($entities as $key => $entity) {
          $results['spouse'][$key] = [
            'wikidata_id' => $key,
            'name' => $entity['labels']['en']['value'],
            'is_permanent_collection' => FALSE,
          ];
        }
        // Update Spouse results with Viola content data (if available).
        $query = \Drupal::entityQuery('node')
          ->condition('status', 1)
          ->condition('type', 'creator')
          ->condition('field_wikidata_id', $ids, 'IN');
        $nids = $query->execute();
        foreach (array_values($nids) as $nid) {
          $node = Node::load($nid);
          $key = $node->get('field_wikidata_id')->value;
          $results['spouse'][$key]['uuid'] = $node->uuid();
          $results['spouse'][$key]['name'] = $node->label();
          $results['spouse'][$key]['constituent_id'] = $node->get('field_constituent_id')->value;
          $results['spouse'][$key]['is_permanent_collection'] = $node->get('field_is_permanent_collection')->value ? TRUE : FALSE;
          $results['spouse'][$key]['path'] = \Drupal::service('path_alias.manager')
            ->getAliasByPath('/node/'. $node->id());
        }
      }
      // Get value of "image" property.
      if ($images = $this->getImages($wikidata_id)) {
        $results['images'] = $images;
      }
    }

    return $results;
  }

  /**
   * Retrieve and decode the response.
   *
   * @param string $wikidata_id
   *   The Wikidata "Q" ID of the entity to retrieve.
   *
   * @return array
   *   A decoded array of data.
   *
   * @see https://www.mediawiki.org/wiki/API:Query
   * @see http://docs.guzzlephp.org/en/latest/quickstart.html
   */
  public function getEntityData($wikidata_id) {
    $uri = sprintf('https://www.wikidata.org/wiki/Special:EntityData/%s.json', $wikidata_id);
    try {
      if ($response = $this->http_client->request('GET', $uri)) {
        if ($data = $response->getBody()->getContents()) {
          $data = json_decode($data, TRUE);
          if (array_key_exists('entities', $data) && array_key_exists($wikidata_id, $data['entities'])) {
            return $data['entities'][$wikidata_id];
          }
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('wikidata_client', $e);
    }
    return [];
  }

  /**
   * Retrieve response for an Entity's specific property.
   *
   * @param array $entities
   *   The Wikidata entity IDs.
   * @param array $props
   *   The props filter.
   *
   * @return array
   *   A decoded array of data.
   */
  public function getEntities($ids = [], $props = []) {
    $uri = 'https://www.wikidata.org/w/api.php';
    $query = [
      'action' => 'wbgetentities',
      'format' => 'json',
      'ids' => implode('|', $ids),
      'props' => implode('|', $props),
      'languages' => 'en'
    ];
    $options = ['query' => $query, 'http_errors' => FALSE];
    try {
      if ($response = $this->http_client->request('GET', $uri, $options)) {
        if ($data = $response->getBody()->getContents()) {
          return json_decode($data, TRUE);
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('wikidata_client', $e);
    }
    return [];
  }

  /**
   * Retrieve response for an Entity's specific property.
   *
   * @param string $wikidata_id
   *   The Wikidata "Q" ID of the entity to retrieve.
   * @param string $property
   *   The Wikidata "P" property ID.
   * @param array $props
   *   The props filter.
   *
   * @return array
   *   A decoded array of data.
   *
   * @see https://www.mediawiki.org/wiki/API:Query
   */
  public function getProperty($wikidata_id, $property, $props = []) {
    $uri = 'https://www.wikidata.org/w/api.php';
    $query = [
      'action' => 'wbgetclaims',
      'format' => 'json',
      'entity' => $wikidata_id,
      'property' => $property,
      'props' => implode('|', $props),
    ];
    $options = ['query' => $query, 'http_errors' => FALSE];
    try {
      if ($response = $this->http_client->request('GET', $uri, $options)) {
        if ($data = $response->getBody()->getContents()) {
          $data = json_decode($data, TRUE);
          if (array_key_exists('claims', $data) && array_key_exists($property, $data['claims'])) {
            return $data['claims'][$property];
          }
        }
      }
    }
    catch (RequestException $e) {
      watchdog_exception('wikidata_client', $e);
    }
    return [];
  }

  /**
   * Helper to get the Wikidata entity page ID.
   *
   * @param array $data
   *   A wikidata entity.
   *
   * @return string
   *
   */
  public function getPageId($data) {
    if (array_key_exists('pageid', $data)) {
      return $data['pageid'];
    }
    return '';
  }

  /**
   * Helper to get the Wikidata entity images formatted for import.
   *
   * @param string $wikidata_id
   *   The Wikidata "Q" ID of the entity to retrieve.
   *
   * @return array
   *
   * @see: https://commons.wikimedia.org/wiki/Commons:Reusing_content_outside_Wikimedia/technical
   * @see: https://www.mediawiki.org/wiki/API:Imageinfo
   * @see: https://www.wikidata.org/wiki/Property:P18
   */
  public function getImages($wikidata_id) {
      $results = [];
      $data = $this->getProperty($wikidata_id, self::IMAGE_PROPERTY);
      foreach($data as $imageData) {
        if (isset($imageData['mainsnak']) && $imageData['mainsnak']['datatype'] === 'commonsMedia') {
          $filename = str_replace(' ', '_', $imageData['mainsnak']['datavalue']['value']);
          $uri = 'https://www.wikidata.org/w/api.php';
          $props = ['imageinfo'];
          $iiprop = ['extmetadata', 'url', 'mime'];
          $query = [
            'action' => 'query',
            'format' => 'json',
            'prop' => implode('|', $props),
            'iiprop' => implode('|', $iiprop),
            'redirects' => 1,
            'titles' => sprintf('File:%s', $filename),
          ];
          $options = ['query' => $query, 'http_errors' => FALSE];
          // Get the image information.
          $image = [];
          try {
            // Example: https://www.wikidata.org/w/api.php?action=query&prop=imageinfo&iiprop=extmetadata|url|mime&titles=File:William%20H.%20Johnson.JPG&format=json
            if ($response = $this->http_client->request('GET', $uri, $options)) {
              if ($data = $response->getBody()->getContents()) {
                $data = json_decode($data, TRUE);
                if (array_key_exists('query', $data) && array_key_exists('pages', $data['query'])) {
                  foreach($data['query']['pages'] as $page) {
                    $info = reset($page['imageinfo']);
                    $image['url'] = $info['url'];
                    $image['mime'] = $info['mime'];
                    // Build the dynamic derivative image urls.
                    $base = 'https://commons.wikimedia.org/wiki/Special:FilePath';
                    $parts = pathinfo(parse_url($image['url'], PHP_URL_PATH));
                    $filename = sprintf('%s.%s', $parts['filename'], $parts['extension']);
                    $image['thumbnails'] = [
                      'xsmall' => sprintf('%s/%s?width=%spx', $base, $filename, 320),
                      'small' => sprintf('%s/%s?width=%spx', $base, $filename, 640),
                      'medium' => sprintf('%s/%s?width=%spx', $base, $filename, 960),
                      'large' => sprintf('%s/%s?width=%spx', $base, $filename, 1200),
                    ];
                    // Get image metadata.
                    if (isset($info['extmetadata'])) {
                      if (!empty($info['extmetadata']['ImageDescription']['value'])) {
                        $image['description'] = strip_tags($info['extmetadata']['ImageDescription']['value']);
                      }
                      if (!empty($info['extmetadata']['UsageTerms']['value'])) {
                        $image['usageRights'] = $info['extmetadata']['UsageTerms']['value'];
                      }
                      if (!empty($info['extmetadata']['Credit']['value'])) {
                        $image['dateTime'] = strip_tags($info['extmetadata']['Credit']['value']);
                      }
                      if (!empty($info['extmetadata']['LicenseShortName']['value'])) {
                        $image['license'] = $info['extmetadata']['LicenseShortName']['value'];
                      }
                      if (!empty($info['extmetadata']['DateTime']['value'])) {
                        $image['dateTime'] = $info['extmetadata']['DateTime']['value'];
                      }
                      if (!empty($info['extmetadata']['AttributionRequired']['value'])) {
                        $image['attributionRequired'] = mb_strtolower($info['extmetadata']['AttributionRequired']['value']);
                      }
                      if (!empty($info['extmetadata']['Copyrighted']['value'])) {
                        $image['copyrighted'] = mb_strtolower($info['extmetadata']['Copyrighted']['value']);
                      }
                    }
                  }
                }
              }
            }
          }
          catch (RequestException $e) {
            watchdog_exception('wikidata_client', $e);
          }
          array_push($results, $image);
        }
      }
      return $results;
  }

}
