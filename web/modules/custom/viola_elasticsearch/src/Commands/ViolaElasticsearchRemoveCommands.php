<?php

namespace Drupal\viola_elasticsearch\Commands;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\viola_elasticsearch\Elastic\Index;
use Drupal\viola_elasticsearch\Elastic\Indexer;
use Drush\Commands\DrushCommands;
use Drush\Utils\StringUtils;

class ViolaElasticsearchRemoveCommands extends DrushCommands {

  /**
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Indexer
   */
  protected $indexer;

  /**
   * @var \Drupal\viola_elasticsearch\Elastic\Index
   */
  protected $index;

  /**
   * Class constructor.
   *
   * @param \Drupal\viola_elasticsearch\Elastic\Indexer $indexer
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   */
  public function __construct(Indexer $indexer, LoggerChannelFactoryInterface $loggerChannelFactory) {
    parent::__construct();
    $this->indexer = $indexer;
    $this->loggerChannelFactory = $loggerChannelFactory;
    $this->index = new Index();
  }

  /**
   * Remove documents from the Elasticsearch index.
   *
   * @param string $type
   *   The content type to remove from index.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option index
   *   The index name to perform actions upon.
   * @option limit
   *   Limit number of items to remove.
   * @option offset
   *   Number of items to offset.
   * @option size
   *   The batch size when performing bulk operations.
   * @option ids
   *   A comma-separated list of specific Node ids to remove.
   * @usage viola-elasticsearch:index
   *
   * @command viola-elasticsearch:remove
   * @aliases viola-es:remove
   */
  public function index($type = '', $options = [
    'index' => 'catalog',
    'limit' => 100000,
    'offset' => 0,
    'size' => 100,
    'ids' => '',
  ]) {
    $this->logger()->notice(dt("Running viola-elasticsearch:remove ..."));
    $indexName = $options['index'] ?? $this->index->getIndexName();
    // Set the bundles if supplied as an option or use all allowed bundles as default.
    $bundles = $this->indexer->nodeBundles;
    $bundle = mb_strtolower($type);
    if ($bundle && in_array($bundle, $this->indexer->nodeBundles)) {
      $bundles = [$bundle];
    }
    // Perform action on specific ids.
    $ids = StringUtils::csvToArray($options['ids']) ?? [];
    if ($ids) {
      $this->indexer->bulkOperation($ids, 'delete', $indexName);
      $this->logger()->success(
        dt('Queued @count item(s) for removal from the Elasticsearch index.', ['@count' => count($ids)])
      );
    }
    else {
      // Perform action on all Nodes by bundle.
      foreach ($bundles as $bundle) {
        $this->logger()->notice(dt('Indexing @bundle content ...', ['@bundle' => $bundle]));
        $nids = $this->indexer->queryNodes($bundle, $options['offset'], $options['limit']);
        $total = count($nids);
        $chunks = array_chunk($nids, $options['size']);
        unset($nids);
        foreach ($chunks as $key => $nids) {
          $this->indexer->bulkOperation($nids, 'index', $indexName);
          unset($chunks[$key]);
        }
        $this->logger()->success(
          dt('Queued @count item(s) for removal from the Elasticsearch index.', ['@count' => $total])
        );
      }
    }
  }

  /**
   * Remove custom routes from the index.
   *
   * @option index
   *   The index name to perform actions upon.
   * @usage viola-elasticsearch:remove-routes
   *
   * @command viola-elasticsearch:remove-routes
   * @aliases viola-es:remove-routes
   */
  public function removeRoutes($options = ['index' => 'catalog']) {
    $this->logger()->notice(dt("viola-elasticsearch:remove-routes ..."));
    $indexName = $options['index'] ?? $this->index->getIndexName();
    // Custom routes we want to index.
    $routes = [
      [
        'title' => 'Art + Artists',
        'uuid' => 'efd285c5-381d-44f2-be3c-66a8c22d5f4b',
      ],
      [
        'title' => 'Events',
        'uuid' => 'd19ae1f7-b9a1-420a-af42-b1d2d528f104',
      ],
      [
        'title' => 'Exhibitions',
        'uuid' => '812d362d-4d8c-41b5-80cc-1cbb6272045b',
      ],
    ];
    $count = 0;
    foreach ($routes as $route) {
      if ($route['uuid']) {
        // The bulk operation index target and settings.
        $params = [
          'index' => $indexName,
          'refresh' => TRUE,
        ];
        // The action for this item.
        $params['body'][] = [
          'delete' => [
            '_id' => $route['uuid'],
          ]
        ];
        $this->indexer->queueBulkJob($params);
        $count++;
      }
    }
    $this->logger()->success(
      dt('Queued @count route(s) for removal from the Elasticsearch index.', ['@count' => $count])
    );
  }

}
