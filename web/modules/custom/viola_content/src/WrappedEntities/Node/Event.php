<?php

namespace Drupal\viola_content\WrappedEntities\Node;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\viola_content\WrappedEntities\Traits\HasDateRangeTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Drupal\viola_core\Utils\Markup;
use Drupal\viola_core\Utils\Strings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The wrapped entity for the event content type.
 */
class Event extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;
  use HasDateRangeTrait;
  use HasCommonEntityFieldsTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Web Typography utilities service.
   *
   * @var \Drupal\viola_core\Typography\TypographyService
   */
  protected $typography;
  /**
   * JSON decoded value of the JSON data field.
   * @var array
   */
  public $data;

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   */
  public function __construct(EntityInterface $entity, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity);
    $this->entityTypeManager = $entityTypeManager;
    $this->typography = \Drupal::service('viola.typography');
    $this->data = JSON::decode($this->getEntity()->get('field_data')->value);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityInterface $entity): self {
    return new static(
      $entity,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Return a "custom field" value from the nested data array.
   * @param $label
   *
   * @return string
   */
  private function getCustomFieldValue($label) {
    if (isset($this->data['customFields'])) {
      $filtered = array_filter($this->data['customFields'], function ($item) use ($label) {
        return ($item['label'] == $label);
      });
      if ($filtered) {
        return trim(reset($filtered)['value']);
      }
    }
    return '';
  }

  /**
   * @return string
   */
  public function getAccessibility() {
    return $this->getCustomFieldValue('Accessibility');
  }

  /**
   * @return string
   */
  public function getActionsUrl() {
    if (isset($this->data['eventActionUrl'])) {
      return trim($this->data['eventActionUrl']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getCategories() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_event_series'
    );
  }

  /**
   * @return string
   */
  public function getCategoriesDescription() {
    $value = $this->getCustomFieldValue('Categories');
    return Strings::decodeSpecialChars($value);
  }

  /**
   * @return string
   */
  public function getCoSponsor() {
    $value = $this->getCustomFieldValue('Co-sponsor');
    return Strings::decodeSpecialChars($value);
  }

  /**
   * @return string
   */
  public function getCostDescription() {
    return $this->getCustomFieldValue('Cost');
  }

  /**
   * @return string
   */
  public function getDateDescription() {
    if (isset($this->data['dateTimeFormatted'])) {
      return Strings::decodeSpecialChars($this->data['dateTimeFormatted']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getDateRange() {
    return $this->getDateRangeFromStrings(
      $this->getEntity()->get('field_event_schedule')->value,
      $this->getEntity()->get('field_event_schedule')->end_value
    );
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->getEntity()->get('field_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDwellTime() {
    $value = $this->getCustomFieldValue('Dwell Time');
    return Strings::decodeSpecialChars($value);
  }

  /**
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]
   */
  public function getImages() {
    return $this->wrapReferencedEntites($this->getEntity(), 'field_images');
  }

  /**
   * @return string
   */
  public function getLocationDescription() {
    $value = $this->getCustomFieldValue('Event Location');
    return Strings::decodeSpecialChars($value);
  }

  /**
   * @return string
   */
  public function getRegistrationUrl() {
    if (isset($this->data['signUpUrl'])) {
      return trim($this->data['signUpUrl']);
    }
    return '';
  }

  /**
   * @return string
   */
  public function getSummary() {
    return $this->getEntity()->get('field_summary')->value;
  }

  /**
   * @return string
   */
  public function getTicketsUrl() {
    $value = $this->getCustomFieldValue('Get Tickets');
    return Markup::getUrl($value) ?: '';
  }

  /**
   * @return string
   */
  public function getTrumbaEventId() {
    return $this->getEntity()->get('field_trumba_event_id')->value ?? '';
  }

  /**
   * @return string
   */
  public function getVenue() {
    $venues = $this->getReferencedTaxonomyTermLabels($this->getEntity(), 'field_venue');
    return reset($venues);
  }

  /**
   * @return string
   */
  public function getWebcastUrl() {
    $value = $this->getCustomFieldValue('Webcast');
    return Markup::getUrl($value) ?: '';
  }

}
