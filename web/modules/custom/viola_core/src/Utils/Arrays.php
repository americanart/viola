<?php

namespace Drupal\viola_core\Utils;

use Drupal\viola_core\Utils\Strings;

class Arrays {

    /**
     * Flatten a multi-dimensional array into a single level.
     *
     * @param  array  $array
     * @param  int  $depth
     * @return array
     */
    public static function flatten($array, $depth = INF) {
        $result = [];
        foreach ($array as $item) {
            if (!is_array($item)) {
                $result[] = $item;
            } else {
                $values = $depth === 1
                    ? array_values($item)
                    : static::flatten($item, $depth - 1);
                foreach ($values as $value) {
                    $result[] = $value;
                }
            }
        }
        return $result;
    }

    /**
     * Get a specific item from array or $default if item is not set.
     *
     * @param  array            $arr
     * @param  string|int|array $key     one or more keys
     * @param  null             $default
     * @return mixed
     */
    public static function get(array $arr, $key, $default = null) {
        foreach (is_array($key) ? $key : [$key] as $k) {
            if (is_array($arr) && array_key_exists($k, $arr)) {
                $arr = $arr[$k];
            } else {
                return $default;
            }
        }
        return $arr;
    }

    /**
     * Determines if an array is associative.
     *
     * @param  array $array
     * @return bool
     */
    public static function isAssoc(array $array): bool {
        $keys = array_keys($array);
        return array_keys($keys) !== $keys;
    }

}
