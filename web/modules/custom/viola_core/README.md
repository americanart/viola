# Viola Core

Core functionality common to all custom Viola modules

## Services

- `viola.color` Color utilities like extracting colors from an image, and assigning a "color" Taxonomy Term.
- `viola.typography` Apply typography fixes for text.
- `viola.email` Send an email using Drupal API

## Classes

### Queue Manager

The Queue Manager (`src/Queue/QueueManager`) queues a single job with parameters to create a job with a standard payload
format.

The Queue Manager should be called statically. For example:
```php
// Queue a "Node Import Job" for artworks.
\Drupal\viola_core\Queue\QueueManager::queueJob('collection_import_job', 'create', 'node', 'artwork', $data);
```

### DataMappers

Data Access classes (`src/DataMapper`) map import data to a format compatible with Drupal Entities. This allows other
content utilities to act on the data provided by a Data Mapper without having to have any information about the
data source.

Use the DataMapperFactory to instantiate a new DataMapper object. For example:
```php
// Create a data mapper for TMS Artwork import data.
$mapper = \Drupal\viola_core\DataMapper\DataMapperFactory::create('viola_tms', 'artwork', $import_data);
$data = $mapper->getData();
```


### DataReader

Utility classes (`src/DataReader`) for retrieving and manipulating local and remote data files.

Example reading a local JSON file.
```php
use Drupal\viola_core\DataReader\JsonDataReader;
$reader = new JsonDataReader('file://', '/path/to/example.json');
$data = $reader->read()->getResults();
```

### DataWriter

Utility classes (`src/DataWriter`) for writing to a file.

Example using JsonCollectionStreamWriter to create a JSON collection from PHP objects/variables.
```php
use Drupal\viola_core\DataWriter\JsonCollectionStreamWriter;
$items = [
  ['id' => 1],
  ['id' => 2],
  ['id' => 3],
];
$writer = new JsonCollectionStreamWriter('path/to/example.json');
foreach($items as $item) {
  $writer->add($item);
}
$writer->close();
```
### SystemInfo

A Singleton that gets information about the Drupal system. Example:

```php
$info = \Drupal\viola_core\SystemInfo::getInstance();
$content_types = $info::getAllContentTypes();
```

### Constants

Constants used across Viola modules. Example:

```php
$types = \Drupal\viola_core\Constants::CLASSIFICATIONS;
```
