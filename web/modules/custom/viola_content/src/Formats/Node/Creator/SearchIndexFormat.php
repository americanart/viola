<?php

namespace Drupal\viola_content\Formats\Node\Creator;

use Drupal\viola_content\Formats\FormatBase;
use Drupal\viola_content\WrappedEntities\Node\Creator;

final class SearchIndexFormat extends FormatBase {

  /**
   * @var array
   */
  private $document = [];

  /**
   * Node\Creator\SearchIndexFormat constructor.
   *   Format a Node for elasticsearch indexing.
   *
   * @param \Drupal\viola_content\WrappedEntities\Node\Creator $creator
   *   The wrapped entity.
   */
  public function __construct(Creator $creator) {
    parent::__construct();
    // Add timestamp.
    $this->document['@timestamp'] = time();
    // Format date range for filtering.
    if ($dateRange = $creator->getDateRange()) {
      if (isset($dateRange['start'], $dateRange['end'])) {
        $this->document['dateRange']['lte'] = $dateRange['end'];
        $this->document['dateRange']['gte'] = $dateRange['start'];
      }
    }
    // Title/Name.
    $this->document['title'] = $creator->getTitle();
    // The document type.
    $this->document['type'] = $creator->getBundle();
    // Path alias.
    $this->document['path'] = $creator->getPathAlias();
    // Ethnicities.
    $this->document['ethnicities'] = $creator->getEthnicities();
    // Gender.
    $this->document['gender'] = $creator->getGender();
    // Groups.
    $this->document['groups'] = $creator->getGroups();
    // Influences.
    $this->document['influences'] = [];
    if ($influences = $creator->getInfluencedBy()) {
      foreach ($influences as $influence) {
        $this->document['influences'][] = $influence['name'];
      }
    }
    // Movements.
    $this->document['movements'] = $creator->getMovements();
    // Nationalities.
    $this->document['nationalities'] = $creator->getNationalities();
    // Occupations.
    $this->document['occupations'] = $creator->getOccupations();
    // Places.
    $this->document['places'] = $creator->getPlaces();
    // Set the summary
    if ($dateDescription = $creator->getDateDescription()) {
      $this->document['summary'] = $dateDescription;
    }
    // Build the full text search contents.
    $contents = '';
    // Description.
    if ($description = $creator->getDescription()) {
      $contents .= PHP_EOL . ' ' . $description;
    }
    // Collection Description.
    if ($collectionDescription = $creator->getCollectionDescription()) {
      $contents .= PHP_EOL . ' ' . $collectionDescription;
    }

  }

  /**
   * @return array;
   */
  public function transform() {
    return $this->document;
  }

}
