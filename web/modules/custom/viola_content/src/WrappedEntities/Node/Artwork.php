<?php

namespace Drupal\viola_content\WrappedEntities\Node;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\typed_entity\RepositoryManager;
use Drupal\viola_content\WrappedEntities\Traits\HasDateRangeTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\Content\SmithsonianImage;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The wrapped entity for the artwork content type.
 */
class Artwork extends TransformableWrappedEntity {

  use HasEntityReferencesTrait;
  use HasDateRangeTrait;
  use HasCommonEntityFieldsTrait;

  const FIELD_IMAGES_NAME = 'field_images';

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  /**
   * Web Typography utilities service.
   *
   * @var \Drupal\viola_core\Typography\TypographyService
   */
  protected $typography;

  /**
   * JSON decoded value of the JSON data field.
   * @var array
   */
  public $data;

  /**
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to wrap.
   */
  public function __construct(EntityInterface $entity, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($entity);
    $this->entityTypeManager = $entityTypeManager;
    $this->typography = \Drupal::service('viola.typography');
    $this->data = JSON::decode($this->getEntity()->get('field_data')->value);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, EntityInterface $entity): self {
    return new static(
      $entity,
      $container->get('entity_type.manager')
    );
  }

  /**
   * @return string
   */
  public function getAccessionNumber() {
    return $this->getEntity()->get('field_accession_number')->value;
  }

  /**
   * @return array
   */
  public function getCitations() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['citations'])) {
      return $this->data['tms']['citations'];
    }
    return  [];
  }

  /**
   * @return string
   */
  public function getCopyright() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['creditLineRepro'])) {
      return trim($this->data['tms']['creditLineRepro']);
    }
    return '';
  }

  /**
   * @return string
   */
  public function getCollection() {
    return $this->getReferencedTaxonomyTermLabel(
      $this->getEntity(),
      'field_collection'
    );
  }

  /**
   * @return string
   */
  public function getCollectionDescription() {
    return $this->getEntity()->get('field_collection_description')->value ?? '';
  }

  /**
   * @return array
   */
  public function getColors() {
    $colors = [];
    $terms = $this->getReferencedEntities($this->getEntity(), 'field_colors');
    /** @var TermInterface $term */
    foreach ($terms as $term) {
      $colors[] = [
        'name' => $term->label(),
        'tid' => $term->id(),
        'path' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])->toString(),
        'englishName' => $term->get('field_english_name')->value,
        'hex' => $term->get('field_hex_color_code')->value,
      ];
    }
    return $colors;
  }

  /**
   * @return array
   */
  public function getClassifications() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_classifications'
    );
  }

  /**
   * @return array
   */
  public function getCreators() {
    $creators = [];
    if ( !empty($this->data['tms']) && !empty($this->data['tms']['artworkConstituentRelationships'])) {
      foreach ($this->data['tms']['artworkConstituentRelationships'] as $relationship) {
        // TODO: How to handle when the same constituent has multiple roles?
        if (isset($relationship['constituentId'])) {
          // TODO use the CreatorRepository to retrieve Constituents.
          $nodes = $this->entityTypeManager->getStorage('node')->loadByProperties([
            'field_constituent_id' => $relationship['constituentId'],
            'type' => 'creator',
            'status' => 1
          ]);
          $collection = \Drupal::service(RepositoryManager::class)->wrapMultiple($nodes);
          foreach ($collection as $item) {
            $creatorData = $item->transformInto('default');
            // Append the fields specific to the relationship with the Artwork.
            $creatorData['creatorRole'] = $relationship['role'];
            $creatorData['order'] = $relationship['displayOrder'];
            $creators[] = $creatorData;
          }
        }
      }
      // Sort by relationship display order.
      usort($creators, function($a, $b){
        return ($a['order'] < $b['order']) ? -1 : 1;
      });
    }
    return $creators;
  }

  /**
   * @return string
   */
  public function getCredit() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['creditLine'])) {
      return trim($this->data['tms']['creditLine']);
    }
    return '';
  }

  /**
   * @return mixed
   */
  public function getData() {
    return $this->data;
  }

  /**
   * @return string
   */
  public function getDateDescription() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['dated'])) {
      return trim($this->data['tms']['dated']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getDateRange() {
    return $this->getDateRangeFromStrings(
      $this->getEntity()->get('field_date_range')->value,
      $this->getEntity()->get('field_date_range')->end_value
    );
  }

  /**
   * @return string
   */
  public function getDefaultDescription() {
    if ($this->getDescription()) {
      return $this->getDescription();
    }
    elseif ($this->getCollectionDescription()) {
      return $this->getCollectionDescription();
    }
    return '';
  }

  /**
   * @return array
   */
  public function getDepictions() {
    if (isset($this->data['wikidata']) && !empty($this->data['wikidata']['depicts'])) {
      return $this->data['wikidata']['depicts'];
    }
    return [];
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->getEntity()->get('field_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getDimensionsDescription() {
    return $this->getEntity()->get('field_dimensions_description')->value ?? '';
  }

  /**
   * @return string
   */
  public function getExhibitionHistory() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['exhibitions'])) {
      return trim($this->data['tms']['exhibitions']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getGenres() {
    if (isset($this->data['wikidata']) && !empty($this->data['wikidata']['genre'])) {
      return $this->data['wikidata']['genre'];
    }
    return [];
  }

  /**
   * @return string
   */
  public function getGuid() {
    if (isset($this->data['tms']) && isset($this->data['tms']['guid']) && !empty($this->data['tms']['guid'])) {
      return trim($this->data['guid']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getImages() {
    // TODO: Separate these "collection" images from images that might be supplied via entity reference field.
    if (isset($this->data['tms']) && !empty($this->data['tms']['images'])) {
      foreach($this->data['tms']['images'] as $item) {
        if ($image = SmithsonianImage::fromProperties($item)) {
          $images[] = $image;
        }
      }
      // Sort images by primary boolean value.
      usort($images, function($a, $b){
        return ($a['primary'] < $b['primary']) ? -1 : 1;
      });
      return $images;
    }
    return [];
  }

  /**
   * @return array
   */
  public function getKeywords() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_keywords'
    );
  }

  /**
   * @return string
   */
  public function getLocationDescription() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['curSiteDescription'])) {
      return trim($this->data['tms']['curSiteDescription']);
    }
    return '';
  }

  /**
   * @return string
   */
  public function getLongTitle() {
    return $this->getEntity()->get('field_long_title')->value ?? '';
  }

  /**
   * @return string
   */
  public function getMarkings() {
    if (isset($this->data['tms']) && isset($this->data['tms']['markings'])) {
      return trim($this->data['tms']['markings']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getMedia() {
    $entities = $this->entity->get('field_media')->referencedEntities();
    $media = [];
    // TODO: convert this to use a WrappedEntity.
//    foreach($entities as $entity) {
//      // Parse the media.
//      if ($entity instanceof MediaInterface) {
//        $classPath = sprintf("\\%s\\%s\\%s",
//          ContentDataBase::getNamespace(),
//          Strings::titleCase($entity->getEntityTypeId()),
//          Strings::titleCase($entity->bundle())
//        );
//        if (class_exists($classPath)) {
//          /** @var ContentDataInterface $contentData */
//          $contentData = $classPath::parse($entity);
//          $media[] = $contentData->toArray();
//        }
//      }
//    }
    return $media;
  }

  /**
   * @return array
   */
  public function getMediums() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_mediums'
    );
  }

  /**
   * @return string
   */
  public function getMediumsDescription() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['medium'])) {
      return trim($this->data['tms']['medium']);
    }
    return '';
  }

  /**
   * @return array
   */
  public function getMovements() {
    if (isset($this->data['wikidata']) && !empty($this->data['wikidata']['movement'])) {
      return $this->data['wikidata']['movement'];
    }
    return [];
  }

  /**
   * @return int
   */
  public function getObjectId() {
    return $this->getEntity()->get('field_object_id')->value;
  }

  /**
   * @return array
   */
  public function getOntology() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['ontology'])) {
      $terms = [];
      foreach ($this->data['tms']['ontology'] as $ontology) {
        $terms[] = $ontology['pathTerm'];
      }
     return $terms;
    }
    return [];
  }

  /**
   * @return bool
   */
  public function isOnView() {
    return (bool) $this->getEntity()->get('field_is_on_view')->value;
  }

  /**
   * @return bool
   */
  public function isOpenAccess() {
    return $this->getUsageStatement() === 'CC0' ? TRUE : FALSE;
  }

  /**
   * @return bool
   */
  public function isNewAcquisition() {
    return (bool) $this->getEntity()->get('field_is_new_acquisition')->value;
  }

  /**
   * @return array
   */
  public function getPeriods() {
    return $this->getReferencedTaxonomyTermLabels(
      $this->getEntity(),
      'field_time_periods'
    );
  }

  /**
   * @return bool
   */
  public function isPermanentCollection() {
    return (bool) $this->getEntity()->get('field_is_permanent_collection')->value;
  }

  /**
   * @return string
   */
  public function getSummary() {
    return $this->getEntity()->get('field_summary')->value;
  }

  /**
   * @return string
   */
  public function getTranslatedTitle() {
    return $this->getEntity()->get('field_translated_title')->value;
  }

  /**
   * @return array
   */
  public function getTranslatedDescriptions() {
    $descriptions = [];
    $paragraphs = $this->getReferencedEntities($this->getEntity(), 'field_translated_descriptions');
    foreach($paragraphs as $paragraph) {
      if ($paragraph instanceof ParagraphInterface) {
        $description['description'] = $paragraph->get('field_description')->value;
        $languageTerms = $paragraph->get('field_language')->referencedEntities();
        /** @var TermInterface $language */
        if ($language = reset($languageTerms)) {
          $description['language'] = $language->label();
          $description['code'] = $language->get('field_language_code')->value;
          $description['nativeName'] = $language->get('field_native_name')->value;
        }
        $descriptions[] = $description;
      }
    }
    return $descriptions;
  }

  /**
   * @return bool
   */
  public function hasUnidentifiedCreator() {
    return (bool) $this->getEntity()->get('field_has_unidentified_creator')->value;
  }

  /**
   * @return array
   */
  public function getUsageRights() {
    $usageTerms = $this->getReferencedEntities($this->getEntity(), 'field_usage_rights');
    $term = reset($usageTerms);
    if ($term instanceof TermInterface) {
      return [
        'name' => $term->label(),
        'tid' => $term->id(),
        'path' => Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()])->toString(),
        'restricted' => $term->label() == 'Restricted (See File)' ? TRUE : FALSE,
      ];
    }
    return [];
  }

  /**
   * @return string
   */
  public function getUsageStatement() {
    if (isset($this->data['tms']) && !empty($this->data['tms']['siUsageStatement'])) {
      return trim($this->data['tms']['siUsageStatement']);
    }
    return '';
  }

}
