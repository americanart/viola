<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class ArtistGlossaryQuery
 *   Build an Elasticsearch DSL Query to get an alphabetic listing of artists by letter,
 *   with aggregated number of artworks.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class ArtistGlossaryQuery extends QueryDslBase {

  public function __construct($params) {
    parent::__construct();
    $this->setStart(0)
      ->setSize(1000)
      ->setSort(['alphabeticalName.keyword']);
    if (isset($params['letter'])) {
      $this->setQuery([
        'prefix' => [
          'alphabeticalName' => $params['letter'],
        ],
      ]);
    }
  }
}
