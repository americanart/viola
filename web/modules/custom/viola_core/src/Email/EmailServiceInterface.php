<?php

namespace Drupal\viola_core\Email;

interface EmailServiceInterface {

  /**
   * Sends an email message.
   *
   * @param string $key
   *   A key to identify the email sent, necessary for altering an email.
   * @param string $recipients
   *   A comma separated list of emails to send to.
   * @param string $subject
   *   The email subject. plain text.
   * @param string $message
   *   The email body. HTML or plain text.
   *
   * @return boolean
   */
  public function send($key, $recipients, $subject, $message);

}
