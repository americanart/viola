<?php

namespace Drupal\viola_content\WrappedEntities\Media;

use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\viola_content\WrappedEntities\Traits\HasCommonEntityFieldsTrait;
use Drupal\viola_content\WrappedEntities\Traits\HasEntityReferencesTrait;
use Drupal\viola_content\WrappedEntities\TransformableWrappedEntity;

/**
 * The wrapped entity for the image media type.
 */
class Image extends TransformableWrappedEntity {

  use HasCommonEntityFieldsTrait;
  use HasEntityReferencesTrait;

  const MEDIA_FIELD_NAME = 'field_media_image';

  /**
   * @return string
   */
  public function getAltText() {
    if ($this->getEntity()->hasField(self::MEDIA_FIELD_NAME)) {
      return $this->getEntity()->get(self::MEDIA_FIELD_NAME)->first()->get('alt')->getString();
    }
    return '';
  }

  /**
   * @return string
   */
  public function getCaption() {
    return $this->getEntity()->get('field_caption')->value ?: '';
  }

  /**
   * @return array
   */
  public function getDerivativeImageUrls() {
    $file = $this->getFile();
    $sizes = [];
    if ($file && $file instanceof FileInterface) {
      $uri = $file->getFileUri();
      $sizes = [
        'xsmall' => ImageStyle::load('xsmall')->buildUrl($uri),
        'small' => ImageStyle::load('small')->buildUrl($uri),
        'medium' => ImageStyle::load('medium')->buildUrl($uri),
        'large' => ImageStyle::load('large')->buildUrl($uri),
        'xlarge' => ImageStyle::load('xlarge')->buildUrl($uri),
      ];
      $sizes['default'] = $sizes['medium'];
    }
    return $sizes;
  }

  /**
   * @return FileInterface|null
   */
  public function getFile() {
    $files = $this->getReferencedEntities($this->getEntity(), self::MEDIA_FIELD_NAME);
    if (is_array($files) && reset($files) instanceof FileInterface) {
     return reset($files);
    }
    return NULL;
  }

  /**
   * @return string
   */
  public function getHeight() {
    return $this->getEntity()->get('field_height')->value;
  }

  /**
   * @return string
   */
  public function getImageUrl() {
    $file = $this->getFile();
    if ($file && $file instanceof FileInterface) {
      $uri = $file->getFileUri();
      return $file->createFileUrl(FALSE);
    }
    return '';
  }

  /**
   * @return string
   */
  public function getOrientation() {
    if (!is_numeric($this->getWidth()) && !is_numeric($this->getHeight())) {
      return '';
    }
    if ($this->getWidth() == $this->getHeight()) {
      return 'square';
    }
    if ($this->getWidth() > $this->getHeight()) {
      return 'landscape';
    }
    else {
      return 'portrait';
    }
  }

  /**
   * @return string
   */
  public function getWidth() {
    return $this->getEntity()->get('field_width')->value;
  }


}
