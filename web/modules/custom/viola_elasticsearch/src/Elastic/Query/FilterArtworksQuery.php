<?php

namespace Drupal\viola_elasticsearch\Elastic\Query;

use Drupal\viola_elasticsearch\Elastic\Index;

/**
 * Class FilterArtworksQuery
 *   Build an Elasticsearch DSL Query to filter Artworks content.
 *
 * @package Drupal\viola_elasticsearch\Elastic
 */
class FilterArtworksQuery extends QueryDslBase {

  public function __construct($params) {
    parent::__construct();
    $sorts = NULL;
    //$this->setSort(['highlights.keyword']);
    // Set up the query with default filters.
    $query = [
      'bool' => [
        'must_not' => [
          // Results must not be copyright restricted.
          'term' => [
            'usageRights' => 'Restricted (See File)'
          ],
        ],
        'filter' => [
          [
            // Limit results to artworks.
            'term' => [
              'type' => 'artwork'
            ]
          ],
          [
            // Require results have an image.
            'exists' => [
              'field' => 'image',
            ]
          ],
        ]
      ],
    ];
    // Apply given nested query.
    if (array_key_exists('constituent_id', $params) && is_numeric($params['constituent_id'])) {
      $query['bool']['must']['nested'] = [
        'path' => 'creators',
        'query' => [
          'bool' => [
            'must' => [
              [
                'match' => [
                  'creators.constituentId' =>  $params['constituent_id']
                ],
              ],
            ],
          ],
        ],
      ];
    }
    // Apply given keyword filters to query.
    $keywords = ['classifications', 'mediums', 'periods'];
    foreach ($params as $key => $value) {
      if (in_array($key, $keywords)) {
        // Filter by keyword.
        if(is_array($value)) {
          foreach ($value as $key2 => $value2) {
            $size = sizeof($query['bool']['filter']);
            $query['bool']['filter'][$size] =
              ['term' => [
                $key . '.keyword' => $value2
              ]];
          }
        } else {
          $query['bool']['filter'][] = [
            'term' => [
              $key . '.keyword' => $value
            ]
          ];
        }
      }
      elseif ($key == 'colors') {
        $colors = is_array($value) ? $value : [$value];
        $nestedQueries = [];
        foreach ($colors as $color) {
          $nestedQueries[] = [
            'nested' => [
              'path' => 'colors',
              'query' => [
                'bool' => [
                  'filter' => [
                    [
                      'term' => [
                        'colors.color' => $color,
                      ]
                    ],
                  ]
                ]
              ],
            ],
          ];
        }
        $query['bool']['must'] = $nestedQueries;
        // Create a sort on the "weight" field in nested "colors" objects
        // @link: https://www.elastic.co/guide/en/elasticsearch/reference/6.8/search-request-sort.html#_nested_sorting_examples
        $sorts = [
          'colors.weight' => [
            'mode' => 'max',
            'order' => 'desc',
            'nested' => [
              'path' => 'colors',
              'filter' => [
                'terms' => [
                  'colors.color.keyword' => $colors,
                ],
              ],
            ],
          ]
        ];
      }
    }

    $this->setQuery($query);
    if ($sorts) {
      $this->setSort($sorts);
    }
  }
}
