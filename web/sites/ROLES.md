# User Roles

## Administrator
The administrator role has all site permissions.

## Site manager
The site manager would be able to manage most things about a site, including managing users.

## Editor
The Editor has rights Create/Read/Update/Delete permissions for all content, plus scheduling and publishing, but not necessarily site configuration.

## Author
Authors are basic content creators. They can Create/Read/Update all content, but can not publish outside the workflow, manage revisions, etc.

## Contributor
Contributors are authors with very limited permissions, and are only able to add "Post" content.

## Authenticated User
An authenticated visitor with no content management permissions.

Anonymous User
An unauthenticated site visitor.
