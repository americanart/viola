<?php


namespace Drupal\viola_content\Plugin\TypedRepositories\Traits;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\node\NodeInterface;
use Drupal\viola_core\SystemInfo;

/**
 * Trait IsNodeManager
 *   Common CRUD methods for all Node Repositories.
 *
 * @package Drupal\viola_content\Plugin\TypedRepositories\Traits
 */
trait IsNodeManagerTrait {

  /**
   * Load all entities.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]|bool
   */
  public function all() {
    return $this->findWhere();
  }

  /**
   * Load entities filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface[]|bool
   */
  public function findWhere($fields = []) {
    try {
      $query = $this->entityTypeManager->getStorage($this->getEntityType()->id());
      $conditions = [
        'status' => TRUE,
        'type' => $this->getBundle(),
      ];
      foreach ($fields as $field_name => $field_value) {
        $conditions[$field_name] = $field_value;
      }
      $results = $query->loadByProperties($conditions);
      $entities = [];
      foreach ($results as $item) {
        $entities[] = $item;
      }
      if ($entities) {
        return $this->wrapMultiple($entities);
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Load the first entity from results filtered by fields.
   *
   * @params array $fields
   *   An associative array of field name and values to filter.
   *
   * @return \Drupal\typed_entity\WrappedEntities\WrappedEntityInterface|bool
   */
  public function firstWhere($fields = []) {
    try {
      $results = $this->findWhere($fields);
      if ($results) {
        return reset($results);
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return FALSE;
  }


  /**
   * Check if a Node exists.
   *
   * @param int $id
   *   The link id.
   *
   * @return bool
   *   Boolean answer to whether a link exists.
   */
  public function exists($id) {
    try {
      return $this->entityTypeManager->getStorage($this->getEntityType()->id())->load($id) ? TRUE : FALSE;
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return FALSE;
  }

  /**
   * Prepare supplied fields and field values for entity create/update.
   *
   * @param array $fields
   *   Field data.
   *
   * @return array
   *   Combined array of default and supplied fields.
   */
  protected function prepareFields(array $fields) {
    $commonFields = [
      'langcode' => 'en',
      'changed' => \Drupal::time()->getRequestTime(),
      'type' => $this->getBundle(),
      'uid' => 1,
      'status' => 1,
    ];
    // Unset any non-Drupal fields that might have been attached.
    unset($fields['nid']);
    return array_merge($commonFields, $fields);
  }

  /**
   * Create a new Node populated by the provided field values.
   *
   * @param array $fields
   *
   * @return int
   */
  public function createNode(array $fields) {
    $status = 0;
    try {
      $fields = $this->prepareFields($fields);
      $node = $this->entityTypeManager->getStorage($this->getEntityType()->id())->create($fields)->enforceIsNew();
      $status = $node->save();
      // TODO: Also taxonomy terms have path alias, handle in base or override in subclass.
      if ($status == SAVED_NEW && $node instanceof NodeInterface) {
        // Update the Node with a path alias.
        \Drupal::service('pathauto.generator')->updateEntityAlias($node, 'update');
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }

  /**
   * Update an existing Node.
   *
   * @param array $fields
   * @param \Drupal\node\Entity\Node $node
   *
   * @return int
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function updateNode(array $fields, NodeInterface $node) {
    $status = 0;
    try {
      // Handle JSON `field_data` field by merging existing and supplied data.
      if (!empty($fields['field_data']) && $node->hasField('field_data')) {
        $data = Json::decode($node->get('field_data')->value) ?: [];
        $updates = Json::decode($fields['field_data']);
        $fields['field_data'] = Json::encode(array_merge($data, $updates));
      }
      $fields = $this->prepareFields($fields);
      foreach ($fields as $field_name => $field_value) {
        $node->set($field_name, $field_value);
      }
      // Manually create a revision?
      //if ($entity instanceof RevisionableInterface) {
      // TODO: Test if still necessary when this lands: @see https://www.drupal.org/project/drupal/issues/2803717
      // $entity->setNewRevision();
      // $entity->setRevisionUserId(1);
      //}

      /**
       * Clean up empty entity references before saving.
       * Empty references often occur when referenced content is deleted.
       * The update method not a perfect, but acceptable place to handle these
       * dangling references.
       * @see https://www.drupal.org/project/drupal/issues/2723323
       */
      $entityReferenceFields = SystemInfo::getEntityFieldsByType('entity_reference', $this->getEntityType()->id(), $this->getBundle());
      foreach ($entityReferenceFields as $field) {
        $fieldItemList = $node->$field;
        $hasBadReference = FALSE;
        if ($fieldItemList instanceof EntityReferenceFieldItemList) {
          foreach ($fieldItemList as $delta => $item) {
            if ($item && !$item->entity && $fieldItemList->get($delta)) {
              $fieldItemList->removeItem($delta);
              $hasBadReference = TRUE;
            }
          }
          if ($hasBadReference) {
            $fieldItemList->filterEmptyItems();
          }
        }
      }
      $status = $node->save();
      // TODO: Also taxonomy terms have path alias, handle in base or override in subclass.
      if ($status == SAVED_UPDATED) {
        // Update the Node with a path alias.
        \Drupal::service('pathauto.generator')->updateEntityAlias($node, 'update');
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('viola_content')->error($e->getMessage());
    }
    return $status;
  }

  /**
   * Delete a Node.
   *
   * @param \Drupal\node\Entity\Node $node
   *
   * @return mixed
   */
  public function deleteNode(NodeInterface $node) {
    return $this->entityTypeManager->getStorage($this->getEntityType()->id())->delete([$node]);
  }

}
